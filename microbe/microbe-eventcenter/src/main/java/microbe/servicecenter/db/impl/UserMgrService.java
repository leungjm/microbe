package microbe.servicecenter.db.impl;

import java.util.List;

import microbe.dao.IUserDAO;
import microbe.model.tables.User;
import microbe.servicecenter.db.IUserMgrService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lanshi.microbe.repository.hibernate.service.DAOBasedService;

@Service
@Transactional(readOnly = true)
public class UserMgrService extends DAOBasedService<User, Long, IUserDAO>
		implements IUserMgrService {

	@Override
	public List<User> findByName(String name) {
		if (dao != null) {
			return dao.findByName(name);
		}
		return null;
	}

}
