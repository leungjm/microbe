package microbe.servicecenter.db;

import java.util.Date;
import java.util.List;

import microbe.model.enums.JobStatusEnum;
import microbe.model.tables.JobRecord;
import microbe.model.view.PagerListResultJson;

import com.lanshi.microbe.repository.hibernate.service.IDAOBasedService;

public interface IJobRecordMgrService extends IDAOBasedService<JobRecord, Long> {

	List<JobRecord> findByGroupJobGroupModelId(String group,
			String jobGroupModelId);

	List<JobRecord> findByJobGroup(String group);

	List<JobRecord> findByStatusDayBet(List<JobStatusEnum> status,
			Date beginDateTime, Date endDateTime);

	List<JobRecord> findByStatusBeforeDay(List<JobStatusEnum> status,
			Date endDateTime);

	List<JobRecord> findByUserId(Long userId);

	List<JobRecord> findByUserIdStartDayEndDay(Long userId, Date startDay,
			Date endDay);

	List<JobRecord> findByUserIdStartDayEndDayStatuses(Long userId,
			Date startTime, Date endTime, JobStatusEnum[] statuses);

	List<String> findJobGrouByUserIdGroupByJobGroup(Long userId);

	List<JobRecord> findByUserIdDayBetween(Long userId, Date beginDateTime,
			Date endDateTime);

	List<JobRecord> findByUserIdStartDayEndDayStatusGroups(Long userId,
			Date startTime, Date endTime, JobStatusEnum[] status,
			String[] groups);

	PagerListResultJson<JobRecord> findByUserIdDayBetweenPages(Long userId,
			Date beginDateTime, Date endDateTime, Integer pageIndex,
			Integer pageSize);

}
