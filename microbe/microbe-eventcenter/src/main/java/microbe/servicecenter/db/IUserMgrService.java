package microbe.servicecenter.db;

import java.util.List;

import microbe.model.tables.User;

import com.lanshi.microbe.repository.hibernate.service.IDAOBasedService;

public interface IUserMgrService extends IDAOBasedService<User, Long> {

	List<User> findByName(String name);

}
