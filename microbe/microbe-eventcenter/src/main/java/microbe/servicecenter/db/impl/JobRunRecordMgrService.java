package microbe.servicecenter.db.impl;

import java.util.List;

import microbe.dao.IJobRunRecordDAO;
import microbe.model.tables.JobRunRecord;
import microbe.servicecenter.db.IJobRunRecordMgrService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lanshi.microbe.repository.hibernate.service.DAOBasedService;

@Service
@Transactional(readOnly = true)
public class JobRunRecordMgrService extends
		DAOBasedService<JobRunRecord, Long, IJobRunRecordDAO> implements
		IJobRunRecordMgrService {

	@Override
	public List<JobRunRecord> findByJobId(Long jobId) {
		if (dao != null) {
			return dao.findByJobId(jobId);
		}
		return null;
	}

}
