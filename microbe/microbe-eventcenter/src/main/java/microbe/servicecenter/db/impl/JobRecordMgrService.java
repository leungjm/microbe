package microbe.servicecenter.db.impl;

import java.util.Date;
import java.util.List;

import microbe.dao.IJobRecordDAO;
import microbe.model.enums.JobStatusEnum;
import microbe.model.tables.JobRecord;
import microbe.model.view.PagerListResultJson;
import microbe.servicecenter.db.IJobRecordMgrService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lanshi.microbe.repository.hibernate.service.DAOBasedService;

@Service
@Transactional(readOnly = true)
public class JobRecordMgrService extends
		DAOBasedService<JobRecord, Long, IJobRecordDAO> implements
		IJobRecordMgrService {

	@Override
	public List<JobRecord> findByGroupJobGroupModelId(String group,
			String jobGroupModelId) {
		if (dao != null) {
			return dao.findByGroupJobGroupModelId(group, jobGroupModelId);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByJobGroup(String group) {
		if (dao != null) {
			return dao.findByJobGroup(group);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByStatusDayBet(List<JobStatusEnum> status,
			Date beginDateTime, Date endDateTime) {
		if (dao != null) {
			return dao.findByStatusDayBet(status, beginDateTime, endDateTime);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByStatusBeforeDay(List<JobStatusEnum> status,
			Date endDateTime) {
		if (dao != null) {
			return dao.findByStatusBeforeDay(status, endDateTime);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByUserId(Long userId) {
		if (dao != null) {
			return dao.findByUserId(userId);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByUserIdStartDayEndDay(Long userId,
			Date startDay, Date endDay) {
		if (dao != null) {
			return dao.findByUserIdStartDayEndDay(userId, startDay, endDay);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByUserIdStartDayEndDayStatuses(Long userId,
			Date startTime, Date endTime, JobStatusEnum[] statuses) {
		if (dao != null) {
			return dao.findByUserIdStartDayEndDay(userId, startTime, endTime,
					statuses);
		}
		return null;
	}

	@Override
	public List<String> findJobGrouByUserIdGroupByJobGroup(Long userId) {
		if (dao != null) {
			return dao.findJobGroupByUserIdGroupByJobGroup(userId);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByUserIdDayBetween(Long userId,
			Date beginDateTime, Date endDateTime) {
		if (dao != null) {
			return dao.findByUserIdDayBetween(userId, beginDateTime,
					endDateTime);
		}
		return null;
	}

	@Override
	public List<JobRecord> findByUserIdStartDayEndDayStatusGroups(Long userId,
			Date startTime, Date endTime, JobStatusEnum[] status,
			String[] groups) {
		if (dao != null) {
			return dao.findByUserIdStartDayEndDayStatusGroups(userId,
					startTime, endTime, status, groups);
		}
		return null;
	}

	@Override
	public PagerListResultJson<JobRecord> findByUserIdDayBetweenPages(
			Long userId, Date beginDateTime, Date endDateTime,
			Integer pageIndex, Integer pageSize) {
		if (dao != null) {
			return dao.findByUserIdDayBetweenPages(userId, beginDateTime,
					endDateTime, pageIndex, pageSize);
		}
		return null;
	}

}
