package microbe.servicecenter.db;

import java.util.List;

import microbe.model.tables.JobRunRecord;

import com.lanshi.microbe.repository.hibernate.service.IDAOBasedService;

public interface IJobRunRecordMgrService extends
		IDAOBasedService<JobRunRecord, Long> {

	List<JobRunRecord> findByJobId(Long jobId);

}
