package microbe.servicecenter.service;

import microbe.model.tables.JobRecord;

public interface IJobService {
	public boolean addNotifyRobot(JobRecord jobRecord);

	public boolean stopNotifyRobot(JobRecord jobRecord);

	public boolean delNotifyRobot(JobRecord jobRecord);
}
