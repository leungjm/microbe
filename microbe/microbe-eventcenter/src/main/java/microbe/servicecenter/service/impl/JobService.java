package microbe.servicecenter.service.impl;

import java.util.Date;

import microbe.jobcenter.NotifyRobot;
import microbe.model.tables.JobRecord;
import microbe.scheduler.IRobotManager;
import microbe.scheduler.Robot;
import microbe.servicecenter.db.IJobRecordMgrService;
import microbe.servicecenter.db.IJobRunRecordMgrService;
import microbe.servicecenter.service.IJobService;
import microbe.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.repository.hibernate.service.BaseService;

@Service
public class JobService extends BaseService implements IJobService {

	@Autowired
	private IRobotManager robotManager;
	@Autowired
	private IJobRecordMgrService jobRecordMgrService;
	@Autowired
	private IJobRunRecordMgrService jobRunRecordMgrService;

	@Override
	public boolean addNotifyRobot(JobRecord jobRecord) {
		Date notifyDay = jobRecord.getNotifyTime();
		Date now = DateUtil.getDate();
		if (DateUtil.isSameDate(now, notifyDay) && notifyDay.after(now)) {// 假如是同一天并且此时后，立马添加到schedulers
			MicrobeLogger.info("now add notify robot sqlId={}",
					jobRecord.getId());
			NotifyRobot robot = new NotifyRobot(jobRecord.getJobGroup());
			robot.setActive(true);
			robot.setName(jobRecord.toString());
			robot.setRepeatCount(jobRecord.getRepeatCount());
			robot.setRepeatInterval(jobRecord.getRepeatTimeInterval());
			robot.setSqlId(jobRecord.getId());
			robot.setStartTime(jobRecord.getNotifyTime());
			robot.setJobRecordMgrService(jobRecordMgrService);
			robot.setJobRunRecordMgrService(jobRunRecordMgrService);
			robot.setJobService(this);
			robot.init();
			return robotManager.addRobot(robot);
		}
		return false;
	}

	@Override
	public boolean stopNotifyRobot(JobRecord jobRecord) {
		MicrobeLogger.info("now stop notify robot sqlId={} ...",
				jobRecord.getId());
		Robot robot = robotManager.getRobotByGroupSqlId(
				jobRecord.getJobGroup(), jobRecord.getId());
		if (robot != null) {
			return robotManager.stopRobot(robot.getUuid());
		} else {
			MicrobeLogger.info("robot sqlId={} is missing...",
					jobRecord.getId());
		}
		return false;
	}

	@Override
	public boolean delNotifyRobot(JobRecord jobRecord) {
		MicrobeLogger.info("now delete nofity robot sqlId={}",
				jobRecord.getId());
		Robot robot = robotManager.getRobotByGroupSqlId(
				jobRecord.getJobGroup(), jobRecord.getId());
		if (robot != null) {
			return robotManager.delRobot(robot.getUuid());
		} else {
			MicrobeLogger.info("robot sqlId={} is missing...",
					jobRecord.getId());
		}
		return false;
	}

}
