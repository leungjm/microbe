package microbe.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import microbe.Constant;
import microbe.model.enums.JobStatusEnum;
import microbe.model.json.LoginAccountJson;
import microbe.model.tables.JobRecord;
import microbe.model.tables.JobRunRecord;
import microbe.model.view.ListResultJson;
import microbe.model.view.ObjectResultJson;
import microbe.model.view.PagerListResultJson;
import microbe.model.view.ResponseCode;
import microbe.scheduler.IRobotManager;
import microbe.scheduler.Robot;
import microbe.servicecenter.db.IJobRecordMgrService;
import microbe.servicecenter.db.IJobRunRecordMgrService;
import microbe.util.DateUtil;
import microbe.util.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.repository.hibernate.dao.PageCounter;

@RestController
public class JobRecordController extends BaseCtrl {
	@Autowired
	private IJobRecordMgrService jobRecordMgrService;
	@Autowired
	private IRobotManager robotManager;
	@Autowired
	private IJobRunRecordMgrService jobRunRecordMgrService;

	@RequestMapping(value = "/record/findByGroupJob.do", method = RequestMethod.POST)
	public ListResultJson<String> findByGroupJob(HttpServletRequest request) {
		ListResultJson<String> result = new ListResultJson<String>();
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (loginJson != null) {
			List<String> records = jobRecordMgrService
					.findJobGrouByUserIdGroupByJobGroup(loginJson.getUser()
							.getId());
			result.setResultCode(ResponseCode.SUCC);
			result.setResultDesc("查询成功");
			result.setResponse(records);
		} else {
			result.setResultCode(ResponseCode.UN_LOGIN);
			result.setResultDesc("尚未登陆");
		}
		return result;
	}

	@RequestMapping(value = "/record/findByUserTodayOrJobRun.do", method = RequestMethod.POST)
	public PagerListResultJson<JobRecord> findByUserTodayOrJobRun(
			HttpServletRequest request,
			@RequestParam(required = true) Boolean jobRun,
			@RequestParam("pageIndex") Integer pageIndex,
			@RequestParam("pageSize") Integer pageSize) {
		PagerListResultJson<JobRecord> result = new PagerListResultJson<JobRecord>();
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (loginJson != null) {
			pageIndex = pageIndex - 1;
			List<JobRecord> records = null;
			if (jobRun) {
				records = new ArrayList<JobRecord>();
				List<String> groups = jobRecordMgrService
						.findJobGrouByUserIdGroupByJobGroup(loginJson.getUser()
								.getId());
				Integer totalSize = 0;
				if (groups != null) {
					for (String group : groups) {
						List<Robot> robots = robotManager.getGroupRobots(group,
								pageIndex, pageSize);
						totalSize += robotManager.getGroupRobotsSize(group);
						if (robots != null && robots.size() > 0) {
							for (Robot robot : robots) {
								JobRecord job = jobRecordMgrService
										.findById(robot.getSqlId());
								if (job != null) {
									if (job.getJobStatus() != JobStatusEnum.RUNNING
											&& robot.isActive()) {
										job.setJobStatus(JobStatusEnum.RUNNING);
										jobRecordMgrService.update(job);
									}
									records.add(job);
								}
							}
						}
					}
				}
				result.setPageCounter(new PageCounter(totalSize, pageIndex,
						pageSize));
				result.setResponse(records);
			} else {
				result = jobRecordMgrService.findByUserIdDayBetweenPages(
						loginJson.getUser().getId(),
						DateUtil.getBeginDateTime(DateUtil.getDate()),
						DateUtil.getEndDateTime(DateUtil.getDate()), pageIndex,
						pageSize);
			}
			result.setResultCode(ResponseCode.SUCC);
			result.setResultDesc("查询成功");
		} else {
			result.setResultCode(ResponseCode.UN_LOGIN);
			result.setResultDesc("尚未登陆");
		}
		return result;
	}

	@RequestMapping(value = "/record/findByDaysStatusGroupsRecords.do", method = RequestMethod.POST)
	public ListResultJson<JobRecord> findByDaysStatusGroupsRecords(
			HttpServletRequest request,
			String startDay,
			String endDay,
			@RequestParam(value = "status[]", required = false) JobStatusEnum[] status,
			@RequestParam(value = "groups[]", required = false) String[] groups) {
		ListResultJson<JobRecord> result = new ListResultJson<JobRecord>();
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (loginJson != null) {
			Date startTime = null;
			Date endTime = null;
			try {
				if (!StringUtil.isEmptyStr(startDay)) {
					startTime = DateUtil.getBeginDateTime(DateUtil
							.parseDateDayFormat(startDay));
				}
				if (!StringUtil.isEmptyStr(startDay)) {
					endTime = DateUtil.getEndDateTime(DateUtil
							.parseDateDayFormat(endDay));
				}
			} catch (ParseException e) {
				MicrobeLogger.error("", e);
			}
			List<JobRecord> records = jobRecordMgrService
					.findByUserIdStartDayEndDayStatusGroups(loginJson.getUser()
							.getId(), startTime, endTime, status, groups);
			result.setResultCode(ResponseCode.SUCC);
			result.setResultDesc("查询成功");
			result.setResponse(records);
		} else {
			result.setResultCode(ResponseCode.UN_LOGIN);
			result.setResultDesc("尚未登陆");
		}
		return result;
	}

	@RequestMapping(value = "/record/findJobRunRecords.do", method = RequestMethod.POST)
	public ListResultJson<JobRunRecord> findJobRunRecords(
			HttpServletRequest request, @RequestParam("recordId") Long recordId) {
		ListResultJson<JobRunRecord> result = new ListResultJson<JobRunRecord>();
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (loginJson != null) {
			if (recordId != null) {
				List<JobRunRecord> records = jobRunRecordMgrService
						.findByJobId(recordId);
				result.setResultCode(ResponseCode.SUCC);
				result.setResultDesc("查询成功");
				result.setResponse(records);
			} else {
				result.setResultCode(ResponseCode.FAIL);
				result.setResultDesc("参数异常");
			}
		} else {
			result.setResultCode(ResponseCode.UN_LOGIN);
			result.setResultDesc("尚未登陆");
		}
		return result;
	}

	@RequestMapping(value = "/record/changeStatus.do", method = RequestMethod.POST)
	public ObjectResultJson<JobRecord> changeStatus(HttpServletRequest request,
			@RequestParam("recordId") Long recordId,
			@RequestParam("status") JobStatusEnum status) {
		ObjectResultJson<JobRecord> result = new ObjectResultJson<JobRecord>();
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (loginJson != null) {
			if (recordId != null) {
				JobRecord record = jobRecordMgrService.findById(recordId);
				if (record != null) {
					record.setJobStatus(status);
					jobRecordMgrService.update(record);
					Robot robot = robotManager.getRobotByGroupSqlId(
							record.getJobGroup(), record.getId());
					if (status == JobStatusEnum.CANCEL) {
						if (robot != null) {
							robotManager.delRobot(robot.getUuid());
						}
					} else if (status == JobStatusEnum.RUNNING) {
						if (robot != null) {
							if (!robot.isActive()) {
								robotManager.startRobot(robot.getUuid());
							}
						}
					} else if (status == JobStatusEnum.STOP) {
						if (robot != null) {
							robotManager.stopRobot(robot.getUuid());
						}
					}
					result.setResultCode(ResponseCode.SUCC);
					result.setResultDesc("查询成功");
					result.setResponse(record);
				} else {
					result.setResultCode(ResponseCode.FAIL);
					result.setResultDesc("参数异常");
				}
			} else {
				result.setResultCode(ResponseCode.FAIL);
				result.setResultDesc("参数异常");
			}
		} else {
			result.setResultCode(ResponseCode.UN_LOGIN);
			result.setResultDesc("尚未登陆");
		}
		return result;
	}
}
