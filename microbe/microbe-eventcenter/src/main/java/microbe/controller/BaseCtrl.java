package microbe.controller;

import javax.annotation.PostConstruct;

import com.lanshi.microbe.Logger.MicrobeLogger;

public abstract class BaseCtrl {

	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info(getClass() + " is inited!");
	}

}
