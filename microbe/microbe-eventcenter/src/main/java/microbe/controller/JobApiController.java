package microbe.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import microbe.Constant;
import microbe.model.enums.JobStatusEnum;
import microbe.model.json.LoginAccountJson;
import microbe.model.tables.JobRecord;
import microbe.model.view.BaseResultJson;
import microbe.model.view.ResponseCode;
import microbe.servicecenter.db.IJobRecordMgrService;
import microbe.servicecenter.service.IJobService;
import microbe.util.DateUtil;
import microbe.util.JsonUtil;
import microbe.util.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanshi.microbe.Logger.MicrobeLogger;

@RestController
public class JobApiController extends BaseCtrl {

	@Autowired
	private IJobRecordMgrService jobRecordMgrService;
	@Autowired
	private IJobService jobService;

	@RequestMapping(value = "/job/addRobot.do", method = RequestMethod.POST)
	public BaseResultJson addRobot(HttpServletRequest request, String data) {
		BaseResultJson result = new BaseResultJson();
		MicrobeLogger.info("addRobot with data:{}", data);
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (loginJson != null) {
			if (data != null) {
				try {
					ObjectMapper mapper = JsonUtil.getInstance();
					JsonNode jsonNode = mapper.readTree(data);
					String jobGroupModelId = jsonNode.get("jobGroupModelId")
							.asText();// 第三方所属组的唯一标识
					String jobGroup = jsonNode.get("jobGroup").asText();// 第三方所属组
					String jobModelDesc = jsonNode.get("jobModelDesc").asText();// 第三方所属组的唯一标识
					String jobNotifyJsonData = jsonNode
							.get("jobNotifyJsonData").asText();// 通知时回传数据
					String notifyTimeStr = jsonNode.get("notifyTime").asText();// 首次提醒时间
					Integer tryFailCount = jsonNode.get("tryFailCount").asInt();// 失败尝试次数
					Integer failTimeInterval = jsonNode.get("failTimeInterval")
							.asInt();// 失败尝试时间间隔
					Integer repeatCount = jsonNode.get("repeatCount").asInt();// 循环次数
					Long repeatTimeInterval = jsonNode
							.get("repeatTimeInterval").asLong();// 重复时间间隔
					String notifyUrl = jsonNode.get("notifyUrl").asText();// 通知调用的url
					Date notifyTime = DateUtil
							.parseDateSecondFormat(notifyTimeStr);
					// ----------------------------------------
					if (!StringUtil.isEmptyStr(jobGroupModelId)
							&& !StringUtil.isEmptyStr(jobGroup)) {
						List<JobRecord> jobRecords = jobRecordMgrService
								.findByGroupJobGroupModelId(jobGroup,
										jobGroupModelId);
						if (jobRecords != null && jobRecords.size() > 0) {
							result.setResultCode(ResponseCode.UN_ACCETABLE);
							result.setResultDesc("jobGroupModelId已经存在");
						} else {
							JobRecord record = new JobRecord();
							record.setFailTimeInterval(failTimeInterval == null ? 0
									: failTimeInterval);
							record.setJobGroup(jobGroup);
							record.setJobGroupModelId(jobGroupModelId);
							record.setJobNotifyJsonData(jobNotifyJsonData);
							record.setJobRunCount(0);
							record.setJobStatus(JobStatusEnum.WAITING);
							record.setNotifyTime(notifyTime);
							record.setJobModelDesc(jobModelDesc);
							record.setNotifyUrl(notifyUrl);
							record.setRepeatCount(repeatCount < 0 ? 0
									: repeatCount);
							record.setRepeatTimeInterval(repeatTimeInterval <= 0 ? Constant.DAY_SECOND
									: repeatTimeInterval);
							record.setTryFailCount(tryFailCount < 0 ? 0
									: tryFailCount);
							record.setUserId(loginJson.getUser().getId());
							if (jobRecordMgrService.add(record)) {
								result.setResultCode(ResponseCode.SUCC);
								result.setResultDesc("添加任务成功");
								jobService.addNotifyRobot(record);
							} else {
								result.setResultCode(ResponseCode.SUCC);
								result.setResultDesc("添加任务成功");
							}
						}
					} else {
						result.setResultCode(ResponseCode.UN_ACCETABLE);
						result.setResultDesc("jobGroupModelId不能为空，且jobGroupModelId必须是该用户下唯一的");
					}
				} catch (JsonProcessingException e) {
					MicrobeLogger.error("", e);
					result.setResultCode(ResponseCode.UN_ACCETABLE);
					result.setResultDesc("参数内容解析有错");
				} catch (IOException e) {
					MicrobeLogger.error("", e);
					result.setResultCode(ResponseCode.UN_ACCETABLE);
					result.setResultDesc("参数内容解析有错");
				} catch (ParseException e) {
					MicrobeLogger.error("", e);
					result.setResultCode(ResponseCode.UN_ACCETABLE);
					result.setResultDesc("参数内容提醒时间解析有错，输入时间格式为："
							+ DateUtil.YYYYMMDDHHMMSS);
				}
			} else {
				result.setResultCode(ResponseCode.UN_ACCETABLE);
				result.setResultDesc("参数有错");
			}
		} else {
			result.setResultCode(ResponseCode.UN_LOGIN);
			result.setResultDesc("尚未登录");
		}
		return result;
	}

	@RequestMapping(value = "/job/delRobot.do", method = RequestMethod.POST)
	public BaseResultJson delRobot(HttpServletRequest request, String data) {
		BaseResultJson result = new BaseResultJson();
		LoginAccountJson loginJson = (LoginAccountJson) request
				.getAttribute(Constant.LOGIN_USER_JSON);
		if (data != null) {
			try {
				ObjectMapper mapper = JsonUtil.getInstance();
				JsonNode jsonNode = mapper.readTree(data);
				String jobGroupModelId = jsonNode.get("jobGroupModelId")
						.asText();// 第三方所属组的唯一标识
				String jobGroup = jsonNode.get("jobGroup").asText();// 第三方所属组的唯一标识
				if (!StringUtil.isEmptyStr(jobGroupModelId)
						&& !StringUtil.isEmptyStr(jobGroup)) {
					List<JobRecord> jobRecords = jobRecordMgrService
							.findByGroupJobGroupModelId(jobGroup,
									jobGroupModelId);
					if (jobRecords != null && jobRecords.size() > 0) {
						JobRecord record = jobRecords.get(0);
						if (record.getUserId().equals(
								loginJson.getUser().getId())) {
							record.setJobStatus(JobStatusEnum.CANCEL);
							if (jobRecordMgrService.update(record)) {
								result.setResultCode(ResponseCode.SUCC);
								result.setResultDesc("删除成功");
								jobService.delNotifyRobot(record);
							} else {
								result.setResultCode(ResponseCode.FAIL);
								result.setResultDesc("删除失败");
							}
						} else {
							result.setResultCode(ResponseCode.FAIL);
							result.setResultDesc("删除失败, 该任务不属于您！");
						}
					} else {
						result.setResultCode(ResponseCode.UN_ACCETABLE);
						result.setResultDesc("参数有错, 找不到相应记录");
					}
				} else {
					result.setResultCode(ResponseCode.UN_ACCETABLE);
					result.setResultDesc("参数有错, jobGroupModelId不能为空");
				}
			} catch (Exception e) {
				MicrobeLogger.error("", e);
				result.setResultCode(ResponseCode.UN_ACCETABLE);
				result.setResultDesc("参数有错");
			}
		}
		return result;
	}
}
