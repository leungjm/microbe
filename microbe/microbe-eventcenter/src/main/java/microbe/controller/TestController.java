package microbe.controller;

import microbe.model.tables.User;
import microbe.servicecenter.db.IUserMgrService;
import microbe.util.MD5HashUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController extends BaseCtrl {

	@Autowired
	private IUserMgrService userMgrService;

	@RequestMapping("test/admin/add.do")
	public User addUser() {
		User user = new User();
		user.setGroup("jfit");
		user.setPassword(MD5HashUtil.hashCode("jfit2016", true));
		user.setUserName("jfit");
		userMgrService.add(user);
		return user;
	}

	@RequestMapping(value = "test/admin/hello.do", method = RequestMethod.GET)
	public User hello() {
		User user = new User();
		user.setGroup("Root");
		user.setPassword("HelloKetty!");
		user.setUserName("root");
		return user;
	}

	@RequestMapping(value = "test/admin/index.do", method = RequestMethod.GET)
	public String showIndex() {
		return "HEklJLKJKDSIUFB";
	}

	@RequestMapping(value = "test/admin/testJobCallBack.do", method = RequestMethod.POST)
	public String testJobCallBack(String data) {
		return "job say:" + data;
	}
}
