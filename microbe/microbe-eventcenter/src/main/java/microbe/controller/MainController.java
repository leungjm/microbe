package microbe.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import microbe.Constant;
import microbe.model.json.LoginAccountJson;
import microbe.model.tables.User;
import microbe.model.view.BaseResultJson;
import microbe.model.view.ResponseCode;
import microbe.servicecenter.db.IUserMgrService;
import microbe.util.MD5HashUtil;
import microbe.util.SessionUtil;
import microbe.util.StringUtil;
import microbe.util.UUIDGeneratorUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController extends BaseCtrl {

	@Autowired
	private IUserMgrService userMgrService;

	@RequestMapping(value = "/admin/login.do", method = RequestMethod.POST)
	public BaseResultJson login(HttpServletRequest request,
			HttpServletResponse response, String name, String password) {
		BaseResultJson json = new BaseResultJson();
		if (!StringUtil.isEmptyStr(name) && !StringUtil.isEmptyStr(password)) {
			List<User> userList = userMgrService.findByName(name);
			if (userList != null && userList.size() == 1) {
				User user = userList.get(0);
				if (MD5HashUtil.hashCode(password, true).equals(
						user.getPassword())) {
					String sessionId = UUIDGeneratorUtil.getSimpleUUID();
					LoginAccountJson loginInfo = new LoginAccountJson();
					loginInfo.setIp(request.getRemoteAddr());
					loginInfo.setLoginTime(new Date());
					loginInfo.setUser(user);
					loginInfo.setUuid(sessionId);
					SessionUtil.putLoginInfo(sessionId, loginInfo);
					// 设置请求的session
					response.setHeader(Constant.SESSION_NAME, sessionId);
					json.setResultCode(ResponseCode.SUCC);
					json.setResultDesc("登录成功");
				} else {
					json.setResultCode(ResponseCode.FAIL);
					json.setResultDesc("密码错误");
				}
			} else {
				json.setResultCode(ResponseCode.FAIL);
				json.setResultDesc("用户名错误");
			}
		} else {
			json.setResultCode(ResponseCode.FAIL);
			json.setResultDesc("用户名/密码错误");
		}
		return json;
	}

	@RequestMapping(value = "/login/check.do", method = RequestMethod.POST)
	public BaseResultJson loginCheck(HttpServletRequest request) {
		BaseResultJson json = new BaseResultJson();
		json.setResultCode(ResponseCode.SUCC);
		json.setResultDesc("登录成功");
		return json;
	}
}
