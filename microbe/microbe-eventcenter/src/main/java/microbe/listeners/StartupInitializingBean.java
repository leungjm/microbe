package microbe.listeners;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;

@Component
public class StartupInitializingBean implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		MicrobeLogger
				.info("StartupInitializingBean runing ...........................");

	}

}
