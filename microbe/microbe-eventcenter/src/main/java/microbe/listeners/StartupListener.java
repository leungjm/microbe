package microbe.listeners;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import microbe.jobcenter.NotifyRobot;
import microbe.model.enums.JobStatusEnum;
import microbe.model.tables.JobRecord;
import microbe.scheduler.IRobotManager;
import microbe.servicecenter.db.IJobRecordMgrService;
import microbe.servicecenter.db.IJobRunRecordMgrService;
import microbe.servicecenter.service.IJobService;
import microbe.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;

@Component
public class StartupListener implements
		ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	private IRobotManager robotManager;
	@Autowired
	private IJobRecordMgrService jobRecordMgrService;
	@Autowired
	private IJobRunRecordMgrService jobRunRecordMgrService;
	@Autowired
	private IJobService jobService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		MicrobeLogger
				.info("StartupListener runing ...........................");
		initNotifyJob();

	}

	private void initNotifyJob() {
		MicrobeLogger.info("init notify job.....");
		List<JobStatusEnum> status = new ArrayList<JobStatusEnum>();
		status.add(JobStatusEnum.RUNNING);
		status.add(JobStatusEnum.WAITING);
		final Date now = DateUtil.getDate();
		final List<JobRecord> records = jobRecordMgrService.findByStatusDayBet(
				status, DateUtil.getBeginDateTime(now),
				DateUtil.getEndDateTime(now));
		if (records != null && records.size() > 0) {
			ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(
					1);
			exec.schedule(new Runnable() {
				@Override
				public void run() {
					Date now = DateUtil.getDate();
					for (JobRecord record : records) {
						if (now.before(record.getNotifyTime())) {
							if (jobService.addNotifyRobot(record)) {
								record.setJobStatus(JobStatusEnum.RUNNING);
								jobRecordMgrService.update(record);
							} else {
								MicrobeLogger.warn(
										"robot add fail with sqlId={}",
										record.getId());
							}
						} else {
							NotifyRobot robot = new NotifyRobot(record
									.getJobGroup());
							robot.setActive(true);
							robot.setName(record.toString());
							robot.setRepeatCount(record.getRepeatCount());
							robot.setRepeatInterval(record
									.getRepeatTimeInterval());
							robot.setSqlId(record.getId());
							robot.setStartTime(record.getNotifyTime());
							robot.setJobRecordMgrService(jobRecordMgrService);
							robot.setJobRunRecordMgrService(jobRunRecordMgrService);
							robot.setJobService(jobService);
							robot.init();
							robot.execute();
						}
					}
				}
			}, 60000, TimeUnit.MILLISECONDS);

		}

	}

}
