package microbe.jobcenter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import microbe.Constant;
import microbe.model.enums.JobStatusEnum;
import microbe.model.enums.OperatorTypeEnum;
import microbe.model.enums.RunResultEnum;
import microbe.model.tables.JobRecord;
import microbe.model.tables.JobRunRecord;
import microbe.model.view.ResponseCode;
import microbe.scheduler.Robot;
import microbe.servicecenter.db.IJobRecordMgrService;
import microbe.servicecenter.db.IJobRunRecordMgrService;
import microbe.servicecenter.service.IJobService;
import microbe.util.DateUtil;
import microbe.util.HttpUtil;
import microbe.util.JsonUtil;

import org.apache.http.client.methods.CloseableHttpResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanshi.microbe.Logger.MicrobeLogger;

public class NotifyRobot extends Robot {

	private IJobRecordMgrService jobRecordMgrService;
	private IJobRunRecordMgrService jobRunRecordMgrService;
	private IJobService jobService;
	private static final String defaultEncoding = "UTF-8";

	public NotifyRobot(String group) {
		super(group);
	}

	@Override
	protected boolean doJob() throws Exception {
		JobRecord job = jobRecordMgrService.findById(sqlId);
		boolean runResult = false;
		if (job != null
				&& (job.getJobStatus() == JobStatusEnum.RUNNING || job
						.getJobStatus() == JobStatusEnum.WAITING)) {
			CloseableHttpResponse response = null;
			try {
				int tryNotifyCount = 0;
				String code = null;
				String desc = null;
				ObjectMapper mapper = JsonUtil.getInstance();
				for (int i = -1; i < job.getTryFailCount(); i++) {
					Map<String, String> param = new HashMap<String, String>();
					JobRunRecord record = new JobRunRecord();
					param.put("modelId", job.getJobGroupModelId());
					param.put("data", job.getJobNotifyJsonData());
					MicrobeLogger.info(
							"now try notify job with times:{}, record:{}",
							tryNotifyCount, JsonUtil.obj2json(job));
					response = HttpUtil.getInstance().doPostForResponse(
							job.getNotifyUrl(), param, null, defaultEncoding,
							5000, 5000, null);
					if (response != null) {
						int status = response.getStatusLine().getStatusCode();
						if (status == 200) {
							String result = HttpUtil.entity2String(response,
									defaultEncoding);
							if (result != null) {
								try {
									JsonNode jsonNode = mapper.readTree(result);
									code = jsonNode.get(Constant.RESPONSE_CODE)
											.asText();
									desc = jsonNode.get(Constant.RESPONSE_DESC)
											.asText();
								} catch (Exception e) {
									MicrobeLogger
											.warn("job notify client with result:{} is not def json. but return statusCode:{}",
													result, status);
									code = ResponseCode.SUCC;
									desc = "job notify client with result:"
											+ result
											+ " is not def json. but return statusCode:"
											+ status;
								}
							}
						}
					}

					if (ResponseCode.SUCC.equals(code) || "SUCC".equals(code)
							|| "SUCCESS".equals(code) || "succ".equals(code)) {
						record.setRunResult(RunResultEnum.SUCCESS);
						runResult = true;
						MicrobeLogger.info("jobID-" + job.getId() + "运行成功");
					} else {
						record.setRunResult(RunResultEnum.FAIL);
						MicrobeLogger.info("jobID-" + job.getId() + "运行失败");
					}

					record.setJobId(job.getId());
					record.setOperatorId(null);
					record.setOperatorType(OperatorTypeEnum.ROBOT);
					record.setReturnCode(code);
					record.setReturnMsg(desc);
					record.setRunTime(job.getNotifyTime());
					jobRunRecordMgrService.add(record);

					if (!runResult && job.getFailTimeInterval() > 0) {
						try {
							Thread.sleep(job.getFailTimeInterval());
						} catch (Exception e) {
							MicrobeLogger.error("jobID-" + job.getId()
									+ "运行出错：", e);
						}
					} else {
						break;
					}
				}
				// 更改任务是否重复执行
				job.setHadRepeatCount(job.getHadRepeatCount() + 1);
				if (job.getRepeatCount() - job.getHadRepeatCount() > 0) {
					job.setJobStatus(JobStatusEnum.RUNNING);
					Date now = DateUtil.getDate();
					Date nextDay = DateUtil.getDate(job.getNotifyTime()
							.getTime() + job.getRepeatTimeInterval());
					while (true) {
						if (nextDay.after(now)) {
							job.setNotifyTime(nextDay);// 更新下次执行时间
							break;
						} else {
							nextDay = DateUtil.getDate(nextDay.getTime()
									+ job.getRepeatTimeInterval());
						}
					}
				} else {// 循环执行次数已完成，任务停止
					job.setJobStatus(JobStatusEnum.FINISH);
				}
				job.setJobRunCount(job.getJobRunCount() + 1);
				if (jobRecordMgrService.update(job)) {
					jobService.addNotifyRobot(job);
				}
			} catch (Exception e) {
				MicrobeLogger.error("jobID-" + job.getId() + "运行出错：", e);
			} finally {
				if (response != null) {
					response.close();
				}
				remove();
			}
		}
		return runResult;
	}

	public void setJobRecordMgrService(IJobRecordMgrService jobRecordMgrService) {
		this.jobRecordMgrService = jobRecordMgrService;
	}

	public void setJobRunRecordMgrService(
			IJobRunRecordMgrService jobRunRecordMgrService) {
		this.jobRunRecordMgrService = jobRunRecordMgrService;
	}

	public void setJobService(IJobService jobService) {
		this.jobService = jobService;
	}

}
