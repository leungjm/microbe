package microbe.scheduler;

import java.util.List;
import java.util.Map;

public interface IRobotManager {

	public Robot getRobot(String uuid);

	public boolean addRobot(Robot robot);

	public boolean addRobots(List<Robot> robots);

	public boolean updateRobot(Robot robot);

	public boolean delRobot(String uuid);

	public boolean testRobot(String uuid);

	public boolean startRobot(String uuid);

	public boolean stopRobot(String uuid);

	public boolean startAllRobots();

	public boolean stopAllRobots();

	public boolean startScheduler();

	public boolean shutScheduler();

	public Robot getRobotByGroupSqlId(String group, Long sqlId);

	public List<Robot> getAllRobots();

	public Map<String, List<Robot>> getAllGroupRobots();

	public List<Robot> getGroupRobots(String group);

	public List<Robot> getGroupRobots(String group, Integer pageIndex,
			Integer pageSize);

	public Integer getGroupRobotsSize(String group);
}
