package microbe.scheduler;

import microbe.scheduler.impl.RobotManager;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.util.MiscUtil;

public class RobotJob implements Job {


	private static IRobotManager robotManager = RobotManager.getInstance();

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobKey jobKey = context.getJobDetail().getKey();

		MicrobeLogger.info("EXECUTE JOB WITH NAME: " + jobKey.getName() + " IN GROUP: "
				+ jobKey.getGroup());
		try {
			Robot robot = robotManager.getRobot(context.getJobDetail().getKey()
					.getName());
			if (robot != null) {
				robot.execute();

				MicrobeLogger.info("FINISHED JOB WITH NAME: " + jobKey.getName()
						+ " IN GROUP: " + jobKey.getGroup());
			} else {
				MicrobeLogger.error("CAN'T FOUND ROBOT WITH NAME: " + jobKey.getName()
						+ " IN GROUP: " + jobKey.getGroup());
			}

		} catch (Exception e) {
			MicrobeLogger.error("EXECUTE JOB BY ERROR WITH NAME: " + jobKey.getName()
					+ " IN GROUP: " + jobKey.getGroup());
			MicrobeLogger.error(MiscUtil.traceInfo(e));
			JobExecutionException exception = new JobExecutionException(e);
			exception.setUnscheduleFiringTrigger(false);

			throw exception;
		}
	}
}
