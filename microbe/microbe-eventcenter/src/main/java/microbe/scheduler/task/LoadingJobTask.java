package microbe.scheduler.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import microbe.model.enums.JobStatusEnum;
import microbe.model.tables.JobRecord;
import microbe.servicecenter.db.IJobRecordMgrService;
import microbe.servicecenter.service.IJobService;
import microbe.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;

import com.lanshi.microbe.Logger.MicrobeLogger;

public class LoadingJobTask extends BaseTask {
	@Autowired
	private IJobRecordMgrService jobRecordMgrService;
	@Autowired
	private IJobService jobService;

	@Override
	public void work() {
		MicrobeLogger.info("init notify job.....");
		List<JobStatusEnum> status = new ArrayList<JobStatusEnum>();
		status.add(JobStatusEnum.RUNNING);
		status.add(JobStatusEnum.WAITING);
		status.add(JobStatusEnum.STOP);
		Date nextDay = DateUtil.getNextDay(DateUtil.getDate(), false);
		List<JobRecord> records = jobRecordMgrService.findByStatusDayBet(
				status, DateUtil.getBeginDateTime(nextDay),
				DateUtil.getEndDateTime(nextDay));

		if (records != null && records.size() > 0) {
			for (JobRecord record : records) {
				if (jobService.addNotifyRobot(record)) {
					record.setJobStatus(JobStatusEnum.RUNNING);
					jobRecordMgrService.update(record);
				} else {
					MicrobeLogger.warn("robot add fail with sqlId={}",
							record.getId());
				}
			}
		}
	}

}
