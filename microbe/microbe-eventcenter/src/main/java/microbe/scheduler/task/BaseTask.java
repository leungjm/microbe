package microbe.scheduler.task;

import javax.annotation.PostConstruct;

import com.lanshi.microbe.Logger.MicrobeLogger;

public abstract class BaseTask implements Runnable {

	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info(" task is running...");
	}

	@Override
	public void run() {
		work();
	}

	public abstract void work();

}
