package microbe.scheduler.impl;

import java.util.Date;

import microbe.scheduler.IScheduler;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.util.MiscUtil;

@Service
public class RobotScheduler implements IScheduler{
	
	private static Scheduler scheduler;
	
	public RobotScheduler() {
		try {
			scheduler = new StdSchedulerFactory().getScheduler();
		} catch (SchedulerException e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}
	}

	@Override
	public boolean start() throws SchedulerException {
		try {
			scheduler.start();
			return scheduler.isStarted();
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't start scheduler!");
			throw e;
		}
	}

	@Override
	public boolean isStarted() throws SchedulerException {
		try {
			return scheduler.isStarted();
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't get scheduler status!");
			throw e;
		}
	}

	@Override
	public boolean shutdown(boolean waitForJobsToComplete)
			throws SchedulerException {
		try {
			scheduler.shutdown(waitForJobsToComplete);
			return scheduler.isShutdown();
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't shutdown scheduler!");
			throw e;
		}
	}

	@Override
	public Date scheduleJob(JobDetail jobDetail, Trigger trigger)
			throws SchedulerException {
		try {
			return scheduler.scheduleJob(jobDetail, trigger);
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't scheduleJob: " + e.getMessage());
			throw e;
		}
	}

	@Override
	public void pauseJob(JobKey jobKey) throws SchedulerException {
		try {
			scheduler.pauseJob(jobKey);
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't pauseJob: " + jobKey.getName() + ", "
					+ jobKey.getGroup());
			throw e;
		}
	}

	@Override
	public void resumeJob(JobKey jobKey) throws SchedulerException {
		try {
			scheduler.resumeJob(jobKey);
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't resumeJob: " + jobKey.getName() + ", "
					+ jobKey.getGroup());
			throw e;
		}
		
	}

	@Override
	public boolean deleteJob(JobKey jobKey) throws SchedulerException {
		try {
			return scheduler.deleteJob(jobKey);
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't deleteJob: " + jobKey.getName() + ", "
					+ jobKey.getGroup());
			throw e;
		}
	}

	@Override
	public void triggerJob(JobKey jobKey) throws SchedulerException {
		try {
			scheduler.triggerJob(jobKey);
		} catch (SchedulerException e) {
			MicrobeLogger.error("Can't triggerJob: " + jobKey.getName() + ", "
					+ jobKey.getGroup());
			throw e;
		}
	}


}
