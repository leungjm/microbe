package microbe.scheduler.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import microbe.scheduler.IRobotManager;
import microbe.scheduler.IScheduler;
import microbe.scheduler.Robot;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.util.MiscUtil;

@Service
public class RobotManager implements IRobotManager, Runnable {

	private Map<String, Robot> robotMap = Collections
			.synchronizedMap(new LinkedHashMap<String, Robot>());

	/**
	 * 数据库ID跟UUID还有Group的映射关系Map Map<String, Map<Integer, String>>-->group :map
	 * Map<Integer, String>-->sqlId:uuid
	 */
	private Map<String, Map<Long, String>> reflectionMap = Collections
			.synchronizedMap(new LinkedHashMap<String, Map<Long, String>>());

	@Autowired
	private IScheduler scheduler;

	private static IRobotManager instance;

	public RobotManager() {
		instance = this;
	}

	public static IRobotManager getInstance() {
		return instance;
	}

	@Override
	public void run() {
		try {
			if (this.scheduler.isStarted()) {
				MicrobeLogger.info("RobotManager schedule " + robotMap.size()
						+ " robots.");
				Set<String> groups = reflectionMap.keySet();
				for (String group : groups) {
					Map<Long, String> map = reflectionMap.get(group);
					if (map != null) {
						MicrobeLogger.info("Group " + group + " contains "
								+ map.size() + " robots:");

						List<Integer> ids = Arrays.asList(map.keySet().toArray(
								new Integer[map.size()]));

						Collections.sort(ids);
						MicrobeLogger.info(ids.toArray().toString());
					}
				}
			} else {
				MicrobeLogger.error("Scheduler is NOT started!");
			}
		} catch (SchedulerException e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}
	}

	@Override
	public Robot getRobotByGroupSqlId(String group, Long sqlId) {
		if (reflectionMap.containsKey(group))
			return getRobot(reflectionMap.get(group).get(sqlId));

		return null;
	}

	@Override
	public Robot getRobot(String uuid) {
		return this.robotMap.get(uuid);
	}

	@Override
	public boolean addRobot(Robot robot) {
		if (this.robotMap.get(robot.getUuid()) == null) {
			try {
				this.robotMap.put(robot.getUuid(), robot);

				this.addReflection(robot);

				this.scheduler.scheduleJob(robot.getJobDetail(),
						robot.getTrigger());

				if (!robot.isActive())
					this.scheduler.pauseJob(robot.getJobDetail().getKey());

				return true;
			} catch (SchedulerException e) {
				MicrobeLogger.error(MiscUtil.traceInfo(e));
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean addRobots(List<Robot> robots) {
		boolean res = true;
		for (Robot robot : robots) {
			res &= addRobot(robot);
		}

		return res;
	}

	@Override
	public boolean updateRobot(Robot robot) {
		if (delRobot(robot.getUuid())) {
			return addRobot(robot);
		}

		return false;
	}

	@Override
	public boolean delRobot(String uuid) {
		try {
			Robot robot = this.robotMap.get(uuid);
			if (robot != null) {
				this.robotMap.remove(uuid);

				this.delReflection(robot);

				return this.scheduler.deleteJob(robot.getJobDetail().getKey());
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
			return false;
		}
	}

	@Override
	public boolean testRobot(String uuid) {
		try {
			Robot robot = this.robotMap.get(uuid);
			if (robot != null) {
				if (robot.isInited()) {
					this.scheduler.triggerJob(robot.getJobDetail().getKey());
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
			return false;
		}
	}

	@Override
	public boolean startRobot(String uuid) {
		Robot robot = this.robotMap.get(uuid);
		if (robot != null) {
			if (!robot.isActive()) {
				if (robot.isInited()) {
					try {
						this.scheduler.resumeJob(robot.getJobDetail().getKey());
						robot.setActive(true);
					} catch (SchedulerException e) {
						MicrobeLogger.error(MiscUtil.traceInfo(e));
						return false;
					}
				} else {
					return false;
				}
			}

			return robot.isActive();
		} else {
			return false;
		}
	}

	@Override
	public boolean stopRobot(String uuid) {
		Robot robot = this.robotMap.get(uuid);
		if (robot != null) {
			if (robot.isActive()) {
				try {
					this.scheduler.pauseJob(robot.getJobDetail().getKey());
					robot.setActive(false);
				} catch (SchedulerException e) {
					MicrobeLogger.error(MiscUtil.traceInfo(e));
					return false;
				}
			}

			return robot.shutDown();
		} else {
			return false;
		}
	}

	@Override
	public boolean startAllRobots() {
		for (Robot robot : this.robotMap.values()) {
			startRobot(robot.getUuid());
		}

		return true;
	}

	@Override
	public boolean stopAllRobots() {
		for (Robot robot : this.robotMap.values()) {
			stopRobot(robot.getUuid());
		}

		return true;
	}

	@Override
	@PostConstruct
	public boolean startScheduler() {
		try {
			return this.scheduler.start();
		} catch (SchedulerException e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
			return false;
		}
	}

	@Override
	@PreDestroy
	public boolean shutScheduler() {
		try {
			stopAllRobots();
			return this.scheduler.shutdown(false);
		} catch (SchedulerException e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
			return false;
		}
	}

	/************************** Private methods **************************/
	private void addReflection(Robot robot) {
		String group = robot.getGroup();
		Map<Long, String> sqlIdAndUuidReflection = this.reflectionMap
				.get(group);
		if (null == sqlIdAndUuidReflection) {
			sqlIdAndUuidReflection = Collections
					.synchronizedMap(new HashMap<Long, String>());
			sqlIdAndUuidReflection.put(robot.getSqlId(), robot.getUuid());
			this.reflectionMap.put(group, sqlIdAndUuidReflection);
		} else {
			sqlIdAndUuidReflection.put(robot.getSqlId(), robot.getUuid());
		}
	}

	private void delReflection(Robot robot) {
		String group = robot.getGroup();
		Map<Long, String> sqlIdAndUuidReflection = this.reflectionMap
				.get(group);
		if (null != sqlIdAndUuidReflection) {
			sqlIdAndUuidReflection.remove(robot.getSqlId());
		}
	}

	// @Override
	// public List<Robot> getAllRobots() {
	// ArrayList<Robot> robots = new ArrayList<Robot>();
	// robots.addAll(robotMap.values());
	//
	// return robots;
	// }

	@Override
	public List<Robot> getAllRobots() {
		List<Robot> robots = new ArrayList<Robot>();
		robots.addAll(robotMap.values());
		return robots;
	}

	@Override
	public Map<String, List<Robot>> getAllGroupRobots() {
		Map<String, List<Robot>> maps = new HashMap<String, List<Robot>>();
		Set<String> groups = reflectionMap.keySet();
		Iterator<String> iter = groups.iterator();
		while (iter.hasNext()) {
			String group = iter.next();
			maps.put(group, getGroupRobots(group));
		}
		return null;
	}

	@Override
	public List<Robot> getGroupRobots(String group) {
		List<Robot> robots = new ArrayList<Robot>();
		Map<Long, String> groupRef = reflectionMap.get(group);
		if (groupRef != null) {
			Iterator<String> iter = groupRef.values().iterator();
			while (iter.hasNext()) {
				String uuidKey = iter.next();
				Robot robot = robotMap.get(uuidKey);
				if (robot != null) {
					robots.add(robot);
				}
			}
		}
		return robots;
	}

	@Override
	public List<Robot> getGroupRobots(String group, Integer pageIndex,
			Integer pageSize) {
		List<Robot> robots = new ArrayList<Robot>();
		Map<Long, String> groupRef = reflectionMap.get(group);
		Integer startIndex = pageIndex * pageSize;
		if (groupRef != null && groupRef.size() >= startIndex) {
			Iterator<String> iter = groupRef.values().iterator();
			Integer flag = 0;
			while (iter.hasNext()) {
				if (flag >= startIndex && flag < pageSize) {
					String uuidKey = iter.next();
					Robot robot = robotMap.get(uuidKey);
					if (robot != null) {
						robots.add(robot);
					}
				} else {
					iter.next();
				}
				flag++;
			}
		}
		return robots;
	}

	@Override
	public Integer getGroupRobotsSize(String group) {
		Map<Long, String> groupRef = reflectionMap.get(group);
		if (groupRef != null) {
			return groupRef.size();
		}
		return 0;
	}
}
