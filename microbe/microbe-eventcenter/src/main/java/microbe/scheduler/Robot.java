package microbe.scheduler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import microbe.scheduler.impl.RobotManager;
import microbe.util.UUIDGeneratorUtil;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.util.MiscUtil;

public abstract class Robot implements Externalizable {

	public final static byte STATE_OK = 1;

	public final static byte STATE_STOP = 0;

	public final static byte STATE_NOT_INIT = -1;

	protected Long sqlId;

	protected String uuid;

	protected String name;

	private String group = Scheduler.DEFAULT_GROUP;

	private Long repeatInterval = Long.MAX_VALUE;

	private Integer repeatCount = SimpleTrigger.REPEAT_INDEFINITELY;

	private Date startTime;

	private Trigger trigger = null;

	private JobDetail jobDetail = null;

	private AtomicBoolean inited = new AtomicBoolean(false);

	private AtomicBoolean active = new AtomicBoolean(false);

	private AtomicBoolean isRunning = new AtomicBoolean(false);

	public Robot(String group) {
		this.group = group;
		this.uuid = UUIDGeneratorUtil.getSimpleUUID();
	}

	public final boolean init() {
		// this.jobDetail = new JobDetail(Integer.toString(sqlId), group,
		// RobotJob.class);

		// this.jobDetail = new JobDetailImpl(uuid, group, RobotJob.class);

		// this.jobDetail = new JobDetailImpl();
		// this.jobDetail.setName(uuid);
		// this.jobDetail.setGroup(group);
		// this.jobDetail.setJobClass(RobotJob.class);

		this.jobDetail = JobBuilder.newJob(RobotJob.class)
				.withIdentity(uuid, group).build();

		this.trigger = genTrigger();
		this.inited.set(true);

		return inited.get();
	}

	public final void execute() {
		if (!this.isRunning.getAndSet(true)) {
			try {
				if (doJob()) {
					MicrobeLogger.info("Robot sqlId:" + this.sqlId + " uuid:" + uuid
							+ " group:" + group + " do job succ!");
				} else {
					MicrobeLogger.info("Robot sqlId:" + this.sqlId + " uuid:" + uuid
							+ " group:" + group + " do job fail!");
				}
			} catch (Exception e) {
				MicrobeLogger.error(MiscUtil.traceInfo(e));
			} finally {
				this.isRunning.set(false);
			}
		} else {
			MicrobeLogger.warn("group:" + group + "  sqlId:" + sqlId + "  uuid:" + uuid
					+ " is still running...");
		}
	}

	public boolean shutDown() {
		return true;
	}

	@Override
	public final void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		this.setSqlId(in.readLong());
		this.setUuid(in.readUTF());
		this.setName(in.readUTF());
		this.setRepeatInterval(in.readLong());
		this.setRepeatCount(in.readInt());
		this.setStartTime(new Date(in.readLong()));
		this.setActive(in.readBoolean());
	}

	@Override
	public final void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(this.getSqlId());
		out.writeUTF(this.getUuid());
		out.writeUTF(this.getName());
		out.writeLong(this.getRepeatInterval());
		out.writeInt(this.getRepeatCount());
		out.writeLong(this.getStartTime().getTime());
		out.writeBoolean(this.isActive());
	}

	protected abstract boolean doJob() throws Exception;

	protected final boolean remove() {
		// return RobotManager.getInstance().delRobot(sqlId);
		return RobotManager.getInstance().delRobot(uuid);
	}

	protected Trigger genTrigger() {
		// SimpleTrigger trigger = new SimpleTriggerImpl(uuid, group,
		// this.getStartTime(), null, this.getRepeatCount(),
		// this.getRepeatInterval());

		// SimpleTriggerImpl trigger = new SimpleTriggerImpl();
		// trigger.setName(uuid);
		// trigger.setGroup(group);
		// trigger.setStartTime(this.getStartTime());
		// trigger.setEndTime(null);
		// trigger.setRepeatCount(this.getRepeatCount());
		// trigger.setRepeatInterval(this.getRepeatInterval());

		Trigger trigger = TriggerBuilder
				.newTrigger()
				.forJob(jobDetail.getKey())
				.withSchedule(
						SimpleScheduleBuilder
								.simpleSchedule()
								.withRepeatCount(this.getRepeatCount())
								.withIntervalInMilliseconds(
										this.getRepeatInterval()))
				.startAt(this.getStartTime()).build();

		return trigger;
	}

	public Long getSqlId() {
		return sqlId;
	}

	public final void setSqlId(Long sqlId) {
		this.sqlId = sqlId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Long getRepeatInterval() {
		return repeatInterval;
	}

	public void setRepeatInterval(Long repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public Integer getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Trigger getTrigger() {
		return trigger;
	}

	public JobDetail getJobDetail() {
		return jobDetail;
	}

	public final boolean isInited() {
		return inited.get();
	}

	public boolean isActive() {
		return active.get();
	}

	public void setActive(boolean active) {
		this.active.set(active);
	}

	public boolean isRunning() {
		return isRunning.get();
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}