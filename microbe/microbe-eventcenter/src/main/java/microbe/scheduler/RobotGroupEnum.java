package microbe.scheduler;

public enum RobotGroupEnum {

	LETOUTNOTIFY("LetoutNotify"), HEALTHINFOPUSH("HealInfoPush"), TEST("Test");

	private String group;

	private RobotGroupEnum(String group) {
		this.group = group;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
}
