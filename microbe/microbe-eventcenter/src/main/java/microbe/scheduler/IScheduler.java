package microbe.scheduler;

import java.util.Date;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

public interface IScheduler {
	
	public boolean start() throws SchedulerException;

	public boolean isStarted() throws SchedulerException;

	public boolean shutdown(boolean waitForJobsToComplete)
			throws SchedulerException;

	public Date scheduleJob(JobDetail jobDetail, Trigger trigger)
			throws SchedulerException;

	public void pauseJob(JobKey jobKey) throws SchedulerException;

	public void resumeJob(JobKey jobKey) throws SchedulerException;

	public boolean deleteJob(JobKey jobKey) throws SchedulerException;

	public void triggerJob(JobKey jobKey) throws SchedulerException;
}
