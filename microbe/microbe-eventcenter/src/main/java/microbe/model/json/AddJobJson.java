package microbe.model.json;

import java.io.Serializable;

import microbe.util.JsonUtil;

public class AddJobJson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String jobGroupModelId;// 第三方所属组的唯一标识
	private String jobNotifyJsonData;// 通知时回传数据
	private String notifyTime;// 首次提醒时间
	private Integer tryFailCount;// 失败尝试次数
	private Integer failTimeInterval;// 失败尝试时间间隔
	private Integer repeatCount;// 循环次数
	private Long repeatTimeInterval;// 重复时间间隔
	private String notifyUrl;// 通知调用的url

	public String getJobGroupModelId() {
		return jobGroupModelId;
	}

	public void setJobGroupModelId(String jobGroupModelId) {
		this.jobGroupModelId = jobGroupModelId;
	}

	public String getJobNotifyJsonData() {
		return jobNotifyJsonData;
	}

	public void setJobNotifyJsonData(String jobNotifyJsonData) {
		this.jobNotifyJsonData = jobNotifyJsonData;
	}

	public String getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(String notifyTime) {
		this.notifyTime = notifyTime;
	}

	public Integer getTryFailCount() {
		return tryFailCount;
	}

	public void setTryFailCount(Integer tryFailCount) {
		this.tryFailCount = tryFailCount;
	}

	public Integer getFailTimeInterval() {
		return failTimeInterval;
	}

	public void setFailTimeInterval(Integer failTimeInterval) {
		this.failTimeInterval = failTimeInterval;
	}

	public Integer getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public Long getRepeatTimeInterval() {
		return repeatTimeInterval;
	}

	public void setRepeatTimeInterval(Long repeatTimeInterval) {
		this.repeatTimeInterval = repeatTimeInterval;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public static void main(String[] args) {
		try {
			System.out.println(JsonUtil.obj2json(new AddJobJson()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
