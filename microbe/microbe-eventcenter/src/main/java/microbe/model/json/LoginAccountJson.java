package microbe.model.json;

import java.util.Date;

import microbe.model.tables.User;

public class LoginAccountJson {

	private String uuid;
	private User user;
	private Date loginTime;
	private String ip;

	public LoginAccountJson() {
	}

	public LoginAccountJson(String uuid, User user, Date loginTime, String ip) {
		this.uuid = uuid;
		this.user = user;
		this.loginTime = loginTime;
		this.ip = ip;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
