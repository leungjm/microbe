package microbe.model.view;

import java.util.Map;

/**
 * 复杂对象
 * 
 * @author admin
 * 
 */
public class ComplexResultJson extends BaseResultJson {
	private Map<String, Object> response;

	public Map<String, Object> getResponse() {
		return response;
	}

	public void setResponse(Map<String, Object> response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "resultCode:" + getResultCode() + ", msg:" + getMsg()
				+ ", responseSize:"
				+ (response != null ? response.size() : "0");
	}

}
