package microbe.model.view;

import microbe.util.GenericToString;

/**
 * 对象结果
 * 
 * @author admin
 * 
 */
public class ObjectResultJson<T> extends BaseResultJson {
	/**
	 * 返回的数据对象
	 */
	private T response;

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "resultCode:" + getResultCode() + ", msg:" + getMsg()
				+ ", response:{" + GenericToString.toString(response) + "}";
	}

	public String toSuperString() {
		return super.toString();
	}
}
