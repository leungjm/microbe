package microbe.model.view;

import java.util.List;

import microbe.util.GenericToString;

import com.lanshi.microbe.repository.hibernate.dao.PageCounter;

/**
 * 分页结果集
 * 
 * @author admin
 * 
 * @param <T>
 */
public class PagerListResultJson<T> extends BaseResultJson {
	/**
	 * 结果集
	 */
	private List<T> response;
	/**
	 * 分页数据
	 */
	private PageCounter pageCounter;

	public List<T> getResponse() {
		return response;
	}

	public void setResponse(List<T> response) {
		this.response = response;
	}

	public PageCounter getPageCounter() {
		return pageCounter;
	}

	public void setPageCounter(PageCounter pageCounter) {
		this.pageCounter = pageCounter;
	}

	@Override
	public String toString() {
		return "resultCode:" + getResultCode() + ", msg:" + getMsg()
				+ ", responseSize:"
				+ (response != null ? response.size() : "0")
				+ ", pageCounter:{" + GenericToString.toString(pageCounter)
				+ "}";
	}

}
