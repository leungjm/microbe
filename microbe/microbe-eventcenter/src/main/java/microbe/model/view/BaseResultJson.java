package microbe.model.view;

/**
 * 基础结果描述
 * 
 * @author admin
 * 
 */
public class BaseResultJson {
	/**
	 * 结果编码
	 */
	private String resultCode;
	/**
	 * 结果描述
	 */
	private String resultDesc;
	/**
	 * 异常原因
	 */
	private String msg;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "resultCode:" + getResultCode() + ",resultDesc:"
				+ getResultDesc() + ",msg:" + getMsg();
	}
}
