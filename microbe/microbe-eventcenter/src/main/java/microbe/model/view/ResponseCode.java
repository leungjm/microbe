package microbe.model.view;

public class ResponseCode {
	public static final String SUCC = "success";
	public static final String FAIL = "fail";
	public static final String UN_LOGIN = "unlogin";
	public static final String UN_ACCETABLE = "unaccetable";
}
