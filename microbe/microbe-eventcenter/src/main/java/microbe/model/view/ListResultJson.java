package microbe.model.view;

import java.util.List;

/**
 * 结果集
 * 
 * @author admin
 * 
 * @param <T>
 */
public class ListResultJson<T> extends BaseResultJson {
	/**
	 * 结果集
	 */
	private List<T> response;

	public List<T> getResponse() {
		return response;
	}

	public void setResponse(List<T> response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "resultCode:" + getResultCode() + ", msg:" + getMsg()
				+ ", responseSize:"
				+ (response != null ? response.size() : "0");
	}

}
