package microbe.model.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import microbe.model.enums.JobStatusEnum;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lanshi.microbe.repository.hibernate.db.BaseModel;

@Entity
@Table(name = "t_jobs_record", catalog = "microbe_event_center")
public class JobRecord extends BaseModel<JobRecord> implements Serializable {

	/**
	 * CREATE TABLE `t_jobs_record` ( `Id` bigint(20) unsigned NOT NULL
	 * AUTO_INCREMENT, `UserId` bigint(20) NOT NULL COMMENT '所属用户ID', `JobGroup`
	 * varchar(30) NOT NULL COMMENT '任务组', `JobNotifyJsonData` text COMMENT
	 * '回传数据', `NotifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT
	 * '通知时间', `TryFailCount` int(11) NOT NULL DEFAULT '0' COMMENT '失败尝试通知次数',
	 * `FailTimeInterval` int(11) NOT NULL DEFAULT '0' COMMENT '失败继续尝试间隔时间，秒',
	 * `RepeatCount` int(11) NOT NULL DEFAULT '0' COMMENT '根据通知时间循环次数',
	 * `RepeatTimeInterval` int(11) NOT NULL DEFAULT '-1' COMMENT '下次循环间隔时间',
	 * `NotifyUrl` varchar(100) NOT NULL COMMENT '通知时调用接口', `JobStatus` int(11)
	 * NOT NULL COMMENT '任务状态', `JobRunCount` int(11) NOT NULL DEFAULT '0'
	 * COMMENT '任务运行次数统计', `UpdateTime` timestamp NOT NULL DEFAULT '0000-00-00
	 * 00:00:00' COMMENT '更新时间', `CreateTime` timestamp NOT NULL DEFAULT
	 * '0000-00-00 00:00:00' COMMENT '创建时间', PRIMARY KEY (`Id`) ) ENGINE=InnoDB
	 * DEFAULT CHARSET=gbk
	 */
	private static final long serialVersionUID = 1L;

	private Long userId;// 所属用户
	private String jobGroup;// 所属组
	private String jobGroupModelId;// 第三方所属组的唯一标识
	private String jobModelDesc;// 第三方所属组的唯一标识
	private String jobNotifyJsonData;// 通知时回传数据
	private Date notifyTime;// 首次提醒时间
	private Integer tryFailCount;// 失败尝试次数
	private Integer failTimeInterval;// 失败尝试时间间隔
	private Integer repeatCount;// 循环次数
	private Integer hadRepeatCount = 0;// 已循环执行次数
	private Long repeatTimeInterval;// 重复时间间隔
	private String notifyUrl;// 通知调用的url
	private JobStatusEnum jobStatus;// 任务状态
	private Integer jobRunCount = 0;// 执行次数

	public JobRecord() {

	}

	public JobRecord(Long userId, String jobGroup, String jobGroupModelId,
			String jobNotifyJsonData, Date notifyTime, Integer tryFailCount,
			Integer failTimeInterval, Integer repeatCount,
			Integer hadRepeatCount, Long repeatTimeInterval, String notifyUrl,
			JobStatusEnum jobStatus, Integer jobRunCount) {
		this.userId = userId;
		this.jobGroup = jobGroup;
		this.jobGroupModelId = jobGroupModelId;
		this.jobNotifyJsonData = jobNotifyJsonData;
		this.notifyTime = notifyTime;
		this.tryFailCount = tryFailCount;
		this.failTimeInterval = failTimeInterval;
		this.repeatCount = repeatCount;
		this.hadRepeatCount = hadRepeatCount;
		this.repeatTimeInterval = repeatTimeInterval;
		this.notifyUrl = notifyUrl;
		this.jobStatus = jobStatus;
		this.jobRunCount = jobRunCount;
	}

	@Column(name = "UserId", nullable = false)
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "JobGroup", nullable = false, length = 30)
	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	@Column(name = "JobGroupModelId", nullable = false, length = 30)
	public String getJobGroupModelId() {
		return jobGroupModelId;
	}

	public void setJobGroupModelId(String jobGroupModelId) {
		this.jobGroupModelId = jobGroupModelId;
	}

	@Column(name = "JobModelDesc", length = 50)
	public String getJobModelDesc() {
		return jobModelDesc;
	}

	public void setJobModelDesc(String jobModelDesc) {
		this.jobModelDesc = jobModelDesc;
	}

	@Column(name = "JobNotifyJsonData", nullable = true)
	public String getJobNotifyJsonData() {
		return jobNotifyJsonData;
	}

	public void setJobNotifyJsonData(String jobNotifyJsonData) {
		this.jobNotifyJsonData = jobNotifyJsonData;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = "GMT+8")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NotifyTime", length = 19, nullable = false)
	public Date getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(Date notifyTime) {
		this.notifyTime = notifyTime;
	}

	@Column(name = "TryFailCount", nullable = false)
	public Integer getTryFailCount() {
		return tryFailCount;
	}

	public void setTryFailCount(Integer tryFailCount) {
		this.tryFailCount = tryFailCount;
	}

	@Column(name = "FailTimeInterval", nullable = false)
	public Integer getFailTimeInterval() {
		return failTimeInterval;
	}

	public void setFailTimeInterval(Integer failTimeInterval) {
		this.failTimeInterval = failTimeInterval;
	}

	@Column(name = "RepeatCount", nullable = false)
	public Integer getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	@Column(name = "HadRepeatCount", nullable = false)
	public Integer getHadRepeatCount() {
		return hadRepeatCount;
	}

	public void setHadRepeatCount(Integer hadRepeatCount) {
		this.hadRepeatCount = hadRepeatCount;
	}

	@Column(name = "RepeatTimeInterval", nullable = false)
	public Long getRepeatTimeInterval() {
		return repeatTimeInterval;
	}

	public void setRepeatTimeInterval(Long repeatTimeInterval) {
		this.repeatTimeInterval = repeatTimeInterval;
	}

	@Column(name = "NotifyUrl", nullable = false)
	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Column(name = "JobStatus", nullable = false)
	public JobStatusEnum getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(JobStatusEnum jobStatus) {
		this.jobStatus = jobStatus;
	}

	@Column(name = "JobRunCount", nullable = false)
	public Integer getJobRunCount() {
		return jobRunCount;
	}

	public void setJobRunCount(Integer jobRunCount) {
		this.jobRunCount = jobRunCount;
	}

	@Override
	public int compareTo(JobRecord o) {
		return this.id.compareTo(o.id);
	}

}
