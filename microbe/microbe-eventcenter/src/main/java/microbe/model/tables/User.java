package microbe.model.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.lanshi.microbe.repository.hibernate.db.BaseModel;

@Entity
@Table(name = "t_users")
public class User extends BaseModel<User> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userName;
	private String password;
	private String group;

	public User() {

	}

	public User(String userName, String password, String group) {
		this.userName = userName;
		this.password = password;
		this.group = group;
	}

	@Column(name = "UserName", unique = true, nullable = false, length = 30)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "Password", nullable = false, length = 100)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "UserGroup", nullable = false, length = 30)
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public int compareTo(User o) {
		return this.id.compareTo(o.id);
	}

}
