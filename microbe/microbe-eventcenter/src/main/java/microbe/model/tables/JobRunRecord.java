package microbe.model.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import microbe.model.enums.OperatorTypeEnum;
import microbe.model.enums.RunResultEnum;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lanshi.microbe.repository.hibernate.db.BaseModel;

@Entity
@Table(name = "t_job_run_record", catalog = "microbe_event_center")
public class JobRunRecord extends BaseModel<JobRunRecord> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long jobId;
	private Date runTime;
	private RunResultEnum runResult;
	private String returnCode;
	private String returnMsg;
	private OperatorTypeEnum operatorType;
	private Long operatorId;

	public JobRunRecord() {
	}

	public JobRunRecord(Long jobId, Date runTime, RunResultEnum runResult,
			String returnCode, String returnMsg, OperatorTypeEnum operatorType,
			Long operatorId) {
		super();
		this.jobId = jobId;
		this.runTime = runTime;
		this.runResult = runResult;
		this.returnCode = returnCode;
		this.returnMsg = returnMsg;
		this.operatorType = operatorType;
		this.operatorId = operatorId;
	}

	@Column(name = "JobId", nullable = false)
	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = "GMT+8")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RunTime", length = 19, nullable = false)
	public Date getRunTime() {
		return runTime;
	}

	public void setRunTime(Date runTime) {
		this.runTime = runTime;
	}

	@Column(name = "RunResult")
	public RunResultEnum getRunResult() {
		return runResult;
	}

	public void setRunResult(RunResultEnum runResult) {
		this.runResult = runResult;
	}

	@Column(name = "ReturnCode")
	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@Column(name = "ReturnMsg")
	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	@Column(name = "OperatorType", nullable = false)
	public OperatorTypeEnum getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(OperatorTypeEnum operatorType) {
		this.operatorType = operatorType;
	}

	@Column(name = "OperaterId")
	public Long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	@Override
	public int compareTo(JobRunRecord o) {
		return this.id.compareTo(o.id);
	}

}
