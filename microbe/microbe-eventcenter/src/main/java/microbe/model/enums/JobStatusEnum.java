package microbe.model.enums;

public enum JobStatusEnum {
	/**
	 * 运行中
	 */
	RUNNING,
	/**
	 * 等待运行
	 */
	WAITING,
	/**
	 * 暂停
	 */
	STOP,
	/**
	 * 取消
	 */
	CANCEL,
	/**
	 * 完成
	 */
	FINISH;

}
