package microbe.model.enums;

public enum OperatorTypeEnum {
	ROBOT, STOP, RESTART, DELETE;
}
