package microbe.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.lanshi.microbe.Logger.MicrobeLogger;

public class MD5HashUtil {

	private static MessageDigest md = null;

	static {
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			MicrobeLogger.error("", e);
		}
	}

	public static String hashCode(String dataToHash) {
		return hashData(dataToHash.getBytes(), false);
	}

	public static String hashCode(String dataToHash, boolean isLowerCase) {
		return hashData(dataToHash.getBytes(), isLowerCase);
	}

	public static String hashCode(String dataToHash, String charsetName) {
		try {
			return hashData(dataToHash.getBytes(charsetName), false);
		} catch (UnsupportedEncodingException e) {
			MicrobeLogger.error("", e);
		}

		return null;
	}

	public static String hashCode(String dataToHash, String charsetName,
			boolean isLowerCase) {
		try {
			return hashData(dataToHash.getBytes(charsetName), isLowerCase);
		} catch (UnsupportedEncodingException e) {
			MicrobeLogger.error("", e);
		}

		return null;
	}

	public static String hashCode(byte[] dataToHash, boolean isLowerCase) {
		return hashData(dataToHash, isLowerCase);
	}

	private static String hashData(byte[] dataToHash, boolean isLowerCase) {
		if (isLowerCase)
			return bytes2Hex((calculateHash(dataToHash))).toLowerCase();
		else
			return bytes2Hex((calculateHash(dataToHash))).toUpperCase();
	}

	private static byte[] calculateHash(byte[] dataToHash) {
		md.update(dataToHash, 0, dataToHash.length);
		return (md.digest());
	}

	private static String bytes2Hex(byte[] bts) {
		String des = "";
		String tmp = null;
		for (int i = 0; i < bts.length; i++) {
			tmp = (Integer.toHexString(bts[i] & 0xFF));
			if (tmp.length() == 1) {
				des += "0";
			}
			des += tmp;
		}

		return des;
	}

	public static void main(String[] args) {
		String strSrc = "2016-06-21 15:47:53114����610034443";
		String md5_32 = hashCode(strSrc, Boolean.FALSE);
		System.out.println(md5_32);
	}
}
