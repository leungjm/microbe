package microbe.util;

import java.lang.reflect.Field;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.util.MiscUtil;

public class GenericToString {

	private static String EMPTY = "";

	public static String toString(Object obj) {
		StringBuffer sb = new StringBuffer();

		try {
			if (obj != null) {
				Field[] fields = obj.getClass().getDeclaredFields();
				if (fields != null) {
					for (Field field : fields) {
						field.setAccessible(true);
						sb.append(',');
						sb.append(field.getName());
						sb.append('=');
						sb.append(field.get(obj));
					}
				}
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return sb.length() > 0 ? sb.substring(1) : EMPTY;
	}
}
