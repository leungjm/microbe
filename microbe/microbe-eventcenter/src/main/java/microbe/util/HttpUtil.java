package microbe.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.StandardHttpRequestRetryHandler;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.DefaultCookieSpec;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.Args;
import org.apache.http.util.EntityUtils;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.util.MiscUtil;

public class HttpUtil {

	private static volatile HttpUtil instance;

	private volatile CloseableHttpClient client;

	private volatile PoolingHttpClientConnectionManager connManager;

	private volatile CookieStore cookieStore;

	private volatile HttpRequestRetryHandler retryHandler;

	private volatile RequestConfig reqConfig;

	private final String defaultContentType = "application/x-www-form-urlencoded;charset=";

	private final String defaultEncoding = "UTF-8";

	private HttpUtil() {
		RegistryBuilder<ConnectionSocketFactory> registryBuilder = RegistryBuilder
				.<ConnectionSocketFactory> create();

		ConnectionSocketFactory plainSF = new PlainConnectionSocketFactory();
		registryBuilder.register("http", plainSF);

		// 指定信任密钥存储对象和连接套接字工厂
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			SSLContext sslContext = SSLContexts.custom().useProtocol("TLS")
					.loadTrustMaterial(trustStore, new AnyTrustStrategy())
					.build();
			LayeredConnectionSocketFactory sslSF = new SSLConnectionSocketFactory(
					sslContext, NoopHostnameVerifier.INSTANCE);
			registryBuilder.register("https", sslSF);
		} catch (KeyStoreException e) {
			throw new RuntimeException(e);
		} catch (KeyManagementException e) {
			throw new RuntimeException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		Registry<ConnectionSocketFactory> registry = registryBuilder.build();

		// 设置连接管理器
		connManager = new PoolingHttpClientConnectionManager(registry);

		// 设置连接参数
		ConnectionConfig connConfig = ConnectionConfig.custom()
				.setCharset(Charset.forName(defaultEncoding)).build();
		connManager.setDefaultConnectionConfig(connConfig);

		// SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(10000)
		// .build();
		// connManager.setDefaultSocketConfig(socketConfig);

		connManager.setDefaultMaxPerRoute(30);
		connManager.setMaxTotal(500);

		// 指定cookie存储对象
		cookieStore = new BasicCookieStore();

		retryHandler = new StandardHttpRequestRetryHandler(1, true);

		// 构建客户端
		client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore)
				.setConnectionManager(connManager)
				.setRetryHandler(retryHandler).build();

		reqConfig = this.getRequestConfig(5000, 60000);
	}

	public static HttpUtil getInstance() {
		synchronized (HttpUtil.class) {
			if (HttpUtil.instance == null) {
				instance = new HttpUtil();
			}
			return instance;
		}
	}

	public String doGetForString(String url, Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doGetForResponse(url,
					(String) null, defaultEncoding, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doGetForString(String url, int connectTimeout,
			int socketTimeout, Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doGetForResponse(url,
					(String) null, defaultEncoding, connectTimeout,
					socketTimeout, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doGetForString(String url, String query,
			Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doGetForResponse(url, query,
					defaultEncoding, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doGetForString(String url, String query, int connectTimeout,
			int socketTimeout, Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doGetForResponse(url, query,
					defaultEncoding, connectTimeout, socketTimeout, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doGetForString(String url, Map<String, String> queryParams,
			Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doGetForResponse(url,
					queryParams, defaultEncoding, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doGetForString(String url, Map<String, String> queryParams,
			int connectTimeout, int socketTimeout,
			Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doGetForResponse(url,
					queryParams, defaultEncoding, connectTimeout,
					socketTimeout, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	private CloseableHttpResponse doGetForResponse(String url, String query,
			String encoding, Map<String, String> headerAttr) {
		return this.doGetForResponse(url, query, encoding, reqConfig,
				headerAttr);
	}

	private CloseableHttpResponse doGetForResponse(String url, String query,
			String encoding, int connectTimeout, int socketTimeout,
			Map<String, String> headerAttr) {
		return this.doGetForResponse(url, query, encoding,
				this.getRequestConfig(connectTimeout, socketTimeout),
				headerAttr);
	}

	private CloseableHttpResponse doGetForResponse(String url, String query,
			String encoding, RequestConfig reqConfig,
			Map<String, String> headerAttr) {
		try {
			HttpGet get = new HttpGet();
			get.setConfig(reqConfig);
			get.addHeader("Connection", "close");
			get.addHeader("Content-Type", defaultContentType.concat(encoding));
			if (headerAttr != null) {
				for (String key : headerAttr.keySet()) {
					get.addHeader(key, headerAttr.get(key));
				}
			}
			URIBuilder builder = new URIBuilder(url);
			builder.setCharset(Charset.forName(encoding));
			// 填入查询参数
			if (!StringUtil.isEmptyStr(query)) {
				builder.setCustomQuery(query);
			}
			get.setURI(builder.build());

			return client.execute(get);
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	private CloseableHttpResponse doGetForResponse(String url,
			Map<String, String> queryParams, String encoding,
			Map<String, String> headerAttr) {
		return this.doGetForResponse(url, queryParams, encoding, reqConfig,
				headerAttr);
	}

	private CloseableHttpResponse doGetForResponse(String url,
			Map<String, String> queryParams, String encoding,
			int connectTimeout, int socketTimeout,
			Map<String, String> headerAttr) {
		return this.doGetForResponse(url, queryParams, encoding,
				this.getRequestConfig(connectTimeout, socketTimeout),
				headerAttr);
	}

	private CloseableHttpResponse doGetForResponse(String url,
			Map<String, String> queryParams, String encoding,
			RequestConfig reqConfig, Map<String, String> headerAttr) {
		try {
			HttpGet get = new HttpGet();
			get.setConfig(reqConfig);
			get.addHeader("Connection", "close");
			get.addHeader("Content-Type", defaultContentType.concat(encoding));
			if (headerAttr != null) {
				for (String key : headerAttr.keySet()) {
					get.addHeader(key, headerAttr.get(key));
				}
			}
			URIBuilder builder = new URIBuilder(url);
			builder.setCharset(Charset.forName(encoding));
			// 填入查询参数
			if (queryParams != null && !queryParams.isEmpty()) {
				builder.setParameters(paramsConverter(queryParams));
			}
			get.setURI(builder.build());

			return client.execute(get);
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doPostForString(String url, String query,
			Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doPostForResponse(url, query,
					defaultEncoding, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doPostForString(String url, String query, int connectTimeout,
			int socketTimeout, Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doPostForResponse(url, query,
					defaultEncoding, connectTimeout, socketTimeout, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doPostForString(String url, Map<String, String> queryParams,
			Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doPostForResponse(url,
					queryParams, null, defaultEncoding, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doPostForString(String url, Map<String, String> queryParams,
			int connectTimeout, int socketTimeout,
			Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doPostForResponse(url,
					queryParams, null, defaultEncoding, connectTimeout,
					socketTimeout, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doPostForString(String url, Map<String, String> queryParams,
			Map<String, String> formParams, Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doPostForResponse(url,
					queryParams, formParams, defaultEncoding, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public String doPostForString(String url, Map<String, String> queryParams,
			Map<String, String> formParams, int connectTimeout,
			int socketTimeout, Map<String, String> headerAttr) {
		try {
			CloseableHttpResponse response = this.doPostForResponse(url,
					queryParams, formParams, defaultEncoding, connectTimeout,
					socketTimeout, headerAttr);
			if (response != null) {
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null) {
					result = MyEntityUtils.toString(entity, defaultEncoding);
				}
				response.close();

				return result;
			}
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	private CloseableHttpResponse doPostForResponse(String url, String query,
			String encoding, Map<String, String> headerAttr) {
		return this.doPostForResponse(url, query, encoding, reqConfig,
				headerAttr);
	}

	private CloseableHttpResponse doPostForResponse(String url, String query,
			String encoding, int connectTimeout, int socketTimeout,
			Map<String, String> headerAttr) {
		return this.doPostForResponse(url, query, encoding,
				this.getRequestConfig(connectTimeout, socketTimeout),
				headerAttr);
	}

	private CloseableHttpResponse doPostForResponse(String url, String query,
			String encoding, RequestConfig reqConfig,
			Map<String, String> headerAttr) {
		try {
			HttpPost post = new HttpPost(url);
			post.setConfig(reqConfig);
			if (headerAttr != null) {
				for (String key : headerAttr.keySet()) {
					post.addHeader(key, headerAttr.get(key));
				}
			}
			if (!StringUtil.isEmptyStr(query)) {
				StringEntity se = new StringEntity(query, encoding);
				post.setEntity(se);
			}

			return client.execute(post);
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	public CloseableHttpResponse doPostForResponse(String url,
			Map<String, String> queryParams, Map<String, String> formParams,
			String encoding, Map<String, String> headerAttr) {
		return this.doPostForResponse(url, queryParams, formParams, encoding,
				reqConfig, headerAttr);
	}

	public CloseableHttpResponse doPostForResponse(String url,
			Map<String, String> queryParams, Map<String, String> formParams,
			String encoding, int connectTimeout, int socketTimeout,
			Map<String, String> headerAttr) {
		return this.doPostForResponse(url, queryParams, formParams, encoding,
				this.getRequestConfig(connectTimeout, socketTimeout),
				headerAttr);
	}

	private CloseableHttpResponse doPostForResponse(String url,
			Map<String, String> queryParams, Map<String, String> formParams,
			String encoding, RequestConfig reqConfig,
			Map<String, String> headerAttr) {
		try {
			HttpPost post = new HttpPost();
			post.setConfig(reqConfig);
			post.addHeader("Connection", "close");
			post.addHeader("Content-Type", defaultContentType.concat(encoding));
			if (headerAttr != null) {
				for (String key : headerAttr.keySet()) {
					post.addHeader(key, headerAttr.get(key));
				}
			}
			URIBuilder builder = new URIBuilder(url);
			builder.setCharset(Charset.forName(encoding));
			// 填入查询参数
			if (queryParams != null && !queryParams.isEmpty()) {
				builder.setParameters(paramsConverter(queryParams));
			}
			post.setURI(builder.build());

			// 填入表单参数
			if (formParams != null && !formParams.isEmpty()) {
				post.setEntity(new UrlEncodedFormEntity(
						paramsConverter(formParams), encoding));
			}

			return client.execute(post);
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	@SuppressWarnings("unused")
	private CloseableHttpResponse multipartPostForResponse(String url,
			Map<String, String> queryParams, List<FormBodyPart> formParts,
			String encoding) {
		try {
			HttpPost post = new HttpPost();
			post.setConfig(reqConfig);
			post.addHeader("Connection", "close");
			post.addHeader("Content-Type",
					"multipart/form-data;charset=".concat(encoding));

			URIBuilder builder = new URIBuilder(url);
			builder.setCharset(Charset.forName(encoding));
			// 填入查询参数
			if (queryParams != null && !queryParams.isEmpty()) {
				builder.setParameters(paramsConverter(queryParams));
			}
			post.setURI(builder.build());
			// 填入表单参数
			if (formParts != null && !formParts.isEmpty()) {
				MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
						.create();
				entityBuilder = entityBuilder
						.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				for (FormBodyPart formPart : formParts) {
					entityBuilder = entityBuilder.addPart(formPart.getName(),
							formPart.getBody());
				}
				post.setEntity(entityBuilder.build());
			}

			return client.execute(post);
		} catch (Exception e) {
			MicrobeLogger.error(MiscUtil.traceInfo(e));
		}

		return null;
	}

	/**
	 * 获取当前Http客户端状态中的Cookie
	 * 
	 * @param domain
	 *            作用域
	 * @param port
	 *            端口 传null 默认80
	 * @param path
	 *            Cookie路径 传null 默认"/"
	 * @param useSecure
	 *            Cookie是否采用安全机制 传null 默认false
	 * @return
	 */
	public Map<String, Cookie> getCookie(String domain, Integer port,
			String path, Boolean useSecure) {
		if (domain == null) {
			return null;
		}
		if (port == null) {
			port = 80;
		}
		if (path == null) {
			path = "/";
		}
		if (useSecure == null) {
			useSecure = false;
		}
		List<Cookie> cookies = cookieStore.getCookies();
		if (cookies == null || cookies.isEmpty()) {
			return null;
		}

		CookieOrigin origin = new CookieOrigin(domain, port, path, useSecure);
		DefaultCookieSpec cookieSpec = new DefaultCookieSpec();
		Map<String, Cookie> retVal = new HashMap<String, Cookie>();
		for (Cookie cookie : cookies) {
			if (cookieSpec.match(cookie, origin)) {
				retVal.put(cookie.getName(), cookie);
			}
		}

		return retVal;
	}

	/**
	 * 设置单个Cookie
	 * 
	 * @param key
	 *            Cookie键
	 * @param value
	 *            Cookie值
	 * @param domain
	 *            作用域 不可为空
	 * @param path
	 *            路径 传null默认为"/"
	 * @param useSecure
	 *            是否使用安全机制 传null 默认为false
	 * @return 是否成功设置cookie
	 */
	public boolean setCookie(String key, String value, String domain,
			String path, Boolean useSecure) {
		Map<String, String> cookies = new HashMap<String, String>();
		cookies.put(key, value);

		return setCookie(cookies, domain, path, useSecure);
	}

	/**
	 * 批量设置Cookie
	 * 
	 * @param cookies
	 *            cookie键值对图
	 * @param domain
	 *            作用域 不可为空
	 * @param path
	 *            路径 传null默认为"/"
	 * @param useSecure
	 *            是否使用安全机制 传null 默认为false
	 * @return 是否成功设置cookie
	 */
	public boolean setCookie(Map<String, String> cookies, String domain,
			String path, Boolean useSecure) {
		synchronized (cookieStore) {
			if (domain == null) {
				return false;
			}
			if (path == null) {
				path = "/";
			}
			if (useSecure == null) {
				useSecure = false;
			}
			if (cookies == null || cookies.isEmpty()) {
				return true;
			}
			Set<Entry<String, String>> set = cookies.entrySet();
			String key = null;
			String value = null;
			for (Entry<String, String> entry : set) {
				key = entry.getKey();
				if (key == null || key.isEmpty() || value == null
						|| value.isEmpty()) {
					throw new IllegalArgumentException(
							"cookies key and value both can not be empty");
				}
				BasicClientCookie cookie = new BasicClientCookie(key, value);
				cookie.setDomain(domain);
				cookie.setPath(path);
				cookie.setSecure(useSecure);
				cookieStore.addCookie(cookie);
			}

			return true;
		}
	}

	public static String entity2String(CloseableHttpResponse response,
			String defaultCharset) {
		String result = null;
		try {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				result = MyEntityUtils.toString(entity, defaultCharset);
			}
			response.close();
		} catch (Exception e) {
			MicrobeLogger.error("", e);
		}

		return result;
	}

	private RequestConfig getRequestConfig(int connectTimeout, int socketTimeout) {
		RequestConfig requestConfig = RequestConfig.custom()
				.setConnectTimeout(connectTimeout)
				.setSocketTimeout(socketTimeout).build();

		return requestConfig;
	}

	private List<NameValuePair> paramsConverter(Map<String, String> params) {
		List<NameValuePair> nvps = new LinkedList<NameValuePair>();
		Set<Entry<String, String>> paramsSet = params.entrySet();
		for (Entry<String, String> paramEntry : paramsSet) {
			nvps.add(new BasicNameValuePair(paramEntry.getKey(), paramEntry
					.getValue()));
		}

		return nvps;
	}
}

class AnyTrustStrategy implements TrustStrategy {

	@Override
	public boolean isTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		return true;
	}
}

class MyEntityUtils {

	private MyEntityUtils() {
	}

	public static void consumeQuietly(final HttpEntity entity) {
		EntityUtils.consumeQuietly(entity);
	}

	public static void consume(final HttpEntity entity) throws IOException {
		EntityUtils.consume(entity);
	}

	public static void updateEntity(final HttpResponse response,
			final HttpEntity entity) throws IOException {
		EntityUtils.updateEntity(response, entity);
	}

	public static byte[] toByteArray(final HttpEntity entity)
			throws IOException {
		return EntityUtils.toByteArray(entity);
	}

	public static String getContentCharSet(final HttpEntity entity)
			throws ParseException {
		Args.notNull(entity, "Entity");
		String charset = null;
		ContentType contentType = ContentType.get(entity);
		if (contentType != null) {
			charset = contentType.getCharset().displayName();
		}

		return charset;
	}

	public static String getContentMimeType(final HttpEntity entity)
			throws ParseException {
		Args.notNull(entity, "Entity");
		String mimeType = null;
		ContentType contentType = ContentType.get(entity);
		if (contentType != null) {
			mimeType = contentType.getMimeType();
		}

		return mimeType;
	}

	public static String toString(final HttpEntity entity,
			final Charset defaultCharset) throws IOException, ParseException {
		Args.notNull(entity, "Entity");
		final InputStream instream = entity.getContent();
		if (instream == null) {
			return null;
		}
		try {
			Args.check(entity.getContentLength() <= Integer.MAX_VALUE,
					"HTTP entity too large to be buffered in memory");
			int i = (int) entity.getContentLength();
			if (i < 0) {
				i = 4096;
			}

			Charset charset = null;

			if (defaultCharset != null) {
				charset = defaultCharset;
			} else {
				try {
					final ContentType contentType = ContentType.get(entity);
					if (contentType != null) {
						charset = contentType.getCharset();
					}
				} catch (final UnsupportedCharsetException ex) {
					throw new UnsupportedEncodingException(ex.getMessage());
				}
			}

			if (charset == null) {
				charset = HTTP.DEF_CONTENT_CHARSET;
			}

			// final Reader reader = new InputStreamReader(instream, charset);
			// final CharArrayBuffer buffer = new CharArrayBuffer(i);
			// final char[] tmp = new char[1024];
			// int l;
			// while ((l = reader.read(tmp)) != -1) {
			// buffer.append(tmp, 0, l);
			// }
			// return buffer.toString();

			final StringBuffer sb = new StringBuffer();
			final Scanner scan = new Scanner(instream, charset.displayName());
			// 读取远程主机的内容
			while (scan.hasNextLine()) {
				sb.append(scan.nextLine());
			}

			return sb.toString();
		} finally {
			instream.close();
		}
	}

	public static String toString(final HttpEntity entity,
			final String defaultCharset) throws IOException, ParseException {
		return toString(entity,
				defaultCharset != null ? Charset.forName(defaultCharset) : null);
	}

	public static String toString(final HttpEntity entity) throws IOException,
			ParseException {
		return toString(entity, (Charset) null);
	}
}