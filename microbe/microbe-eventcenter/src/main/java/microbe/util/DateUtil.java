package microbe.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static final String HH = "HH";

	public static final String HHMM = "HH:mm";

	public static final String HHMMSS = "HH:mm:ss";

	public static final String YYYYMMDD = "yyyy-MM-dd";

	public static final String YYYYMMDD_S = "yyyyMMdd";

	public static final String YYYYMMDDHHMM = "yyyy-MM-dd HH:mm";

	public static final String YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";

	public static final String YYYYMMDDHHMMSS_S = "yyyyMMddHHmmss";

	public static final String YYYYMMDDHHMMSSSSS = "yyyy-MM-dd HH:mm:ss,SSS";

	public static final String YYYYMMDDHHMMSSSSS_S = "yyyyMMddHHmmssSSS";

	public static synchronized Date getDate() {
		return new Date();
	}

	public static synchronized Date getDate(long millis) {
		return new Date(millis);
	}

	public static synchronized Calendar getCalendar() {
		return Calendar.getInstance();
	}

	public static synchronized Calendar getCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal;
	}

	public static synchronized String getCurrDateTimeFormat() {
		return getDateTimeFormat(getDate());
	}

	public static synchronized String getHourFormat(Date date) {
		return parseDateFormat(date, HH);
	}

	public static synchronized String getHourMinFormat(Date date) {
		return parseDateFormat(date, HHMM);
	}

	public static synchronized Calendar parseCalendarHourMinFormat(
			String strDate) throws ParseException {
		return parseCalendarFormat(strDate, HHMM);
	}

	public static synchronized String getDateTimeFormat(Date date) {
		return parseDateFormat(date, HHMMSS);
	}

	public static synchronized Date parseDateTimeFormat(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, HHMMSS);
	}

	public static synchronized Calendar parseCalendarTimeFormat(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, HHMMSS);
	}

	public static synchronized String getCurrDateDayFormat() {
		return getDateDayFormat(getDate());
	}

	public static synchronized String getDateDayFormat(Date date) {
		return parseDateFormat(date, YYYYMMDD);
	}

	public static synchronized String getDateDayFormat2(Date date) {
		return parseDateFormat(date, YYYYMMDD_S);
	}

	public static synchronized Date parseDateDayFormat(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDD);
	}

	public static synchronized Date parseDateDayFormat2(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDD_S);
	}

	public static synchronized Calendar parseCalendarDayFormat(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDD);
	}

	public static synchronized Calendar parseCalendarDayFormat2(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDD_S);
	}

	public static synchronized String getCurrDateMinuteFormat() {
		return getDateMinuteFormat(getDate());
	}

	public static synchronized String getDateMinuteFormat(Date date) {
		return parseDateFormat(date, YYYYMMDDHHMM);
	}

	public static synchronized Date parseDateMinuteFormat(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDDHHMM);
	}

	public static synchronized Calendar parseCalendarMinuteFormat(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDDHHMM);
	}

	public static synchronized String getCurrDateSecondFormat() {
		return getDateSecondFormat(getDate());
	}

	public static synchronized String getCurrDateSecondFormat2() {
		return getDateSecondFormat2(getDate());
	}

	public static synchronized String getDateSecondFormat(Date date) {
		return parseDateFormat(date, YYYYMMDDHHMMSS);
	}

	public static synchronized String getDateSecondFormat2(Date date) {
		return parseDateFormat(date, YYYYMMDDHHMMSS_S);
	}

	public static synchronized Date parseDateSecondFormat(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDDHHMMSS);
	}

	public static synchronized Date parseDateSecondFormat2(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDDHHMMSS_S);
	}

	public static synchronized Calendar parseCalendarSecondFormat(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDDHHMMSS);
	}

	public static synchronized Calendar parseCalendarSecondFormat2(
			String strDate) throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDDHHMMSS_S);
	}

	public static synchronized String getCurrDateMilliFormat() {
		return getDateMilliFormat(getDate());
	}

	public static synchronized String getDateMilliFormat(Date date) {
		return parseDateFormat(date, YYYYMMDDHHMMSSSSS);
	}

	public static synchronized String getDateMilliFormat2(Date date) {
		return parseDateFormat(date, YYYYMMDDHHMMSSSSS_S);
	}

	public static synchronized Date parseDateMilliFormat(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDDHHMMSSSSS);
	}

	public static synchronized Date parseDateMilliFormat2(String strDate)
			throws ParseException {
		return parseDateFormat(strDate, YYYYMMDDHHMMSSSSS_S);
	}

	public static synchronized Calendar parseCalendarMilliFormat(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDDHHMMSSSSS);
	}

	public static synchronized Calendar parseCalendarMilliFormat2(String strDate)
			throws ParseException {
		return parseCalendarFormat(strDate, YYYYMMDDHHMMSSSSS_S);
	}

	public static synchronized String parseDateFormat(Date date, String pattern) {
		if (date == null)
			return "";

		SimpleDateFormat sdf = new SimpleDateFormat();
		if (pattern != null)
			sdf.applyPattern(pattern);

		return sdf.format(date);
	}

	public static synchronized Date parseDateFormat(String strDate,
			String pattern) throws ParseException {
		if (StringUtil.isEmptyStr(strDate))
			return null;

		SimpleDateFormat sdf = new SimpleDateFormat();
		if (pattern != null)
			sdf.applyPattern(pattern);

		return sdf.parse(strDate);
	}

	public static synchronized Calendar parseCalendarFormat(String strDate,
			String pattern) throws ParseException {
		if (StringUtil.isEmptyStr(strDate))
			return null;

		SimpleDateFormat sdf = new SimpleDateFormat();
		if (pattern != null)
			sdf.applyPattern(pattern);
		sdf.parse(strDate);

		return (Calendar) sdf.getCalendar().clone();
	}

	public static synchronized boolean isLeapYear() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return isLeapYear(year);
	}

	public static synchronized int getYear() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return year;
	}

	public static synchronized boolean isLeapYear(Date date) {
		Calendar gc = Calendar.getInstance();
		gc.setTime(date);
		return isLeapYear(gc.get(Calendar.YEAR));
	}

	public static synchronized boolean isLeapYear(int year) {
		/**
		 * 详细设计： 1.被400整除是闰年，否则： 2.不能被4整除则不是闰年 3.能被4整除同时不能被100整除则是闰年
		 * 3.能被4整除同时能被100整除则不是闰年
		 */
		if ((year % 400) == 0)
			return true;
		else if ((year % 4) == 0) {
			if ((year % 100) == 0)
				return false;
			else
				return true;
		} else
			return false;
	}

	// 获取上下午，0 上午，1 下午
	public static synchronized int getAmPmByTimeRange(String strDate)
			throws ParseException {
		Date date = parseDateFormat(strDate, HHMM);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int res = cal.get(Calendar.AM_PM);

		return res;
	}

	/**
	 * 把yyyyMMddHHmmss字符串格式的日期转换成TimeStamp
	 * 
	 * @return
	 */
	public static Timestamp str2TimeStamp14(String ts) {
		if (null == ts || ts.trim().length() != 14)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(ts.substring(0, 4));
		sb.append("-");
		sb.append(ts.substring(4, 6));
		sb.append("-");
		sb.append(ts.substring(6, 8));
		sb.append(" 00:00:00.0");

		return Timestamp.valueOf(sb.toString());
	}

	/**
	 * 把yyyymmdd字符串格式的日期转换成TimeStamp
	 * 
	 * @return
	 */
	public static Timestamp str2TimeStamp(String ts) {
		if (null == ts || ts.trim().length() != 8)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(ts.substring(0, 4));
		sb.append("-");
		sb.append(ts.substring(4, 6));
		sb.append("-");
		sb.append(ts.substring(6, 8));
		sb.append(" 00:00:00");

		return Timestamp.valueOf(sb.toString());
	}

	public static Date getNextDay(Date currDate, boolean withSameTime) {
		return getNextNDay(currDate, 1, withSameTime);
	}

	/**
	 * 按传输时间返回时间的下一月
	 * 
	 * @param date
	 *            传输时间
	 * @param month
	 *            月数左右移动，0-返回当前月
	 * @return 下一月
	 */
	public static Date getNextMonth(Date date, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();
	}

	public static Date getNextNDay(Date currDate, int n, boolean withSameTime) {
		Calendar cd = getCalendar(currDate);

		cd.add(Calendar.DAY_OF_YEAR, n);
		if (!withSameTime) {
			cd.set(Calendar.HOUR_OF_DAY, 0);
			cd.set(Calendar.MINUTE, 0);
			cd.set(Calendar.SECOND, 0);
			cd.set(Calendar.MILLISECOND, 0);
		}

		return cd.getTime();
	}

	public static Date getNextNEndDay(Date currDate, int n) {
		Calendar cd = getCalendar(currDate);

		cd.add(Calendar.DAY_OF_YEAR, n);
		cd.set(Calendar.HOUR_OF_DAY, 23);
		cd.set(Calendar.MINUTE, 59);
		cd.set(Calendar.SECOND, 59);
		cd.set(Calendar.MILLISECOND, 999);

		return cd.getTime();
	}

	public static Date getLastDay(Date currDate, boolean withSameTime) {
		return getLastNDay(currDate, 1, withSameTime);
	}

	public static Date getLastNDay(Date currDate, int n, boolean withSameTime) {
		Calendar cd = getCalendar(currDate);

		cd.add(Calendar.DAY_OF_YEAR, -n);
		if (!withSameTime) {
			cd.set(Calendar.HOUR_OF_DAY, 0);
			cd.set(Calendar.MINUTE, 0);
			cd.set(Calendar.SECOND, 0);
			cd.set(Calendar.MILLISECOND, 0);
		}

		return cd.getTime();
	}

	public static Date getLastNEndDay(Date currDate, int n) {
		Calendar cd = getCalendar(currDate);

		cd.add(Calendar.DAY_OF_YEAR, -n);
		cd.set(Calendar.HOUR_OF_DAY, 23);
		cd.set(Calendar.MINUTE, 59);
		cd.set(Calendar.SECOND, 59);
		cd.set(Calendar.MILLISECOND, 999);

		return cd.getTime();
	}

	/**
	 * 前一天的开始时间
	 * */
	public static Date getLastBeginDay(Date currDate) {
		return getLastNDay(currDate, 1, false);
	}

	/**
	 * 前一天的结束时间
	 * */
	public static Date getLastEndDay(Date currDate) {
		return getLastNEndDay(currDate, 1);
	}

	/**
	 * 获取日期的初始时间
	 * 
	 * @param currDate
	 * @return
	 */
	public static Date getBeginDateTime(Date currDate) {
		Calendar cd = getCalendar(currDate);
		cd.set(Calendar.HOUR_OF_DAY, 0);
		cd.set(Calendar.MINUTE, 0);
		cd.set(Calendar.SECOND, 0);
		cd.set(Calendar.MILLISECOND, 0);

		return cd.getTime();
	}

	/**
	 * 获取日期的结束时间
	 * 
	 * @param currDate
	 * @return
	 */
	public static Date getEndDateTime(Date currDate) {
		Calendar cd = getCalendar(currDate);
		cd.set(Calendar.HOUR_OF_DAY, 23);
		cd.set(Calendar.MINUTE, 59);
		cd.set(Calendar.SECOND, 59);
		cd.set(Calendar.MILLISECOND, 999);

		return cd.getTime();
	}

	public static Date getStrDateBegin(String date) throws ParseException {
		Date re = parseDateSecondFormat(getDateDayFormat(parseDateDayFormat(date))
				+ " 00:00:00");

		return re;
	}

	public static Date getStrDateEnd(String date) throws ParseException {
		Date re = parseDateSecondFormat(getDateDayFormat(parseDateDayFormat(date))
				+ " 23:59:59");

		return re;
	}

	/**
	 * 取得指定月份的第一天及开始时间
	 * 
	 * @param strdate
	 *            String
	 * @return Date
	 * @throws ParseException
	 */
	public static Date getMonthBegin(String strdate) throws ParseException {
		Date date = parseDateDayFormat(strdate);
		String beginMonth = parseDateFormat(date, "yyyy-MM") + "-01 00:00:00";

		return parseDateFormat(beginMonth, YYYYMMDDHHMMSS);
	}

	/**
	 * 取得指定月份的最后一天及结束时间
	 * 
	 * @param strdate
	 *            String
	 * @return Date
	 * @throws ParseException
	 */
	public static Date getMonthEnd(String strdate) throws ParseException {
		Date date = getMonthBegin(strdate);
		Calendar cd = getCalendar(date);
		cd.add(Calendar.MONTH, 1);
		cd.add(Calendar.DAY_OF_MONTH, -1);
		String endMonth = parseDateFormat(cd.getTime(), "yyyy-MM-dd")
				+ " 23:59:59";

		return parseDateFormat(endMonth, YYYYMMDDHHMMSS);
	}

	public static int getDaysBetween(String d1, String d2)
			throws ParseException {
		Calendar c1 = parseCalendarDayFormat(d1);
		Calendar c2 = parseCalendarDayFormat(d2);

		return getDaysBetween(c1, c2);
	}

	public static int getDaysBetween(Date d1, Date d2) {
		Calendar c1 = getCalendar(d1);
		Calendar c2 = getCalendar(d2);

		return getDaysBetween(c1, c2);
	}

	public static int getDaysBetween(Calendar c1, Calendar c2) {
		if (c1.after(c2)) {
			Calendar swap = c1;
			c1 = c2;
			c2 = swap;
		}

		int days = c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR);
		int y2 = c2.get(Calendar.YEAR);
		if (c1.get(Calendar.YEAR) != y2) {
			Calendar tmp = (Calendar) c1.clone();
			do {
				days += tmp.getActualMaximum(Calendar.DAY_OF_YEAR);// 得到当年的实际天数
				tmp.add(Calendar.YEAR, 1);
			} while (tmp.get(Calendar.YEAR) != y2);
		}

		return days;
	}

	public static int getDaysBetweenExt(Date d1, Date d2) {
		Calendar c1 = getCalendar(d1);
		Calendar c2 = getCalendar(d2);
		boolean b = false;
		if (c1.after(c2)) {
			Calendar swap = c1;
			c1 = c2;
			c2 = swap;
			b = true;
		}
		int days = c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR);
		int y2 = c2.get(Calendar.YEAR);
		if (c1.get(Calendar.YEAR) != y2) {
			Calendar tmp = (Calendar) c1.clone();
			do {
				days += tmp.getActualMaximum(Calendar.DAY_OF_YEAR);// 得到当年的实际天数
				tmp.add(Calendar.YEAR, 1);
			} while (tmp.get(Calendar.YEAR) != y2);
		}
		if (b)
			days = -days;
		return days;
	}

	/**
	 * 判断是否是同一天
	 * 
	 * @param when
	 * @param toWhen
	 * @return
	 */
	public static boolean isSameDate(Date when, Date toWhen) {
		return getDaysBetween(when, toWhen) == 0;
	}

	/**
	 * 两个时间相差多少分钟
	 * 
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public static int getDateSecondLength(Date beginTime, Date endTime) {
		return (int) ((beginTime.getTime() - endTime.getTime()) / (60 * 1000));
	}

	/**
	 * 获取日期的星期几
	 * 
	 * @param date
	 * @return 星期几
	 */
	public static String getDateEEEE(Date date) {
		SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
		return dateFm.format(date);
	}

	public static void main(String[] args) throws Exception {
	}
}