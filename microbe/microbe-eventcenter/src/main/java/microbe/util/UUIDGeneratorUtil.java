package microbe.util;

import java.util.UUID;

public class UUIDGeneratorUtil {

	public static String getSimpleUUID() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.substring(0, 8) + uuid.substring(9, 13)
				+ uuid.substring(14, 18) + uuid.substring(19, 23)
				+ uuid.substring(24);
		return uuid;
	}

	public static void main(String[] args) {

		// System.out.println(RobotGroupEnum.LETOUTNOTIFY.getGroup());
		// for (int i = 0; i < 10; i++)
		// System.out.println(getUUID());
	}
}
