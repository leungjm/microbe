package microbe.util;

import java.util.concurrent.ConcurrentHashMap;

import microbe.model.json.LoginAccountJson;

public class SessionUtil {
	public static final ConcurrentHashMap<String, LoginAccountJson> loginInfo = new ConcurrentHashMap<String, LoginAccountJson>();

	public static void putLoginInfo(String uuid, LoginAccountJson json) {
		loginInfo.put(uuid, json);
	}

	public static LoginAccountJson getLoginInfo(String uuid) {
		return loginInfo.get(uuid);
	}

	public static void removeLoginInfo(String uuid) {
		loginInfo.remove(uuid);
	}
}
