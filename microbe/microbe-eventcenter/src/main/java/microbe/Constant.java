package microbe;

public class Constant {

	public static final String SESSION_NAME = "EVENTSESSION";
	public static final String LOGIN_USER_JSON = "LOGIN_USER_JSON";

	public static final Integer DAY_SECOND = 24 * 60 * 60;

	public static final String RESPONSE_CODE = "code";
	public static final String RESPONSE_DESC = "desc";

}
