package microbe;

import com.lanshi.microbe.jetty.EmbeddedJetty;

public class StartupBoot {

	public static void main(String[] args) throws Exception {
		args = new String[1];
		args[0] = "eventcenter.jetty.properties";

		new EmbeddedJetty().startJetty(args);

	}

}
