package microbe.intercepters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import microbe.Constant;
import microbe.model.json.LoginAccountJson;
import microbe.util.SessionUtil;
import microbe.util.StringUtil;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AuthLoginIntercepter extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String session = request.getHeader(Constant.SESSION_NAME);
		if (!StringUtil.isEmptyStr(session)) {
			LoginAccountJson loginJson = SessionUtil.getLoginInfo(session);
			if (loginJson != null) {
				request.setAttribute(Constant.LOGIN_USER_JSON, loginJson);
				return true;
			}
		}
		response.setStatus(401);
		return false;
	}

}
