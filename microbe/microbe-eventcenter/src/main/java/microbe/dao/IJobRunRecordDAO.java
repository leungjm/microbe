package microbe.dao;

import java.util.List;

import microbe.model.tables.JobRunRecord;

import com.lanshi.microbe.repository.hibernate.dao.IBaseDAO;

public interface IJobRunRecordDAO extends IBaseDAO<JobRunRecord, Long> {

	List<JobRunRecord> findByJobId(Long jobId);

}
