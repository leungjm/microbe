package microbe.dao.impl;

import java.util.List;

import microbe.dao.IJobRunRecordDAO;
import microbe.model.tables.JobRunRecord;

import org.springframework.stereotype.Repository;

import com.lanshi.microbe.repository.hibernate.dao.BaseHibernateDAO;

@Repository
public class JobRunRecordDAO extends BaseHibernateDAO<JobRunRecord, Long>
		implements IJobRunRecordDAO {
	public static final String JOB_ID = "jobId";

	@Override
	public List<JobRunRecord> findByJobId(Long jobId) {
		return findByProperty(JOB_ID, jobId);
	}
}
