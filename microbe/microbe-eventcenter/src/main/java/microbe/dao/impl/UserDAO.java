package microbe.dao.impl;

import java.util.List;

import microbe.dao.IUserDAO;
import microbe.model.tables.User;

import org.springframework.stereotype.Repository;

import com.lanshi.microbe.repository.hibernate.dao.BaseHibernateDAO;

@Repository
public class UserDAO extends BaseHibernateDAO<User, Long> implements IUserDAO {
	public static final String USER_NAME = "userName";
	public static final String PASSWORD = "password";
	public static final String GROUP = "group";

	@Override
	public List<User> findByName(String name) {
		return findByProperty(USER_NAME, name);
	}

}
