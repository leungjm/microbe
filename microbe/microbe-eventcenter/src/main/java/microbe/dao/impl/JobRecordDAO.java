package microbe.dao.impl;

import java.util.Date;
import java.util.List;

import microbe.dao.IJobRecordDAO;
import microbe.model.enums.JobStatusEnum;
import microbe.model.tables.JobRecord;
import microbe.model.view.PagerListResultJson;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.lanshi.microbe.repository.hibernate.dao.BaseHibernateDAO;
import com.lanshi.microbe.repository.hibernate.dao.PageCounter;

@Repository
public class JobRecordDAO extends BaseHibernateDAO<JobRecord, Long> implements
		IJobRecordDAO {

	public static final String USER_ID = "userId";
	public static final String JOB_GROUP = "jobGroup";
	public static final String JOB_GROUP_MODEL_ID = "jobGroupModelId";
	public static final String JOB_MODEL_DESC = "jobModelDesc";
	public static final String JOB_NOTIFY_JSON_DATA = "jobNotifyJsonData";
	public static final String NOTIFY_TIME = "notifyTime";
	public static final String TRY_FAIL_COUNT = "tryFailCount";
	public static final String FAIL_TIME_INTERVAL = "failTimeInterval";
	public static final String REPEAT_COUNT = "repeatCount";
	public static final String HAD_REPEAT_COUNT = "hadRepeatCount";
	public static final String REPEAT_TIME_INTERVAL = "repeatTimeInterval";
	public static final String NOTIFY_URL = "notifyUrl";
	public static final String JOB_STATUS = "jobStatus";
	public static final String JOB_RUN_COUNT = "jobRunCount";

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByGroupJobGroupModelId(String group,
			String jobGroupModelId) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(JOB_GROUP, group));
		crit.add(Restrictions.eq(JOB_GROUP_MODEL_ID, jobGroupModelId));
		return crit.list();
	}

	@Override
	public List<JobRecord> findByJobGroup(String group) {
		return findByProperty(JOB_GROUP, group);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByStatusDayBet(List<JobStatusEnum> status,
			Date beginDateTime, Date endDateTime) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.in(JOB_STATUS, status));
		crit.add(Restrictions.ge(NOTIFY_TIME, beginDateTime));
		crit.add(Restrictions.le(NOTIFY_TIME, endDateTime));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByStatusBeforeDay(List<JobStatusEnum> status,
			Date endDateTime) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.in(JOB_STATUS, status));
		crit.add(Restrictions.le(NOTIFY_TIME, endDateTime));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByUserId(Long userId) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByUserIdStartDayEndDay(Long userId,
			Date startDay, Date endDay) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		if (startDay != null) {
			crit.add(Restrictions.ge(NOTIFY_TIME, startDay));
		}
		if (endDay != null) {
			crit.add(Restrictions.le(NOTIFY_TIME, endDay));
		}
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByUserIdStartDayEndDay(Long userId,
			Date startTime, Date endTime, JobStatusEnum[] statuses) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		if (startTime != null) {
			crit.add(Restrictions.ge(NOTIFY_TIME, startTime));
		}
		if (endTime != null) {
			crit.add(Restrictions.le(NOTIFY_TIME, endTime));
		}
		if (statuses != null && statuses.length > 0) {
			crit.add(Restrictions.in(JOB_STATUS, statuses));
		}
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findJobGroupByUserIdGroupByJobGroup(Long userId) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		crit.setProjection(Projections.groupProperty(JOB_GROUP));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByUserIdDayBetween(Long userId,
			Date beginDateTime, Date endDateTime) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		crit.add(Restrictions.ge(NOTIFY_TIME, beginDateTime));
		crit.add(Restrictions.le(NOTIFY_TIME, endDateTime));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobRecord> findByUserIdStartDayEndDayStatusGroups(Long userId,
			Date startTime, Date endTime, JobStatusEnum[] status,
			String[] groups) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		if (startTime != null) {
			crit.add(Restrictions.ge(NOTIFY_TIME, startTime));
		}
		if (endTime != null) {
			crit.add(Restrictions.le(NOTIFY_TIME, endTime));
		}
		if (status != null && status.length > 0) {
			crit.add(Restrictions.in(JOB_STATUS, status));
		}
		if (groups != null && groups.length > 0) {
			crit.add(Restrictions.in(JOB_GROUP, groups));
		}
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PagerListResultJson<JobRecord> findByUserIdDayBetweenPages(
			Long userId, Date beginDateTime, Date endDateTime,
			Integer pageIndex, Integer pageSize) {
		PagerListResultJson<JobRecord> result = new PagerListResultJson<JobRecord>();
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq(USER_ID, userId));
		crit.add(Restrictions.ge(NOTIFY_TIME, beginDateTime));
		crit.add(Restrictions.le(NOTIFY_TIME, endDateTime));
		crit.setProjection(Projections.rowCount());
		Long total = (Long) crit.uniqueResult();
		result.setPageCounter(new PageCounter(total == null ? 0 : total
				.intValue(), pageIndex, pageSize));
		crit.setProjection(null);
		result.setResponse(crit.list());
		return result;
	}

}
