package microbe.dao;

import java.util.List;

import microbe.model.tables.User;

import com.lanshi.microbe.repository.hibernate.dao.IBaseDAO;

public interface IUserDAO extends IBaseDAO<User, Long> {

	List<User> findByName(String name);

}
