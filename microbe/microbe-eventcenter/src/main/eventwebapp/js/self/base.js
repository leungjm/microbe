var SERVER_URL = 'http://localhost:8098/';
var loginVue;
var checkLogin;
var LOGIN_FLAG;

(function(){
	// 一些方法......
    var getServerAddress;
    var executeFirstRequestOfQueue;
    var dequeueRequest;
    var checkSession;
    
    // 一些变量......
    var requestQueue = [];
    var loadingCount = 0;
    var checkBindCallbackQueue = [];
    
    getServerAddress = function(v) {
        return SERVER_URL;
    };

    executeFirstRequestOfQueue = function() {
        if (requestQueue.length > 0 && !requestQueue[0].isExecuted) {
            requestQueue[0].isExecuted = true;
            requestQueue[0].call();
        }
    };

    dequeueRequest = function() {
        requestQueue.shift();
        executeFirstRequestOfQueue();
    };

    /**
     * version:后台接口版本号,在config.json中定义
     * name:后台接口地址,例如course/listPublicCourseSchedule
     * param:接口参数,json对象
     * loginCheck:是否检查绑定,如果没绑定则自动弹出绑定窗口
     * succDo:成功返回
     * failDo:失败
     * loading:显示进度条,true:显示默认进度条,字符串:显示传入的字符串,false:不显示进度条
     */
    request = function(version, name, param, loginCheck, succDo, failDo, loading) {

        if (loading) {
            loadingCount ++;
        }

        function doRequest() {

            if (loadingCount) {
                $.mobile.loading('show', {
                    text: typeof loading === 'string' ? loading : '努力加载中',
                    textVisible: true,
                    theme: 'b',
                    textonly: false,
                    html: null
                });
            }

            $.post(getServerAddress(version) + name, param, function(data) {

                if (loading) {
                    loadingCount --;
                    if (loadingCount === 0) {
                        $.mobile.loading('hide');
                    }
                }

                if (data.resultCode === 'success' && typeof succDo === 'function') {
                    succDo(data);
                } else if (typeof failDo === 'function') {
                    failDo(data.code, data.desc, data);
                }

                dequeueRequest();

            }, 'json').fail(function(resp) {

                if (loading) {
                    loadingCount --;
                    if (loadingCount === 0) {
                        $.mobile.loading('hide');
                    }
                }
                if (resp.status === 0) {
                    // 网络不可用
                } else if (resp.status === 401) {
                    // 无需处理
                } else {
                    alert('出现错误:' + resp.status + ',' + resp.responseText);
                    if (failDo === 'function') {
                        failDo('status:' + resp.status, 'responseText:' + resp.responseText);
                    }
                }

                dequeueRequest();

            });
        }

        requestQueue.push(doRequest);

        if (loginCheck === true) {
        	checkLogin(function() {
                executeFirstRequestOfQueue();
            });
        } else if (checkBindCallbackQueue.length === 0) {
            executeFirstRequestOfQueue();
        }

    };
	
	checkLogin = function(cb){
		if (checkBindCallbackQueue.length > 0) {
            checkBindCallbackQueue.push(cb);
            return;
        }
        checkBindCallbackQueue.push(cb);
        $.post(getServerAddress() + 'login/check.do', null, function(data) {
            if (data.resultCode === 'success') {
                // 已绑定,登录成功,可以进行所有会员操作
                while (checkBindCallbackQueue.length > 0) {
                    var cb = checkBindCallbackQueue.shift();
                    if (typeof cb === 'function') {
                        cb();
                    }
                }
                $('#popup-login').popup('close');
            } else {
                alert(data.resultDesc);
                checkBindCallbackQueue = [];
            }
        }, 'json').fail(function(resp) {
            if (resp.status === 401) {
                // 需要登录
                $('#popup-login').popup('open');
            } else {
                alert('出现错误:' + resp.status + ',' + resp.responseText);
                checkBindCallbackQueue = [];
            }
        });
	};
	
	checkSession = function (){
		if(!localStorage.sid){
			LOGIN_FLAG = false;
		}
	}
	$(function(){
		loginVue = new Vue({
			el : '#popup-login',
			data : {
				name : '',
				password : '',
				failure : ''
			},
			methods:{
				login : function(evt){
					var self = this;
					self.failure = null;
					$.post(getServerAddress() + 'admin/login.do', {name: self.name, password: self.password}, function(data, textStatus, resObj) {
                        if (data.resultCode === 'success') {
                        	localStorage.sid = resObj.getResponseHeader('EVENTSESSION');  
                            while (checkBindCallbackQueue.length > 0) {
                                var cb = checkBindCallbackQueue.shift();
                                if (typeof cb === 'function') {
                                    cb(data.data);
                                }
                            }
                            $('#popup-login').popup('close');
                        } else {
                        	self.failure = data.resultDesc;
                        }
                    }, 'json').fail(function() {
                        alert('出现错误:' + resp.status + ',' + resp.responseText);
                    });
					
				}
			}
		});
		 $('#popup-login').on('popupafterclose', function(event, ui) {
	            // 关闭绑定界面之后,证明用户不想继续,可以忽略后续操作.
	            checkBindCallbackQueue = [];
	     });

		checkSession();
	});
	
	$.ajaxSetup({
        //contentType: 'application/json',
        beforeSend: function(xhr) {
            if (localStorage.sid) {
                xhr.setRequestHeader('EVENTSESSION', localStorage.sid);
            }
        }
    });
})();

