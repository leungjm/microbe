var recordVal = {
		id : '',
		jobGroupModelId : '',
		jobGroup : '',
		jobModelDesc : '',
		notifyTime : '',
		repeatCount : '',
		hadRepeatCount : '',
		repeatTimeInterval : '',
		notifyUrl : '',
		jobStatus : '',
		createTime : ''
};

var detailRecords = {
		id : '',
		jobId : '',
		runTime : '',
		runResult : '',
		returnCode : '',
		returnMsg : '',
		operatorType : '',
		operatorId : '',
		updateTime : ''
}

var pagerCounterModel = {
		totalItem: 0, 
		currentItem: 1, 
		pageNum: 0, 
		pageSize: 15, 
		pageTotal: 0,
		pageShow : []
}


$(function(){
	
	var pageVue = new Vue({
		el : '#recordPagerVue',
		data : {
			pagerCounter : pagerCounterModel
		},
		methods :{
			countStartShow : function(counter){
				var self = this;
				var pageShow = [];
				if(counter.pageTotal < 9){
					for(var i = 1; i <= counter.pageTotal; i++){
						pageShow.push(i);
					}
				}else{
					for(var i = counter.pageNum - 5; i < counter.pageNum + 5 && i <= counter.totalPage; i++){
						pageShow.push(i);
					}
				}
				counter.pageShow=pageShow;
				self.pagerCounter = counter;
			}
		}
	});
	
	var vJobRecord = new Vue({
		el : '#jobRecordVue',
		data : {
			records : recordVal
		},
		methods : {
			findUserRecords : function(jobRun){
				var self = this;
				request('v1', 'record/findByUserTodayOrJobRun.do', {pageIndex:1, pageSize:15, jobRun:jobRun}, true, function(response){
					localStorage.jobRecords = JSON.stringify(response.response);
					self.records = response.response;
					pageVue.countStartShow(response.pageCounter);
				},function(){
					window.alert("fail");
				}, true);
			},
			detailClick : function(recordId){
				detailVue.findRunRecords(recordId);
			},
			changeStatus : function(record, status, index){
				var self = this;
				request('v1', 'record/changeStatus.do', {recordId:record.id, status:status}, true, function(response){
					var tmpJobRecords = JSON.parse(localStorage.jobRecords);
					tmpJobRecords[index] = response.response;
					self.records = tmpJobRecords;
					localStorage.jobRecords = JSON.stringify(tmpJobRecords);
				},function(){
					window.alert("fail");
				}, true);
			}
		},
		filters: {
		    calHourMin: function (value) {
		      return MillisecondToDate(value);
		    }
		},
		mounted: function () {
            this.$nextTick(function () {
            	this.findUserRecords(false);
            });
        }
	});
	
	var groupVue = new Vue({
		el : '#groupVue',
		data : {
			jobGroups : []
		},
		methods : {
			findGroupJobs : function(){
				var self = this;
				request('v1', 'record/findByGroupJob.do', {}, true, function(response){
					self.jobGroups = response.response;
				},function(){
					window.alert("fail");
				}, true);
			}
		},
		mounted: function () {
            this.$nextTick(function () {
            	this.findGroupJobs();
            });
        },
        watch: {
//        	'jobGroups': function(){window.alert('watch');}
        },
        updated: function(){
//        	window.alert('updated');
        	$('#group-choice').selectmenu().selectmenu('refresh', true);
        }
	});
	
	var dayJobRecord = new Vue({
		el:'#dayVue',
		data : {
			startDay : null,
			endDay : null
		},
		methods : {
			findByDaysRecords : function (){
				var self = this;
				var status = $('#status-choice').val();
				var groups = $('#group-choice').val();
//				window.alert(self.startDay + "  " + self.endDay);
				request('v1', 'record/findByDaysStatusGroupsRecords.do', {startDay:dayJobRecord.startDay, endDay:dayJobRecord.endDay, status:status, groups:groups}, true, function(response){
					vJobRecord.records = response.response;
					localStorage.jobRecords = JSON.stringify(response);
				},function(){
					window.alert("fail");
				}, true);
			}
		}
	});
	
	var todayFinder = function(){
		$("#choiceDiv").css('display', 'none');
		vJobRecord.findUserRecords(false);
	}
	var cronJobRunFinder = function(){
		$("#choiceDiv").css('display', 'none');
		vJobRecord.findUserRecords(true);
	}
	var selectionChoicer = function(){
		localStorage.jobRecords = null;
		$("#choiceDiv").css('display', 'block');
	}

	$("#todayLi").on('click', todayFinder);
	$("#cronJobLi").on('click', cronJobRunFinder);
	$("#selectionLi").on('click', selectionChoicer);
	
// page detail 
	var  detailVue = new Vue({
		el : '#jobRunRecordVue',
		data : {
			detailRecords : detailRecords
		},
		methods : {
			findRunRecords : function (record){
				localStorage.selectedRecordTmp = record;
				var self = this;
				request('v1', 'record/findJobRunRecords.do', {recordId:record.id}, true, function(response){
					self.detailRecords = response.response;
					localStorage.detailRecords = JSON.stringify(response);
				},function(){
					window.alert("fail");
				}, true);
			}
		},
		mounted: function () {
            this.$nextTick(function () {
            	if(localStorage.detailRecords){
            		detailVue.detailRecords = JSON.parse(localStorage.detailRecords).response;
            	}
            	
            });
        }
	});
	function MillisecondToDate(msd) {
		var days    = msd / 1000 / 60 / 60 / 24;
		var daysRound   = Math.floor(days);
		var hours    = msd/ 1000 / 60 / 60 - (24 * daysRound);
		var hoursRound   = Math.floor(hours);
		var minutes   = msd / 1000 /60 - (24 * 60 * daysRound) - (60 * hoursRound);
		var minutesRound  = Math.floor(minutes);
		var seconds   = msd / 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound);
		var time = '';
		if(daysRound > 0){
			time = daysRound + '天';
		}
		if(hoursRound > 0){
			time += hoursRound + '时';
		}
		if(minutesRound > 0){
			time += minutesRound + '分';
		}
		if(seconds > 0){
			time += seconds + '秒';
		}
		return time;
		
       /* var time = parseFloat(msd) /1000;
        if (null!= time &&""!= time) {
        	if(time < 60){
        		 time = parseInt(time) +"秒";
        	}else if (time <= 3600) {
                time = parseInt(time / 60.0) +"分"+ parseInt((parseFloat(time / 60.0) -
                parseInt(time / 60.0)) *60) +"秒";
            }else if (time <=60*60*24) {
                time = parseInt(time /3600.0) +"时"+ parseInt((parseFloat(time /3600.0) -
                parseInt(time /3600.0)) *60) +"分"+
                parseInt((parseFloat((parseFloat(time /3600.0) - parseInt(time /3600.0)) *60) -
                parseInt((parseFloat(time /3600.0) - parseInt(time /3600.0)) *60)) *60) +"秒";
            }else {
            	time = parseInt(time /3600.0) +"时"+ parseInt((parseFloat(time /3600.0) -
                parseInt(time /3600.0)) *60) +"分"+
                parseInt((parseFloat((parseFloat(time /3600.0) - parseInt(time /3600.0)) *60) -
                parseInt((parseFloat(time /3600.0) - parseInt(time /3600.0)) *60)) *60) +"秒";
            }
        }else{
            time = "0 时 0 分0 秒";
        }
        return time;*/
    }

});

