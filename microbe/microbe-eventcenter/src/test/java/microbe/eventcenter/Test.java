package microbe.eventcenter;

import java.util.HashMap;
import java.util.Map;

import microbe.util.HttpUtil;
import microbe.util.JsonUtil;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;

public class Test {

	// public static String jobUrl = "http://localhost:8090/job/addRobot.do";
	// public static String loginUrl = "http://localhost:8090/admin/login.do";
	public static String jobUrl = "http://120.76.220.98:8098/job/addRobot.do";
	public static String loginUrl = "http://120.76.220.98:8098/admin/login.do";
	public static String sessionId = "";

	public static void main2(String[] args) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("data", "ADFASDF");
		queryParams.put("name", "jialllslls");
		Map<String, String> headerAttr = new HashMap<String, String>();
		// headerAttr.put("EVENTSESSION", sessionId);
		CloseableHttpResponse result = HttpUtil.getInstance()
				.doPostForResponse(
						"http://localhost:9000/cb/testIn/testdataPost", null,
						queryParams, "UTF-8", 5000, 5000, headerAttr);
		if (result.getStatusLine().getStatusCode() == 401) {
			System.out.println("response status 401");
		}
		System.out.println("response result : " + result);
	}

	public static void main(String[] args) {
		JobEntity entity = new JobEntity();
		entity.setJobGroupModelId("test04");
		entity.setJobGroup("jfit4");
		entity.setJobModelDesc("TEST04");
		entity.setJobNotifyJsonData("hello kitty4");
		entity.setNotifyTime("2016-12-30 16:30:000");
		entity.setNotifyUrl("http://localhost:8090/test/admin/testJobCallBack.do");
		entity.setRepeatCount(3);
		entity.setRepeatTimeInterval(20 * 60L * 1000);
		entity.setTryFailCount(5);
		entity.setFailTimeInterval(5000);
		try {
			// 登录
			Map<String, String> loginParams = new HashMap<String, String>();
			loginParams.put("name", "jfit");
			loginParams.put("password", "jfit2016");
			CloseableHttpResponse loginResult = HttpUtil.getInstance()
					.doPostForResponse(loginUrl, null, loginParams, "UTF-8",
							null);
			System.out.println("login result:" + loginResult);
			Header header = loginResult.getFirstHeader("EVENTSESSION");
			sessionId = header.getValue();
			System.out.println("sessionId:" + sessionId);
			loginResult.close();
			for (int i = 0; i < 2; i++) {
				entity.setJobGroupModelId("jfit16" + i);
				String json = JsonUtil.obj2json(entity);
				System.out.println("prepare add robot data:" + json);
				Map<String, String> queryParams = new HashMap<String, String>();
				queryParams.put("data", json);
				Map<String, String> headerAttr = new HashMap<String, String>();
				headerAttr.put("EVENTSESSION", sessionId);
				CloseableHttpResponse result = HttpUtil.getInstance()
						.doPostForResponse(jobUrl, queryParams, null, "UTF-8",
								5000, 5000, headerAttr);
				if (result.getStatusLine().getStatusCode() == 401) {
					System.out.println("response status 401");
				}
				System.out.println("response result : " + result);
				result.close();
				// Thread.sleep(1000);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
