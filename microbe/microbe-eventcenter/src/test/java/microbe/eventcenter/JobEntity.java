package microbe.eventcenter;

import java.io.Serializable;

public class JobEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobGroupModelId;// 第三方所属组的唯一标识
	private String jobGroup;// 第三方所属组
	private String jobModelDesc;// 第三方所属组的唯一标识
	private String jobNotifyJsonData;// 通知时回传数据
	private String notifyTime;// 首次提醒时间yyyy-MM-dd HH:mm:ss
	private Integer tryFailCount;// 失败尝试次数
	private Integer failTimeInterval;// 失败尝试时间间隔
	private Integer repeatCount;// 循环次数
	private Long repeatTimeInterval;// 重复时间间隔
	private String notifyUrl;// 通知调用的url

	public JobEntity() {
		super();
	}

	public JobEntity(String jobGroupModelId, String jobModelDesc,
			String jobNotifyJsonData, String notifyTime, Integer tryFailCount,
			Integer failTimeInterval, Integer repeatCount,
			Long repeatTimeInterval, String notifyUrl) {
		this.jobGroupModelId = jobGroupModelId;
		this.jobModelDesc = jobModelDesc;
		this.jobNotifyJsonData = jobNotifyJsonData;
		this.notifyTime = notifyTime;
		this.tryFailCount = tryFailCount;
		this.failTimeInterval = failTimeInterval;
		this.repeatCount = repeatCount;
		this.repeatTimeInterval = repeatTimeInterval;
		this.notifyUrl = notifyUrl;
	}

	public String getJobGroupModelId() {
		return jobGroupModelId;
	}

	public void setJobGroupModelId(String jobGroupModelId) {
		this.jobGroupModelId = jobGroupModelId;
	}

	public String getJobModelDesc() {
		return jobModelDesc;
	}

	public void setJobModelDesc(String jobModelDesc) {
		this.jobModelDesc = jobModelDesc;
	}

	public String getJobNotifyJsonData() {
		return jobNotifyJsonData;
	}

	public void setJobNotifyJsonData(String jobNotifyJsonData) {
		this.jobNotifyJsonData = jobNotifyJsonData;
	}

	public String getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(String notifyTime) {
		this.notifyTime = notifyTime;
	}

	public Integer getTryFailCount() {
		return tryFailCount;
	}

	public void setTryFailCount(Integer tryFailCount) {
		this.tryFailCount = tryFailCount;
	}

	public Integer getFailTimeInterval() {
		return failTimeInterval;
	}

	public void setFailTimeInterval(Integer failTimeInterval) {
		this.failTimeInterval = failTimeInterval;
	}

	public Integer getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public Long getRepeatTimeInterval() {
		return repeatTimeInterval;
	}

	public void setRepeatTimeInterval(Long repeatTimeInterval) {
		this.repeatTimeInterval = repeatTimeInterval;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

}
