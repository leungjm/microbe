package com.lanshi.microbe.container.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.container.IContainer;

public class SpringContainerImpl implements IContainer {

	public static final String SPRING_XML_PATH = "classpath:spring/root/applicationContext.xml";
	private SpringContextImpl springContext;

	@Override
	public void start() {
		AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				SPRING_XML_PATH);
		springContext = new SpringContextImpl(applicationContext);

		MicrobeLogger.warn("now starting spring content with sprign path:{}",
				SPRING_XML_PATH);
		applicationContext.registerShutdownHook();
		applicationContext.start();
	}

	@Override
	public void stop() {
		MicrobeLogger.warn("now going to stop spring application...");
		if (springContext != null && springContext.get() != null) {
			springContext.get().close();
			springContext = null;
		}

	}

	@Override
	public SpringContextImpl getContext() {
		return springContext;
	}

}
