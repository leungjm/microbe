package com.lanshi.microbe.container.spring;

import org.aspectj.lang.annotation.Pointcut;

public class AspectConfig {

	@Pointcut("execution(* com.lanshi..*(..))")
	public void contextConfig() {

	}
}
