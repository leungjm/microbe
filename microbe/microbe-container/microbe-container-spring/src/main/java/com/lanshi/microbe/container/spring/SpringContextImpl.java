package com.lanshi.microbe.container.spring;

import org.springframework.context.support.AbstractApplicationContext;

import com.lanshi.microbe.container.IContext;

public class SpringContextImpl implements IContext<AbstractApplicationContext> {

	private final AbstractApplicationContext applicationContext;

	public SpringContextImpl(final AbstractApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public AbstractApplicationContext get() {
		return this.applicationContext;
	}

}
