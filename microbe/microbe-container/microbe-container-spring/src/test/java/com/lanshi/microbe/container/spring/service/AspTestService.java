package com.lanshi.microbe.container.spring.service;

import org.springframework.stereotype.Service;

@Service
public class AspTestService implements IAspTestService {

	@Override
	public String asp() {
		System.out.println("hello ....");
		return "asp";
	}

}
