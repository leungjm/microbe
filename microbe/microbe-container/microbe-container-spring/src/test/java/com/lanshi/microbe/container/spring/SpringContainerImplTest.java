package com.lanshi.microbe.container.spring;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class SpringContainerImplTest {
	SpringContainerImpl container = new SpringContainerImpl();

	@Test
	public void testStart() {
		container.start();
		assertThat(container.getContext(), notNullValue());
	}

	@Test
	public void testStop() {
		container.start();
		assertThat(container.getContext(), notNullValue());
		container.stop();
		assertThat(container.getContext(), nullValue());
	}

	@Test
	public void stopWhenNotStart() {
		container.stop();
		assertThat(container.getContext(), nullValue());
	}
}
