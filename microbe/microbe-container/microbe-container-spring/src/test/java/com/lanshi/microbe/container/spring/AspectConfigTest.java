package com.lanshi.microbe.container.spring;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.lanshi.microbe.container.spring.service.IAspTestService;

@ContextConfiguration(locations = "classpath:spring/root/applicationContext.xml")
public class AspectConfigTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private IAspTestService aspTestService;

	@Test
	public void publicPointAspect() {
		assertThat(aspTestService.asp(), is("aspimpl"));
	}
}
