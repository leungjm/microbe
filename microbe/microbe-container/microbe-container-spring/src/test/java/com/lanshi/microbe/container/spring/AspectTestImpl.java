package com.lanshi.microbe.container.spring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public final class AspectTestImpl {

	@Around("com.lanshi.microbe.container.spring.AspectConfig.contextConfig()")
	public Object aroundLogger(final ProceedingJoinPoint pjp) throws Throwable {
		Object returnValue = pjp.proceed();
		return null == returnValue ? returnValue : returnValue.toString()
				+ "impl";
	}
}
