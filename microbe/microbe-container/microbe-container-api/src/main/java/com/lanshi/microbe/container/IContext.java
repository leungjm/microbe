package com.lanshi.microbe.container;

public interface IContext<T> {

	T get();
}
