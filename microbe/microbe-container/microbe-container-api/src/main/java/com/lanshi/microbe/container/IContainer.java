package com.lanshi.microbe.container;

public interface IContainer {
	void start();

	void stop();

	IContext<?> getContext();
}
