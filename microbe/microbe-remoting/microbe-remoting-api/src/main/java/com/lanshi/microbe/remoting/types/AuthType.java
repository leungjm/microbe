package com.lanshi.microbe.remoting.types;

public enum AuthType {
	AUTHOR_SUCCESS((byte) 0, "校验成功"), AUTHOR_FAIL((byte) 1, "校验失败"), AUTHOR_DUMPLI(
			(byte) 2, "重复登录");

	private byte value;
	private String desc;

	private AuthType(byte value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public byte value() {
		return value;
	}

	public String desc() {
		return desc;
	}

}
