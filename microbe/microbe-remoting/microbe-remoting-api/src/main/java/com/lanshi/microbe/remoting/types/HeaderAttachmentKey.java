package com.lanshi.microbe.remoting.types;

public enum HeaderAttachmentKey {
	IP("ip", "IP地址"), URL("url", "完整URL"), CLAZZ("clazz", "访问的类"), METHOD(
			"method", "访问的方法"), PARAMS("params", "参数");
	private String key;
	private String desc;

	private HeaderAttachmentKey(String key, String desc) {
		this.key = key;
		this.desc = desc;
	}

	public String key() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String desc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
