package com.lanshi.microbe.remoting.server;

public interface IServer {
	long SYSTEM_MESSAGE_ID = -1L;

	public void start(String host, int port);

	public boolean stop();
}
