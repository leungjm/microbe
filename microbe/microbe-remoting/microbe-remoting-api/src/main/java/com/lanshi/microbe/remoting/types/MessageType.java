package com.lanshi.microbe.remoting.types;

public enum MessageType {
	/**
	 * value: 0 业务请求消息
	 */
	BUSSINESS_REQ((byte) 0, "业务请求消息"),
	/**
	 * value: 1 业务响应消息
	 */
	BUSSINESS_RESP((byte) 1, "业务响应消息"),
	/**
	 * value: 2 既是业务请求消息也是响应消息
	 */
	BUSSINESS_ONEWAY((byte) 2, "既是业务请求消息也是响应消息"),
	/**
	 * value: 3 握手请求消息
	 */
	AUTHOR_REQ((byte) 3, "握手请求消息"),
	/**
	 * value: 4 握手应答消息
	 */
	AUTHOR_RESP((byte) 4, "握手应答消息"),
	/**
	 * value: 5 心跳请求消息
	 */
	PING_REQ((byte) 5, "心跳请求消息"),
	/**
	 * value: 6 心跳应答消息
	 */
	PING_RESP((byte) 6, "心跳应答消息");

	private byte value;
	private String desc;

	private MessageType(byte value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public byte value() {
		return value;
	}

	public String desc() {
		return desc;
	}

}
