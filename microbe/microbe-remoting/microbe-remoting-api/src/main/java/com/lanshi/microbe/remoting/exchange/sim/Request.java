package com.lanshi.microbe.remoting.exchange.sim;

import java.io.Serializable;

public final class Request implements Serializable {

	private static final long serialVersionUID = 2750646443189480771L;

	private long messageId;
	private Class<?> apiClass;
	private String method;
	private Object[] parameters;

	public Request(final Class<?> apiClass, final String method,
			final Object... parameters) {
		messageId = System.nanoTime();
		this.apiClass = apiClass;
		this.method = method;
		this.parameters = parameters;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public Class<?> getApiClass() {
		return apiClass;
	}

	public void setApiClass(Class<?> apiClass) {
		this.apiClass = apiClass;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Object[] getParameters() {
		return parameters;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

}
