package com.lanshi.microbe.remoting.client;

import java.net.InetSocketAddress;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

public interface IClient {

	public void connect(InetSocketAddress socketAddress);

	public Response sendRequest(Request request);

	public void close();

	public InetSocketAddress getRemoteAddress();
}
