package com.lanshi.microbe.remoting.types;

public enum MessagePriority {
	NORMAL((byte) 0), INPORTAN((byte) 0), VERYINPORTAN((byte) 0);
	private byte value;

	private MessagePriority(byte value) {
		this.value = value;
	}

	public byte value() {
		return value;
	}
}
