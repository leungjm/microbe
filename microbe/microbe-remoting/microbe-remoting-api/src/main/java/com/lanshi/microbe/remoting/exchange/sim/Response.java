package com.lanshi.microbe.remoting.exchange.sim;

import java.io.Serializable;

public final class Response implements Serializable {

	private static final long serialVersionUID = 5887232731148682128L;

	private long messageId;
	private Object returnValue;
	private Throwable exception;

	public Response(final long messageId, final Object returnValue) {
		this.messageId = messageId;
		this.returnValue = returnValue;
		this.exception = null;
	}

	public Response(final long messageId, final Throwable exception) {
		this.messageId = messageId;
		this.returnValue = null;
		this.exception = exception;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public Object getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(Object returnValue) {
		this.returnValue = returnValue;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

}
