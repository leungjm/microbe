package com.lanshi.microbe.remoting.udt.server;

import java.util.List;

import com.lanshi.microbe.remoting.udt.model.Order;

public interface IUdtService {
	public void testWithExcetion(Order order);

	public List<Order> queryOrder(String orderId);

	public boolean update(Order order);

}
