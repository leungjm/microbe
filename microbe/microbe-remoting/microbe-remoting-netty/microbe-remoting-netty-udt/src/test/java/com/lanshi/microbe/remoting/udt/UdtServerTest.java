package com.lanshi.microbe.remoting.udt;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.exection.ServerStopException;
import com.lanshi.microbe.remoting.udt.server.NettyUdtServer;

@ContextConfiguration(locations = SpringContainerImpl.SPRING_XML_PATH)
public class UdtServerTest extends AbstractJUnit4SpringContextTests {

	@Resource
	private NettyUdtServer nettyServer;

	@Test(expected = ServerStopException.class)
	public void stopServerWithoutStart() {
		nettyServer.stop();
	}

	@Test
	public void stopServer() throws InterruptedException {
		nettyServer.start("", 1234);
		nettyServer.stop();
	}
}
