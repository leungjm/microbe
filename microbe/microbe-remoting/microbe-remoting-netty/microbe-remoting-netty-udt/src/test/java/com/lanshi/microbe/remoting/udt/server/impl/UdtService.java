package com.lanshi.microbe.remoting.udt.server.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.lanshi.microbe.remoting.udt.model.Order;
import com.lanshi.microbe.remoting.udt.server.IUdtService;
import com.lanshi.microbe.remoting.udt.server.UdtSystemException;

@Service
public class UdtService implements IUdtService {

	@Override
	public void testWithExcetion(Order order) {
		System.out.println(order.getOrderId() + " " + order.getName() + " "
				+ order.getMoney());
		throw new UdtSystemException();
	}

	@Override
	public List<Order> queryOrder(String orderId) {
		System.out.println("orderId:" + orderId);
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order("10221", "neworder", 1458, false));
		return orders;
	}

	@Override
	public boolean update(Order order) {
		System.out.println(order.getOrderId() + " " + order.getName() + " "
				+ order.getMoney());
		return true;
	}

}
