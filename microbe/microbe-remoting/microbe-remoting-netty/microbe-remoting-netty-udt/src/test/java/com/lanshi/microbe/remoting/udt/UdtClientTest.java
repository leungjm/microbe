package com.lanshi.microbe.remoting.udt;

import java.net.InetSocketAddress;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.exection.ClientCloseException;
import com.lanshi.microbe.remoting.udt.client.NettyUdtClient;
import com.lanshi.microbe.remoting.udt.server.NettyUdtServer;

@ContextConfiguration(locations = SpringContainerImpl.SPRING_XML_PATH)
public class UdtClientTest extends AbstractJUnit4SpringContextTests {

	private final String ip = "localhost";
	private final int port = 2345;

	@Resource
	private NettyUdtServer nettyServer;

	@Resource
	private NettyUdtClient nettyClient;

	@Test(expected = ClientCloseException.class)
	public void closeClientWithoutConnect() {
		nettyClient.close();
	}

	@Test
	public void closeClient() {
		nettyServer.start(ip, port);
		nettyClient.connect(new InetSocketAddress(ip, port));
		nettyClient.close();
	}
}
