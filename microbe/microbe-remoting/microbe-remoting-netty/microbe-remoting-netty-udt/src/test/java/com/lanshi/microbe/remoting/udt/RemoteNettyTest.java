package com.lanshi.microbe.remoting.udt;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.udt.asserter.OrderResponseAssert;
import com.lanshi.microbe.remoting.udt.client.NettyUdtClient;
import com.lanshi.microbe.remoting.udt.model.Order;
import com.lanshi.microbe.remoting.udt.server.IUdtService;
import com.lanshi.microbe.remoting.udt.server.NettyUdtServer;

@ContextConfiguration(locations = SpringContainerImpl.SPRING_XML_PATH)
public final class RemoteNettyTest extends AbstractJUnit4SpringContextTests {

	private static volatile boolean serverStarted = false;

	private int port = 6099;
	private String ip = "localhost";

	@Resource
	private NettyUdtServer nettyServer;

	@Resource
	private NettyUdtClient nettyClient;

	@Before
	public void setUp() {
		if (!serverStarted) {
			nettyServer.start(ip, port);
			serverStarted = true;
		}
		nettyClient.connect(new InetSocketAddress(ip, port));
	}

	@Test
	public void cannotFoundMethod() {
		Response response = nettyClient.sendRequest(new Request(
				IUdtService.class, "notExist"));
		OrderResponseAssert.assertCauseException(response,
				ReflectiveOperationException.class);
	}

	@Test
	public void update() {
		Response response = nettyClient.sendRequest(new Request(
				IUdtService.class, "update", new Order("10112", "Order", 201,
						true)));
		OrderResponseAssert.assertHasReturnValue(response, Boolean.class);
	}

	@Test
	public void queryForSync() {
		for (int i = 0; i < 10; i++) {
			Response response = nettyClient.sendRequest(new Request(
					IUdtService.class, "queryOrder", "bar" + i));
			OrderResponseAssert.assertHasReturnValue(response, List.class);
		}
	}

	@Test
	public void queryForAsync() throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newFixedThreadPool(30);
		for (int i = 0; i < 10; i++) {
			Response response = executorService.submit(
					new AsyncQueryCallable(nettyClient, "bar" + i)).get();
			OrderResponseAssert.assertHasReturnValue(response, List.class);
		}
	}

	@Test
	public void queryWithSystemException() {
		Response response = nettyClient.sendRequest(new Request(
				IUdtService.class, "testWithExcetion"));
		OrderResponseAssert.assertException(response, ServerException.class);
	}

	@Test
	public void queryMix() throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		Response normalQueryResponse = executorService.submit(
				new AsyncQueryCallable(nettyClient, "normal")).get();
		Response slowQueryResponse = executorService.submit(
				new AsyncQueryCallable(nettyClient, "slow")).get();
		OrderResponseAssert.assertHasReturnValue(slowQueryResponse, List.class);
		OrderResponseAssert.assertHasReturnValue(normalQueryResponse,
				List.class);
		OrderResponseAssert.assertHasReturnValue(normalQueryResponse,
				"neworder");
	}
}
