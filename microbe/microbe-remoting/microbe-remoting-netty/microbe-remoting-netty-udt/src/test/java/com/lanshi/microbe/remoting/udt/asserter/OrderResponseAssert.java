package com.lanshi.microbe.remoting.udt.asserter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.udt.model.Order;

public final class OrderResponseAssert {

	private OrderResponseAssert() {
	}

	public static void assertException(final Response actual,
			final Class<? extends Throwable> expectedException) {
		assertThat(actual.getReturnValue(), nullValue());
		assertThat(actual.getException(), instanceOf(expectedException));
	}

	public static void assertCauseException(final Response actual,
			final Class<? extends Throwable> expectedException) {
		assertThat(actual.getReturnValue(), nullValue());
		assertThat(actual.getException().getCause(),
				instanceOf(expectedException));
	}

	public static void assertNoReturnValue(final Response actual) {
		assertThat(actual.getReturnValue(), nullValue());
		assertThat(actual.getException(), nullValue());
	}

	public static void assertHasReturnValue(final Response actual,
			final String expected) {
		assertThat(((List<Order>) actual.getReturnValue()).get(0).getName(),
				is(expected));
		assertThat(actual.getException(), nullValue());
	}

	public static void assertHasReturnValue(final Response actual,
			final Class<?> expected) {
		assertThat(actual.getReturnValue(), instanceOf(expected));
		assertThat(actual.getException(), nullValue());
	}

}
