package com.lanshi.microbe.remoting.udt.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.udt.nio.NioUdtProvider;
import io.netty.util.concurrent.ExecutorServiceFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.exection.ServerStopException;
import com.lanshi.microbe.remoting.server.IServer;
import com.lanshi.microbe.remoting.udt.codec.SerializeType;
import com.lanshi.microbe.remoting.udt.cpu.NameExecutorServiceFactory;

@Component
public class NettyUdtServer implements IServer, ApplicationContextAware {

	private ApplicationContext applicationContext;

	private Channel channel;

	@Value("${server.boss.group.threads}")
	private int bossGroupThreads;

	@Value("${server.worker.group.threads}")
	private int workerGroupThreads;

	@Value("${server.backlog.size}")
	private int backlogSize;

	@Value("${serialize.type}")
	private SerializeType serializeType;

	// acceptor deal thread factory
	private ExecutorServiceFactory bossFactory = new NameExecutorServiceFactory(
			"boss");
	// connecter deal thread factory
	private ExecutorServiceFactory workerFactory = new NameExecutorServiceFactory(
			"worker");
	private NioEventLoopGroup bossGroup = new NioEventLoopGroup(
			bossGroupThreads, bossFactory, NioUdtProvider.BYTE_PROVIDER);
	private NioEventLoopGroup workerGroup = new NioEventLoopGroup(
			workerGroupThreads, workerFactory, NioUdtProvider.BYTE_PROVIDER);

	@Override
	public void start(final String host, final int port) {
		// 放里面每次新建Executor,放外面共用，可以避免线程膨胀
		// bossFactory = new NameExecutorServiceFactory("boss");
		// // connecter deal thread factory
		// workerFactory = new NameExecutorServiceFactory("worker");
		// bossGroup = new NioEventLoopGroup(bossGroupThreads, bossFactory,
		// NioUdtProvider.BYTE_PROVIDER);
		// workerGroup = new NioEventLoopGroup(workerGroupThreads,
		// workerFactory,
		// NioUdtProvider.BYTE_PROVIDER);

		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
					.channelFactory(NioUdtProvider.BYTE_ACCEPTOR)
					.option(ChannelOption.SO_BACKLOG, backlogSize)
					.childHandler(getHandler());
			InetAddress address = null;
			if (host == null || host.equals("")) {
				address = InetAddress.getLocalHost();
			} else {
				address = InetAddress.getByName(host);
			}
			channel = b.bind(address, port).sync().channel();
			System.out.println("Netty UDT Server started at port " + port);
		} catch (InterruptedException | UnknownHostException e) {
			throw new ServerException(IServer.SYSTEM_MESSAGE_ID, e);
		}
	}

	private ChannelHandler getHandler() {
		System.out.println("Server start with serialize type:"
				+ serializeType.getServerChannelInitializer());
		return applicationContext.getBean(serializeType
				.getServerChannelInitializer());
	}

	@Override
	public boolean stop() {
		if (null == channel) {
			throw new ServerStopException();
		}
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		channel.closeFuture().syncUninterruptibly();
		bossGroup = null;
		workerGroup = null;
		channel = null;
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
}
