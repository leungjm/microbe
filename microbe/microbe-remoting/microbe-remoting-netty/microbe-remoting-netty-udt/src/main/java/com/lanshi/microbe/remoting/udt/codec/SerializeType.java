package com.lanshi.microbe.remoting.udt.codec;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;

import com.lanshi.microbe.remoting.udt.initializer.NettyClientChannelInitializer;
import com.lanshi.microbe.remoting.udt.initializer.client.JavaClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.udt.initializer.client.KryoClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.udt.initializer.server.JavaServerUdtChannelInitializer;
import com.lanshi.microbe.remoting.udt.initializer.server.KryoServerUdtChannelInitializer;

public enum SerializeType {

	Java(JavaServerUdtChannelInitializer.class,
			JavaClientUdtChannelInitializer.class),

	Kryo(KryoServerUdtChannelInitializer.class,
			KryoClientUdtChannelInitializer.class);

	private final Class<? extends ChannelInitializer<UdtChannel>> serverChannelInitializer;
	private final Class<? extends NettyClientChannelInitializer> clientChannelInitializer;

	private SerializeType(
			final Class<? extends ChannelInitializer<UdtChannel>> serverChannelInitializer,
			final Class<? extends NettyClientChannelInitializer> clientChannelInitializer) {
		this.serverChannelInitializer = serverChannelInitializer;
		this.clientChannelInitializer = clientChannelInitializer;
	}

	public Class<? extends ChannelInitializer<UdtChannel>> getServerChannelInitializer() {
		return serverChannelInitializer;
	}

	public Class<? extends NettyClientChannelInitializer> getClientChannelInitializer() {
		return clientChannelInitializer;
	}
}
