package com.lanshi.microbe.remoting.udt.server;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.lanshi.microbe.container.spring.SpringContainerImpl;

public final class ServerBootstrap {

	private static AbstractApplicationContext ctx;

	public static void main(String[] args) {
		SpringContainerImpl container = startContainer();
		ctx = container.getContext().get();
		startServer();
	}

	private static SpringContainerImpl startContainer() {
		SpringContainerImpl container = new SpringContainerImpl();
		container.start();
		return container;
	}

	private static void startServer() {
		Object port = ctx.getBean(PropertySourcesPlaceholderConfigurer.class)
				.getAppliedPropertySources().get("localProperties")
				.getProperty("server.port");
		ctx.getBean(NettyUdtServer.class).start("",
				Integer.parseInt(port.toString()));
	}

	public static void stopServer() {
		ctx.getBean(NettyUdtServer.class).stop();
	}
}
