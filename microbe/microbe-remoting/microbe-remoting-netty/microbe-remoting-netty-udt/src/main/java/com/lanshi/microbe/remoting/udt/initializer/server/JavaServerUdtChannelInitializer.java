package com.lanshi.microbe.remoting.udt.initializer.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.udt.handler.InboundServerDispatcherHandler;

@Component
public class JavaServerUdtChannelInitializer extends
		ChannelInitializer<UdtChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;

	@Override
	protected void initChannel(UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(
				new ObjectDecoder(ClassResolvers.cacheDisabled(this.getClass()
						.getClassLoader())));
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
