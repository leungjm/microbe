package com.lanshi.microbe.remoting.udt.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.udt.nio.NioUdtProvider;
import io.netty.util.concurrent.ExecutorServiceFactory;

import java.net.InetSocketAddress;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.exection.ClientCloseException;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.udt.codec.SerializeType;
import com.lanshi.microbe.remoting.udt.cpu.NameExecutorServiceFactory;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NettyUdtClient implements IClient, ApplicationContextAware {
	private ApplicationContext applicationContext;

	@Value("${server.worker.group.threads}")
	private int workerGroupThreads;
	@Value("${serialize.type}")
	private SerializeType serializeType;

	// connecter deal thread factory
	private ExecutorServiceFactory workerFactory = new NameExecutorServiceFactory(
			"worker");

	private NioEventLoopGroup workerGroup = new NioEventLoopGroup(
			workerGroupThreads, workerFactory, NioUdtProvider.BYTE_PROVIDER);
	private Channel channel;

	@Override
	public void connect(final InetSocketAddress socketAddress) {
		// workerGroup = new NioEventLoopGroup(workerGroupThreads);
		Bootstrap bootstrap = new Bootstrap();
		bootstrap.group(workerGroup)
				.channelFactory(NioUdtProvider.BYTE_CONNECTOR)
				// .option(ChannelOption.SO_KEEPALIVE, true)
				// .option(ChannelOption.TCP_NODELAY, true)
				.handler(getHandler());
		channel = bootstrap
				.connect(socketAddress.getAddress().getHostAddress(),
						socketAddress.getPort()).syncUninterruptibly()
				.channel();
	}

	private ChannelHandler getHandler() {
		return applicationContext.getBean(serializeType
				.getClientChannelInitializer());
	}

	@Override
	public Response sendRequest(final Request request) {
		channel.writeAndFlush(request);
		return applicationContext.getBean(
				serializeType.getClientChannelInitializer()).getResponse(
				request.getMessageId());
	}

	@Override
	public void close() {
		if (null == channel) {
			throw new ClientCloseException();
		}
		workerGroup.shutdownGracefully();
		channel.closeFuture().syncUninterruptibly();
		workerGroup = null;
		channel = null;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
}
