package com.lanshi.microbe.remoting.udt.handler;

import static java.util.concurrent.TimeUnit.SECONDS;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.exection.ClientException;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.server.IServer;

@Component
@Sharable
public class InboundClientDispartcherHandler extends
		SimpleChannelInboundHandler<Response> {
	private final ConcurrentHashMap<Long, BlockingQueue<Response>> responseMap = new ConcurrentHashMap<>();

	@Override
	public void write(ChannelHandlerContext ctx, Object msg,
			ChannelPromise promise) throws Exception {
		if (msg instanceof Request) {
			Request request = (Request) msg;
			responseMap.putIfAbsent(request.getMessageId(),
					new LinkedBlockingQueue<Response>(1));
		}
		super.write(ctx, msg, promise);
	}

	@Override
	protected void messageReceived(ChannelHandlerContext ctx, Response response)
			throws Exception {
		BlockingQueue<Response> queue = responseMap
				.get(response.getMessageId());
		queue.add(response);
	}

	public Response getResponse(long messageId) {
		Response result;
		responseMap
				.putIfAbsent(messageId, new LinkedBlockingQueue<Response>(1));
		try {
			result = responseMap.get(messageId).take();
			if (null == result) {
				result = getSystemMessage();
			}
		} catch (final InterruptedException ex) {
			throw new ClientException(ex);
		} finally {
			responseMap.remove(messageId);
		}
		return result;
	}

	private Response getSystemMessage() {
		try {
			return responseMap.get(IServer.SYSTEM_MESSAGE_ID).poll(5, SECONDS);
		} catch (final InterruptedException ex) {
			throw new ClientException(ex);
		}
	}
}
