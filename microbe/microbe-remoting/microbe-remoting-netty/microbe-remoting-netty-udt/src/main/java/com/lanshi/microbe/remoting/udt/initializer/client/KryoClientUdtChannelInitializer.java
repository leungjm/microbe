package com.lanshi.microbe.remoting.udt.initializer.client;

import io.netty.channel.udt.UdtChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.udt.codec.kyro.KryoDecoder;
import com.lanshi.microbe.remoting.udt.codec.kyro.KryoEncoder;
import com.lanshi.microbe.remoting.udt.codec.kyro.KryoPool;
import com.lanshi.microbe.remoting.udt.initializer.NettyClientChannelInitializer;

@Component
public class KryoClientUdtChannelInitializer extends
		NettyClientChannelInitializer {

	@Resource
	private KryoPool kryoSerializationFactory;

	@Override
	protected void initChannel(final UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new KryoEncoder(kryoSerializationFactory));
		ch.pipeline().addLast(new KryoDecoder(kryoSerializationFactory));
		super.initChannel(ch);
	}

}
