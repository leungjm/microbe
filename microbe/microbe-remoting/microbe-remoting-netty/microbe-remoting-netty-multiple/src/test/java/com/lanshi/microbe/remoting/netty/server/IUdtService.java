package com.lanshi.microbe.remoting.netty.server;

import java.net.InetSocketAddress;
import java.util.List;

import com.lanshi.microbe.remoting.netty.model.Order;

public interface IUdtService {
	public void testWithExcetion(Order order);

	public List<Order> queryOrder(String orderId);

	public boolean update(Order order);

	public InetSocketAddress sendInetSocketAddress(InetSocketAddress addr);

}
