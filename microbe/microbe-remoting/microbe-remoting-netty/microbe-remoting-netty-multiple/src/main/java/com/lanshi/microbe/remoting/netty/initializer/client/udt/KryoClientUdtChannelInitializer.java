package com.lanshi.microbe.remoting.netty.initializer.client.udt;

import io.netty.channel.udt.UdtChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.kyro.KryoDecoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoEncoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoPool;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientUdtChannelInitializer;

@Component
public class KryoClientUdtChannelInitializer extends
		NettyClientUdtChannelInitializer {

	@Resource
	private KryoPool kryoSerializationFactory;

	@Override
	protected void initChannel(final UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new KryoEncoder(kryoSerializationFactory));
		ch.pipeline().addLast(new KryoDecoder(kryoSerializationFactory));
		super.initChannel(ch);
	}

}
