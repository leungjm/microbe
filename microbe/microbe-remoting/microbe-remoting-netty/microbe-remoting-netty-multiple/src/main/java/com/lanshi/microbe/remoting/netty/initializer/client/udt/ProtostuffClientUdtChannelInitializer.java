package com.lanshi.microbe.remoting.netty.initializer.client.udt;

import io.netty.channel.udt.UdtChannel;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffDecoder;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffEncoder;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientUdtChannelInitializer;

@Component
public class ProtostuffClientUdtChannelInitializer extends
		NettyClientUdtChannelInitializer {

	@Override
	protected void initChannel(final UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new ProtostuffEncoder(Request.class));
		ch.pipeline().addLast(new ProtostuffDecoder(Response.class));
		super.initChannel(ch);
	}

}
