package com.lanshi.microbe.remoting.netty.initializer.client.http;

import io.netty.channel.socket.SocketChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.kyro.KryoDecoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoEncoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoPool;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientHttpChannelInitializer;

@Component
public class KryoClientHttpChannelInitializer extends
		NettyClientHttpChannelInitializer {

	@Resource
	private KryoPool kryoSerializationFactory;

	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new KryoEncoder(kryoSerializationFactory));
		ch.pipeline().addLast(new KryoDecoder(kryoSerializationFactory));
		super.initChannel(ch);
	}

}
