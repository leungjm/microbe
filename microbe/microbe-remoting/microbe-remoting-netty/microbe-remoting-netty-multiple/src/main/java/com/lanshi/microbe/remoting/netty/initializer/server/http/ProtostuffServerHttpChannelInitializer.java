package com.lanshi.microbe.remoting.netty.initializer.server.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffDecoder;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffEncoder;
import com.lanshi.microbe.remoting.netty.handler.InboundServerDispatcherHandler;

@Component
public class ProtostuffServerHttpChannelInitializer extends
		ChannelInitializer<SocketChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new ProtostuffEncoder(Response.class));
		ch.pipeline().addLast(new ProtostuffDecoder(Request.class));
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
