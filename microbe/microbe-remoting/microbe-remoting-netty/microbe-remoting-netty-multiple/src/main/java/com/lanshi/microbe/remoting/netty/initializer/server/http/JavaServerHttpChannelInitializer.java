package com.lanshi.microbe.remoting.netty.initializer.server.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.handler.InboundServerDispatcherHandler;

@Component
public class JavaServerHttpChannelInitializer extends
		ChannelInitializer<SocketChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(
				new ObjectDecoder(ClassResolvers.cacheDisabled(this.getClass()
						.getClassLoader())));
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
