package com.lanshi.microbe.remoting.netty.cpu;

import io.netty.util.concurrent.ExecutorServiceFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NameExecutorServiceFactory implements ExecutorServiceFactory {
	// private static final AtomicInteger counter = new AtomicInteger();

	private final String name;

	public NameExecutorServiceFactory(final String name) {
		this.name = name;
	}

	@Override
	public ExecutorService newExecutorService(int parallelism) {
		return Executors.newFixedThreadPool(parallelism, new NameThreadFactory(
				name));
	}

}
