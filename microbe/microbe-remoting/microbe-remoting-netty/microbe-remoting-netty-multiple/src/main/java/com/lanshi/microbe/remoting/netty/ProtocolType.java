package com.lanshi.microbe.remoting.netty;

public enum ProtocolType {
	Http("http"), Udt("udt");

	private String type;

	private ProtocolType(final String type) {
		this.type = type;
	}

	public String type() {
		return type;
	}

	public static boolean isHttp(ProtocolType type) {
		return Http.type.equals(type.type);
	}

	public static boolean isUdt(ProtocolType type) {
		return Udt.type.equals(type.type);
	}
}
