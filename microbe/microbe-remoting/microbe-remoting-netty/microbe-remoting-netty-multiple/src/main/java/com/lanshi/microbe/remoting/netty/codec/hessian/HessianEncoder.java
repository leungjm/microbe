package com.lanshi.microbe.remoting.netty.codec.hessian;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import com.lanshi.microbe.serialize.hessian.HessianSerializeUtil;

public class HessianEncoder extends MessageToByteEncoder<Object> {

	@Override
	protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out)
			throws Exception {
		byte[] cont = HessianSerializeUtil.serialize(msg);
		out.writeInt(cont.length);
		out.writeBytes(cont);
	}

}
