package com.lanshi.microbe.remoting.netty.initializer.client.udt;

import io.netty.channel.udt.UdtChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientUdtChannelInitializer;

@Component
public class JavaClientUdtChannelInitializer extends
		NettyClientUdtChannelInitializer {

	@Override
	protected void initChannel(final UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(
				new ObjectDecoder(ClassResolvers.cacheDisabled(this.getClass()
						.getClassLoader())));
		super.initChannel(ch);
	}

}
