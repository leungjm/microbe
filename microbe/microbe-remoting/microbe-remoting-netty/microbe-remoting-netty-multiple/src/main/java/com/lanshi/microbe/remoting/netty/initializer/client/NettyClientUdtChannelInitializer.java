package com.lanshi.microbe.remoting.netty.initializer.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.handler.InboundClientDispartcherHandler;

@Component
public class NettyClientUdtChannelInitializer extends
		ChannelInitializer<UdtChannel> {

	@Resource
	private InboundClientDispartcherHandler clientDispatchHandler;

	@Override
	protected void initChannel(final UdtChannel ch) throws Exception {
		ch.pipeline().addLast(clientDispatchHandler);
	}

	public Response getResponse(final long messageId) {
		return clientDispatchHandler.getResponse(messageId);
	}
}
