package com.lanshi.microbe.remoting.netty.codec;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.http.HessianClientHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.http.JavaClientHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.http.KryoClientHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.http.ProtostuffClientHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.http.HessianServerHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.http.JavaServerHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.http.KryoServerHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.http.ProtostuffServerHttpChannelInitializer;

public enum SerializeHttpType {

	Java(JavaServerHttpChannelInitializer.class,
			JavaClientHttpChannelInitializer.class),

	Kryo(KryoServerHttpChannelInitializer.class,
			KryoClientHttpChannelInitializer.class),

	Protostuff(ProtostuffServerHttpChannelInitializer.class,
			ProtostuffClientHttpChannelInitializer.class),

	Hessian(HessianServerHttpChannelInitializer.class,
			HessianClientHttpChannelInitializer.class);

	private final Class<? extends ChannelInitializer<SocketChannel>> serverChannelInitializer;
	private final Class<? extends NettyClientHttpChannelInitializer> clientChannelInitializer;

	private SerializeHttpType(
			final Class<? extends ChannelInitializer<SocketChannel>> serverChannelInitializer,
			final Class<? extends NettyClientHttpChannelInitializer> clientChannelInitializer) {
		this.serverChannelInitializer = serverChannelInitializer;
		this.clientChannelInitializer = clientChannelInitializer;
	}

	public Class<? extends ChannelInitializer<SocketChannel>> getServerChannelInitializer() {
		return serverChannelInitializer;
	}

	public Class<? extends NettyClientHttpChannelInitializer> getClientChannelInitializer() {
		return clientChannelInitializer;
	}
}
