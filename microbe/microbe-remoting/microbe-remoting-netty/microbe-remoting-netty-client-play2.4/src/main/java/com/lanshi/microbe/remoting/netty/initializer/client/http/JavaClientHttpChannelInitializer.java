package com.lanshi.microbe.remoting.netty.initializer.client.http;

import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientHttpChannelInitializer;

@Component
public class JavaClientHttpChannelInitializer extends
		NettyClientHttpChannelInitializer {

	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(
				new ObjectDecoder(ClassResolvers.cacheDisabled(this.getClass()
						.getClassLoader())));
		super.initChannel(ch);
	}

}
