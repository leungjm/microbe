package com.lanshi.microbe.remoting.netty.codec.protostuff;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import com.lanshi.microbe.serialize.protostuff.ProtostuffUtil;

public class ProtostuffEncoder extends MessageToByteEncoder<Object> {
	private Class<?> genericClass;

	public ProtostuffEncoder(Class<?> genericClass) {
		this.genericClass = genericClass;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out)
			throws Exception {
		if (genericClass.isInstance(msg)) {
			byte[] data = ProtostuffUtil.serialize(msg);
			out.writeInt(data.length);
			out.writeBytes(data);
		}
	}

}
