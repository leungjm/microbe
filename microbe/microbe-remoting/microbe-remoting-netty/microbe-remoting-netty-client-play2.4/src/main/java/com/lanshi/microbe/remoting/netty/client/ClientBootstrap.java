package com.lanshi.microbe.remoting.netty.client;

import java.net.InetSocketAddress;

import org.springframework.context.support.AbstractApplicationContext;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.rmiService.IHelloWorld;

public class ClientBootstrap {

	private static AbstractApplicationContext ctx;

	public static void main(String[] args) {
		SpringContainerImpl container = startContainer();
		ctx = container.getContext().get();
		testClient();
	}

	private static void testClient() {
		IClient client = ctx.getBean(IClient.class);
		client.connect(new InetSocketAddress("localhost", 7099));
		Response response = client.sendRequest(new Request(IHelloWorld.class,
				"hello", "liangjingmin", 28));
		System.out.println(response.getReturnValue() + "" + response.getException());
	}

	private static SpringContainerImpl startContainer() {
		SpringContainerImpl container = new SpringContainerImpl();
		container.start();
		return container;
	}


}
