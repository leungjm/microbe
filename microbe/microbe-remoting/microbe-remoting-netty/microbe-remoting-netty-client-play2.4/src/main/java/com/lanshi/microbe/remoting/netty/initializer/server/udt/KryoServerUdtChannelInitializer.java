package com.lanshi.microbe.remoting.netty.initializer.server.udt;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.kyro.KryoDecoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoEncoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoPool;
import com.lanshi.microbe.remoting.netty.handler.InboundServerDispatcherHandler;

@Component
public class KryoServerUdtChannelInitializer extends
		ChannelInitializer<UdtChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;
	@Resource
	private KryoPool kryoSerializationFactory;

	@Override
	protected void initChannel(UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new KryoEncoder(kryoSerializationFactory));
		ch.pipeline().addLast(new KryoDecoder(kryoSerializationFactory));
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
