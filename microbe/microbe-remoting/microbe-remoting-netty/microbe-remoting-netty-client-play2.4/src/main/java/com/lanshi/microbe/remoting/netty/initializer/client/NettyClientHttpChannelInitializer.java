package com.lanshi.microbe.remoting.netty.initializer.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.handler.InboundClientDispartcherHandler;

@Component
public class NettyClientHttpChannelInitializer extends
		ChannelInitializer<SocketChannel> {

	@Resource
	private InboundClientDispartcherHandler clientDispatchHandler;

	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(clientDispatchHandler);
	}

	public void write(Request request) {
		clientDispatchHandler.write(request);
	}

	public Response getResponse(final long messageId) {
		return clientDispatchHandler.getResponse(messageId);
	}
}
