package com.lanshi.microbe.remoting.netty.handler;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.lang.reflect.Method;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.exection.SystemException;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.server.IServer;

@Component
@Sharable
// 注解@Sharable可以让它在channels间共享
public class InboundServerDispatcherHandler extends
		SimpleChannelInboundHandler<Request> implements ApplicationContextAware {
	private ApplicationContext applicationContext;

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, final Request request)
			throws Exception {
		System.out.println("server recv msg : " + request);
		Object result = invoke(request);
		ctx.writeAndFlush(new Response(request.getMessageId(), result));
	}

//	@Override
//	protected void messageReceived(ChannelHandlerContext ctx,
//			final Request request) throws Exception {
//		System.out.println("server recv msg : " + request);
//		Object result = invoke(request);
//		ctx.writeAndFlush(new Response(request.getMessageId(), result));
//	}

	private Object invoke(Request request) {
		Object result = null;
		try {
			Object apiInstance = getApiInstance(request.getApiClass());
			Method method = getRequestMethod(apiInstance, request.getMethod(),
					request.getParameters());
			result = method.invoke(apiInstance, request.getParameters());
		} catch (final ReflectiveOperationException | SystemException ex) {
			throw new ServerException(request.getMessageId(), ex);
		}
		return result;
	}

	private Method getRequestMethod(Object apiInstance, String methName,
			Object[] parameters) throws NoSuchMethodException,
			SecurityException {

		return apiInstance.getClass().getMethod(methName,
				getParameterTypes(parameters));
	}

	private Class<?>[] getParameterTypes(final Object[] parameters) {
		Class<?>[] result = new Class<?>[parameters.length];
		int i = 0;
		for (Object each : parameters) {
			result[i] = each.getClass();
			i++;
		}
		return result;
	}

	private Object getApiInstance(Class<?> apiClass) {

		Object obj = applicationContext.getBean(apiClass);
		return obj;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		Response response;
		if (cause instanceof ServerException) {
			response = new Response(((ServerException) cause).getMessageId(),
					cause);
		} else {
			response = new Response(IServer.SYSTEM_MESSAGE_ID, cause);
		}
		ctx.writeAndFlush(response);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
