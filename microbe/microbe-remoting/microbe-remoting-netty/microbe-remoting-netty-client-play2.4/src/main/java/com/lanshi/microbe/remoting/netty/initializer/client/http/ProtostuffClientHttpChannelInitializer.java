package com.lanshi.microbe.remoting.netty.initializer.client.http;

import io.netty.channel.socket.SocketChannel;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffDecoder;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffEncoder;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientHttpChannelInitializer;

@Component
public class ProtostuffClientHttpChannelInitializer extends
		NettyClientHttpChannelInitializer {

	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new ProtostuffEncoder(Request.class));
		ch.pipeline().addLast(new ProtostuffDecoder(Response.class));
		super.initChannel(ch);
	}

}
