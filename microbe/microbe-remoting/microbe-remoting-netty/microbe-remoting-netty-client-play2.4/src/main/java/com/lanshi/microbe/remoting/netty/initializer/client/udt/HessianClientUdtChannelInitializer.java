package com.lanshi.microbe.remoting.netty.initializer.client.udt;

import io.netty.channel.udt.UdtChannel;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.hessian.HessianDecoder;
import com.lanshi.microbe.remoting.netty.codec.hessian.HessianEncoder;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientUdtChannelInitializer;

@Component
public class HessianClientUdtChannelInitializer extends
		NettyClientUdtChannelInitializer {

	@Override
	protected void initChannel(final UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new HessianEncoder());
		ch.pipeline().addLast(new HessianDecoder());
		super.initChannel(ch);
	}

}
