package com.lanshi.microbe.remoting.netty.initializer.client.http;

import io.netty.channel.socket.SocketChannel;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.hessian.HessianDecoder;
import com.lanshi.microbe.remoting.netty.codec.hessian.HessianEncoder;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientHttpChannelInitializer;

@Component
public class HessianClientHttpChannelInitializer extends
		NettyClientHttpChannelInitializer {

	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new HessianEncoder());
		ch.pipeline().addLast(new HessianDecoder());
		super.initChannel(ch);
	}

}
