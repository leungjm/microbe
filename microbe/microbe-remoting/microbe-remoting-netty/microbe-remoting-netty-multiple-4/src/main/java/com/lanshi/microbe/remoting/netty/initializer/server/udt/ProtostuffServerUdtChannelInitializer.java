package com.lanshi.microbe.remoting.netty.initializer.server.udt;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffDecoder;
import com.lanshi.microbe.remoting.netty.codec.protostuff.ProtostuffEncoder;
import com.lanshi.microbe.remoting.netty.handler.InboundServerDispatcherHandler;

@Component
public class ProtostuffServerUdtChannelInitializer extends
		ChannelInitializer<UdtChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;

	@Override
	protected void initChannel(UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new ProtostuffEncoder(Response.class));
		ch.pipeline().addLast(new ProtostuffDecoder(Request.class));
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
