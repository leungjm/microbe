package com.lanshi.microbe.remoting.netty.codec;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;

import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.udt.HessianClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.udt.JavaClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.udt.KryoClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.udt.ProtostuffClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.udt.HessianServerUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.udt.JavaServerUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.udt.KryoServerUdtChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.server.udt.ProtostuffServerUdtChannelInitializer;

public enum SerializeUdtType {

	Java(JavaServerUdtChannelInitializer.class,
			JavaClientUdtChannelInitializer.class),

	Kryo(KryoServerUdtChannelInitializer.class,
			KryoClientUdtChannelInitializer.class),

	Protostuff(ProtostuffServerUdtChannelInitializer.class,
			ProtostuffClientUdtChannelInitializer.class),

	Hessian(HessianServerUdtChannelInitializer.class,
			HessianClientUdtChannelInitializer.class);

	private final Class<? extends ChannelInitializer<UdtChannel>> serverChannelInitializer;
	private final Class<? extends NettyClientUdtChannelInitializer> clientChannelInitializer;

	private SerializeUdtType(
			final Class<? extends ChannelInitializer<UdtChannel>> serverChannelInitializer,
			final Class<? extends NettyClientUdtChannelInitializer> clientChannelInitializer) {
		this.serverChannelInitializer = serverChannelInitializer;
		this.clientChannelInitializer = clientChannelInitializer;
	}

	public Class<? extends ChannelInitializer<UdtChannel>> getServerChannelInitializer() {
		return serverChannelInitializer;
	}

	public Class<? extends NettyClientUdtChannelInitializer> getClientChannelInitializer() {
		return clientChannelInitializer;
	}
}
