package com.lanshi.microbe;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.udt.UdtChannel;
import io.netty.channel.udt.nio.NioUdtProvider;

import java.net.InetSocketAddress;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.ProtocolType;
import com.lanshi.microbe.remoting.netty.codec.SerializeHttpType;
import com.lanshi.microbe.remoting.netty.codec.SerializeUdtType;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoDecoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoEncoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoPool;
import com.lanshi.microbe.remoting.netty.cpu.NameExecutorServiceFactory;
import com.lanshi.microbe.remoting.netty.handler.InboundClientDispartcherHandler;
import com.lanshi.microbe.rmiService.IHelloWorld;

public class IClientTest {
	private int workerGroupThreads = 5;
	private SerializeUdtType serializeUdtType = SerializeUdtType.Kryo;
	private SerializeHttpType serializeHttpType = SerializeHttpType.Kryo;
	private ProtocolType protocolType = ProtocolType.Udt;

	// connecter deal thread factory
	private NameExecutorServiceFactory workerFactory;

	private NioEventLoopGroup workerGroup;
	private Channel channel;
	KryoPool kryoSerializationFactory = new KryoPool();
	InboundClientDispartcherHandler clientDispatchHandler = new InboundClientDispartcherHandler();

	public static void main(String[] args) {
		IClientTest client = new IClientTest();
		client.connect(new InetSocketAddress("localhost", 7099));
		client.sendRequest();
	}

	public void connect(InetSocketAddress socketAddress) {
		Bootstrap bootstrap = new Bootstrap();
		if (ProtocolType.isUdt(protocolType)) {
			workerFactory = new NameExecutorServiceFactory("worker");
			workerGroup = new NioEventLoopGroup(workerGroupThreads,
					workerFactory, NioUdtProvider.BYTE_PROVIDER);
			bootstrap.group(workerGroup)
					.channelFactory(NioUdtProvider.BYTE_CONNECTOR)
					.handler(new ChannelInitializer<UdtChannel>() {

						@Override
						protected void initChannel(UdtChannel ch)
								throws Exception {
							ch.pipeline().addLast(
									new KryoEncoder(kryoSerializationFactory));
							ch.pipeline().addLast(
									new KryoDecoder(kryoSerializationFactory));
							ch.pipeline().addLast(clientDispatchHandler);
						}
					});
			channel = bootstrap
					.connect(socketAddress.getAddress().getHostAddress(),
							socketAddress.getPort()).syncUninterruptibly()
					.channel();
		}

		System.out.println("Netty client connect server : {} with potocol {}"
				+ socketAddress + protocolType.type());

	}

	public void sendRequest() {
		Request request = new Request(IHelloWorld.class, "hello",
				"liangjingmin", 28);
		clientDispatchHandler.write(request);
		channel.writeAndFlush(request);
		Response resp = clientDispatchHandler.getResponse(request
				.getMessageId());
		System.out.println(resp.getMessageId());
		System.out.println(resp.getReturnValue());
		System.out.println(resp.getException());
	}

}
