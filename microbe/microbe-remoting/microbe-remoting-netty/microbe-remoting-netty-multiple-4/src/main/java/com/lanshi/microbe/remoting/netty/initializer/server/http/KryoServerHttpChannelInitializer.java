package com.lanshi.microbe.remoting.netty.initializer.server.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.kyro.KryoDecoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoEncoder;
import com.lanshi.microbe.remoting.netty.codec.kyro.KryoPool;
import com.lanshi.microbe.remoting.netty.handler.InboundServerDispatcherHandler;

@Component
public class KryoServerHttpChannelInitializer extends
		ChannelInitializer<SocketChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;
	@Resource
	private KryoPool kryoSerializationFactory;

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new KryoEncoder(kryoSerializationFactory));
		ch.pipeline().addLast(new KryoDecoder(kryoSerializationFactory));
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
