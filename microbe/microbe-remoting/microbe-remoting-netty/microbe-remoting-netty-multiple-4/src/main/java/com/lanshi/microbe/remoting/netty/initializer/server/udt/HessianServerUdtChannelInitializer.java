package com.lanshi.microbe.remoting.netty.initializer.server.udt;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.udt.UdtChannel;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.remoting.netty.codec.hessian.HessianDecoder;
import com.lanshi.microbe.remoting.netty.codec.hessian.HessianEncoder;
import com.lanshi.microbe.remoting.netty.handler.InboundServerDispatcherHandler;

@Component
public class HessianServerUdtChannelInitializer extends
		ChannelInitializer<UdtChannel> {

	@Resource
	private InboundServerDispatcherHandler serverDispatcherHandler;

	@Override
	protected void initChannel(UdtChannel ch) throws Exception {
		ch.pipeline().addLast(new HessianEncoder());
		ch.pipeline().addLast(new HessianDecoder());
		ch.pipeline().addLast(serverDispatcherHandler);
	}

}
