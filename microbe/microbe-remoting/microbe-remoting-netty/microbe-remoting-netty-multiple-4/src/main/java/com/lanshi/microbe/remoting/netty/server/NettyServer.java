package com.lanshi.microbe.remoting.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.udt.nio.NioUdtProvider;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.exection.ServerStopException;
import com.lanshi.microbe.remoting.netty.ProtocolType;
import com.lanshi.microbe.remoting.netty.codec.SerializeHttpType;
import com.lanshi.microbe.remoting.netty.codec.SerializeUdtType;
import com.lanshi.microbe.remoting.netty.cpu.NameExecutorServiceFactory;
import com.lanshi.microbe.remoting.server.IServer;

@Component
public class NettyServer implements IServer, ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Value("${server.boss.group.threads}")
	private int bossGroupThreads;

	@Value("${server.worker.group.threads}")
	private int workerGroupThreads;

	@Value("${server.backlog.size}")
	private int backlogSize;

	@Value("${serialize.type}")
	private SerializeUdtType serializeUdtType;

	@Value("${serialize.type}")
	private SerializeHttpType serializeHttpType;

	@Value("${server.protocal}")
	private ProtocolType protocolType;

	// acceptor deal thread factory
	private NameExecutorServiceFactory bossFactory;
	// connecter deal thread factory
	private NameExecutorServiceFactory workerFactory;

	private NioEventLoopGroup bossGroup;
	private NioEventLoopGroup workerGroup;

	private Channel channel;

	@Override
	public void start(final String host, final int port) {
		ServerBootstrap b = new ServerBootstrap();
		bossFactory = new NameExecutorServiceFactory("boss");
		workerFactory = new NameExecutorServiceFactory("worker");
		if (ProtocolType.isUdt(protocolType)) {
			bossGroup = new NioEventLoopGroup(bossGroupThreads, bossFactory,
					NioUdtProvider.BYTE_PROVIDER);
			workerGroup = new NioEventLoopGroup(workerGroupThreads,
					workerFactory, NioUdtProvider.BYTE_PROVIDER);

			b.group(bossGroup, workerGroup)
					.channelFactory(NioUdtProvider.BYTE_ACCEPTOR)
					.option(ChannelOption.SO_BACKLOG, backlogSize)
					.childHandler(getHandler());

		}
		if (ProtocolType.isHttp(protocolType)) {
			bossGroup = new NioEventLoopGroup(bossGroupThreads, bossFactory);
			workerGroup = new NioEventLoopGroup(workerGroupThreads,
					workerFactory);
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.option(ChannelOption.SO_BACKLOG, backlogSize)
					.childOption(ChannelOption.SO_KEEPALIVE, true)
					.childOption(ChannelOption.TCP_NODELAY, true)
					.childHandler(getHandler());
		}
		try {
			InetAddress address = null;
			if (host == null || host.equals("")) {
				address = InetAddress.getLocalHost();
			} else {
				address = InetAddress.getByName(host);
			}
			channel = b.bind(address, port).sync().channel();
			MicrobeLogger.info("Netty Server started at {} with protocol {}",
					channel.localAddress(), protocolType.type());
		} catch (InterruptedException | UnknownHostException e) {
			throw new ServerException(IServer.SYSTEM_MESSAGE_ID, e);
		}
	}

	private ChannelHandler getHandler() {
		if (ProtocolType.isUdt(protocolType)) {
			MicrobeLogger.info("Server start with serialize type:{}",
					serializeUdtType.getServerChannelInitializer());
			return applicationContext.getBean(serializeUdtType
					.getServerChannelInitializer());
		} else if (ProtocolType.isHttp(protocolType)) {
			MicrobeLogger.info("Server start with serialize type: {}",
					serializeHttpType.getServerChannelInitializer());
			return applicationContext.getBean(serializeHttpType
					.getServerChannelInitializer());
		}
		throw new ServerException(IServer.SYSTEM_MESSAGE_ID,
				new RuntimeException(protocolType.type() + " is not excepte!"));
	}

	@Override
	public boolean stop() {
		if (null == channel) {
			throw new ServerStopException();
		}
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		channel.closeFuture().syncUninterruptibly();
		bossGroup = null;
		workerGroup = null;
		channel = null;
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
}
