package com.lanshi.microbe.remoting.netty.handler;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.exection.ClientException;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.server.IServer;

@Component
@Sharable
public class InboundClientDispartcherHandler extends
		SimpleChannelInboundHandler<Response> {
	private final ConcurrentHashMap<Long, BlockingQueue<Response>> responseMap = new ConcurrentHashMap<>();
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, final Response response)
			throws Exception {
		BlockingQueue<Response> queue = responseMap
				.get(response.getMessageId());
		queue.add(response);
		
	}
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelReadComplete(ctx);
	}

	@Override
	public boolean acceptInboundMessage(Object msg) throws Exception {
		// TODO Auto-generated method stub
		return super.acceptInboundMessage(msg);
	}

//	@Override
//	public void write(ChannelHandlerContext ctx, Object msg,
//			ChannelPromise promise) throws Exception {
//		if (msg instanceof Request) {
//			Request request = (Request) msg;
//			responseMap.putIfAbsent(request.getMessageId(),
//					new LinkedBlockingQueue<Response>(1));
//		}
//		super.write(ctx, msg, promise);
//	}

//	@Override
//	protected void messageReceived(ChannelHandlerContext ctx, Response response)
//			throws Exception {
//		BlockingQueue<Response> queue = responseMap
//				.get(response.getMessageId());
//		queue.add(response);
//	}
	
	public void write(Request request){
		responseMap.putIfAbsent(request.getMessageId(),
				new LinkedBlockingQueue<Response>(1));
	}

	public Response getResponse(long messageId) {
		Response result;
		responseMap
				.putIfAbsent(messageId, new LinkedBlockingQueue<Response>(1));
		try {
			result = responseMap.get(messageId).poll(10, TimeUnit.SECONDS);// take();
			if (null == result) {
				result = getSystemMessage();
			}
		} catch (final InterruptedException ex) {
			throw new ClientException(ex);
		} finally {
			responseMap.remove(messageId);
		}
		return result;
	}

	private Response getSystemMessage() {
		try {
			BlockingQueue<Response> errResp = responseMap
					.get(IServer.SYSTEM_MESSAGE_ID);
			if (errResp != null)
				return errResp.poll(5, TimeUnit.SECONDS);
			else
				return new Response(
						IServer.SYSTEM_MESSAGE_ID,
						new RuntimeException(
								"Can not get any respons from server, may be the Server is close!"));
			// return responseMap.get(IServer.SYSTEM_MESSAGE_ID).poll(5,
			// SECONDS);
		} catch (final InterruptedException ex) {
			throw new ClientException(ex);
		}
	}

	
}
