package com.lanshi.microbe.remoting.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.channel.udt.nio.NioUdtProvider;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.exection.ClientCloseException;
import com.lanshi.microbe.exection.ClientException;
import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.ProtocolType;
import com.lanshi.microbe.remoting.netty.codec.SerializeHttpType;
import com.lanshi.microbe.remoting.netty.codec.SerializeUdtType;
import com.lanshi.microbe.remoting.netty.cpu.NameExecutorServiceFactory;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientHttpChannelInitializer;
import com.lanshi.microbe.remoting.netty.initializer.client.NettyClientUdtChannelInitializer;
import com.lanshi.microbe.remoting.server.IServer;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NettyClient implements IClient, ApplicationContextAware {
	private ApplicationContext applicationContext;

	@Value("${server.worker.group.threads}")
	private int workerGroupThreads;
	@Value("${serialize.type}")
	private SerializeUdtType serializeUdtType;
	@Value("${serialize.type}")
	private SerializeHttpType serializeHttpType;
	@Value("${server.protocal}")
	private ProtocolType protocolType;

	// connecter deal thread factory
	private NameExecutorServiceFactory workerFactory;

	private NioEventLoopGroup workerGroup;
	private Channel channel;
	private Class<? extends NettyClientUdtChannelInitializer> clientChannelInitializer;

	@Override
	public void connect(final InetSocketAddress socketAddress) {
		Bootstrap bootstrap = new Bootstrap();
		if (ProtocolType.isUdt(protocolType)) {
			workerFactory = new NameExecutorServiceFactory("worker");
			workerGroup = new NioEventLoopGroup(workerGroupThreads,
					workerFactory, NioUdtProvider.BYTE_PROVIDER);
			bootstrap.group(workerGroup)
					.channelFactory(NioUdtProvider.BYTE_CONNECTOR)
					.handler(getHandler());
			channel = bootstrap
					.connect(socketAddress.getAddress().getHostAddress(),
							socketAddress.getPort()).syncUninterruptibly()
					.channel();
		}
		if (ProtocolType.isHttp(protocolType)) {
			workerGroup = new NioEventLoopGroup(workerGroupThreads);
			bootstrap.group(workerGroup).channel(NioSocketChannel.class)
					.option(ChannelOption.SO_KEEPALIVE, true)
					.option(ChannelOption.TCP_NODELAY, true)
					.handler(getHandler());
			channel = bootstrap
					.connect(socketAddress.getAddress().getHostAddress(),
							socketAddress.getPort()).syncUninterruptibly()
					.channel();
		}
		MicrobeLogger.info("Netty client connect server : {} with potocol {}",
				socketAddress, protocolType.type());
	}

	private ChannelHandler getHandler() {
		if (ProtocolType.isUdt(protocolType)) {
			MicrobeLogger.info("Client start with serialize type:{}",
					serializeUdtType.getClientChannelInitializer());
			return applicationContext.getBean(serializeUdtType
					.getClientChannelInitializer());
		} else if (ProtocolType.isHttp(protocolType)) {
			MicrobeLogger.info("Client start with serialize type:{}",
					serializeHttpType.getClientChannelInitializer());
			return applicationContext.getBean(serializeHttpType
					.getClientChannelInitializer());
		}
		throw new ServerException(IServer.SYSTEM_MESSAGE_ID,
				new RuntimeException(protocolType.type()
						+ " is not excepte! init Bootstrap fail!"));
	}

	@Override
	public Response sendRequest(final Request request) {
		if (ProtocolType.isUdt(protocolType)) {
			NettyClientUdtChannelInitializer clientChannelInitializer = applicationContext
					.getBean(serializeUdtType.getClientChannelInitializer());
			clientChannelInitializer.write(request);
			channel.writeAndFlush(request);
			return clientChannelInitializer.getResponse(request.getMessageId());
		} else if (ProtocolType.isHttp(protocolType)) {
			NettyClientHttpChannelInitializer clientChannelInitializer = applicationContext
					.getBean(serializeHttpType.getClientChannelInitializer());
			clientChannelInitializer.write(request);
			channel.writeAndFlush(request);
			return clientChannelInitializer.getResponse(request.getMessageId());
		}

		throw new ServerException(IServer.SYSTEM_MESSAGE_ID,
				new RuntimeException(protocolType.type()
						+ " is not excepte! Send Request Fail!"));
	}

	@Override
	public void close() {
		if (null == channel) {
			throw new ClientCloseException();
		}
		workerGroup.shutdownGracefully();
		channel.closeFuture().syncUninterruptibly();
		workerGroup = null;
		channel = null;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public InetSocketAddress getRemoteAddress() {
		SocketAddress remoteAddress = channel.remoteAddress();
		if (!(remoteAddress instanceof InetSocketAddress)) {
			throw new ClientException(new RuntimeException(
					"Get remote address error, should be InetSocketAddress"));
		}
		return (InetSocketAddress) remoteAddress;
	}
}
