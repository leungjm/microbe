package com.lanshi.microbe.rmiService.impl;

import org.springframework.stereotype.Service;

import com.lanshi.microbe.rmiService.IHelloWorld;

@Service
public class HelloWorld implements IHelloWorld{


	@Override
	public String hello(String name, Integer age) {
		System.out.println("*********************************");
		System.out.println(name + "  " + age);
		return "Hi, how do you do!";
	}

}
