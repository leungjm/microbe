package com.lanshi.microbe.remoting.netty.server.impl;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.lanshi.microbe.remoting.netty.model.Order;
import com.lanshi.microbe.remoting.netty.server.IUdtService;
import com.lanshi.microbe.remoting.netty.server.UdtSystemException;

@Service
public class UdtService implements IUdtService {

	@Override
	public void testWithExcetion(Order order) {
		System.out.println(order.getOrderId() + " " + order.getName() + " "
				+ order.getMoney());
		throw new UdtSystemException();
	}

	@Override
	public List<Order> queryOrder(String orderId) {
		System.out.println("orderId:" + orderId);
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order("10221", "neworder", 1458, false));
		return orders;
	}

	@Override
	public boolean update(Order order) {
		System.out.println(order.getOrderId() + " " + order.getName() + " "
				+ order.getMoney());
		return true;
	}

	@Override
	public InetSocketAddress sendInetSocketAddress(InetSocketAddress addr) {
		System.out.println(addr.toString());
		return addr;
	}

}
