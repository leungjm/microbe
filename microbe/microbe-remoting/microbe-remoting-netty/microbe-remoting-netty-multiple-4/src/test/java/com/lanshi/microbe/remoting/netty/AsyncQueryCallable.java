package com.lanshi.microbe.remoting.netty;

import java.util.concurrent.Callable;

import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.client.NettyClient;
import com.lanshi.microbe.remoting.netty.server.IUdtService;

public final class AsyncQueryCallable implements Callable<Response> {

	private final NettyClient nettyClient;
	private final String queryString;

	public AsyncQueryCallable(final NettyClient nettyClient,
			final String queryString) {
		this.nettyClient = nettyClient;
		this.queryString = queryString;
	}

	@Override
	public Response call() throws Exception {
		return nettyClient.sendRequest(new Request(IUdtService.class,
				"queryOrder", queryString));
	}
}
