package com.lanshi.microbe.remoting.netty;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.exection.ClientCloseException;
import com.lanshi.microbe.remoting.netty.client.NettyClient;
import com.lanshi.microbe.remoting.netty.server.NettyServer;

@ContextConfiguration(locations = SpringContainerImpl.SPRING_XML_PATH)
public class UdtClientTest extends AbstractJUnit4SpringContextTests {

	private final String ip = "localhost";
	private final int port = 2345;
	private InetAddress address;

	@Resource
	private NettyServer nettyServer;

	@Resource
	private NettyClient nettyClient;

	@Before
	public void initHost() {
		try {
			address = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

	@Test(expected = ClientCloseException.class)
	public void closeClientWithoutConnect() {
		nettyClient.close();
	}

	@Test
	public void closeClient() {
		nettyServer.start(ip, port);
		nettyClient.connect(new InetSocketAddress(ip, port));
		nettyClient.close();
	}
}
