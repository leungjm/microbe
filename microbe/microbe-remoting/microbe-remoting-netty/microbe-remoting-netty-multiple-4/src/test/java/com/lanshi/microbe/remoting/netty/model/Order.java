package com.lanshi.microbe.remoting.netty.model;

import java.io.Serializable;

public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1777460249710800368L;
	private String orderId;
	private String name;
	private int money;
	private boolean useful;

	public Order() {

	}

	public Order(String orderId, String name, int money, boolean useful) {
		this.orderId = orderId;
		this.name = name;
		this.money = money;
		this.useful = useful;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public boolean isUseful() {
		return useful;
	}

	public void setUseful(boolean useful) {
		this.useful = useful;
	}

}
