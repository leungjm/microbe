package com.lanshi.microbe.remoting.netty;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.lanshi.microbe.remoting.netty.asserter.OrderResponseAssert;
import com.lanshi.microbe.remoting.netty.client.NettyClient;
import com.lanshi.microbe.remoting.netty.model.Order;
import com.lanshi.microbe.remoting.netty.server.IUdtService;
import com.lanshi.microbe.remoting.netty.server.NettyServer;

@ContextConfiguration(locations = SpringContainerImpl.SPRING_XML_PATH)
public final class RemoteNettyTest extends AbstractJUnit4SpringContextTests {

	private static volatile boolean serverStarted = false;

	private int port = 6099;
	private String ip = "localhost";

	@Resource
	private NettyServer nettyServer;

	@Resource
	private NettyClient nettyClient;

	@Before
	public void setUp() {
		if (!serverStarted) {
			nettyServer.start(ip, port);
			serverStarted = true;
		}
		nettyClient.connect(new InetSocketAddress(ip, port));
	}

	@Test
	public void cannotFoundMethod() {
		Response response = nettyClient.sendRequest(new Request(
				IUdtService.class, "notExist"));
		OrderResponseAssert.assertCauseException(response,
				ReflectiveOperationException.class);
	}

	@Test
	public void update() {
		long stime = System.currentTimeMillis();
		for (int i = 0; i < 1; i++) {
			Response response = nettyClient.sendRequest(new Request(
					IUdtService.class, "update", new Order("10112", "Order",
							201, true)));
			OrderResponseAssert.assertHasReturnValue(response, Boolean.class);
		}
		System.out.println("useTime:" + (System.currentTimeMillis() - stime));
	}

	// @Test
	public void testInetSocketAddressSerizlize() {// InetSocketAddress部分序列化失败，原因缺少setter方法
		long stime = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			InetSocketAddress addr = null;
			try {
				addr = new InetSocketAddress(InetAddress.getLocalHost(), 9999);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Response response = nettyClient.sendRequest(new Request(
					IUdtService.class, "sendInetSocketAddress", addr));
			System.out.println(response.getReturnValue().toString());
			OrderResponseAssert.assertHasReturnValue(response,
					InetSocketAddress.class);
		}
		System.out.println("useTime:" + (System.currentTimeMillis() - stime));
	}

	@Test
	public void queryForSync() {
		long stime = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			Response response = nettyClient.sendRequest(new Request(
					IUdtService.class, "queryOrder", "bar" + i));
			// OrderResponseAssert.assertHasReturnValue(response, List.class);
		}
		System.out.println("useTime:" + (System.currentTimeMillis() - stime));
	}

	@Test
	public void queryForAsync() throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newFixedThreadPool(30);
		for (int i = 0; i < 10; i++) {
			Response response = executorService.submit(
					new AsyncQueryCallable(nettyClient, "bar" + i)).get();
			OrderResponseAssert.assertHasReturnValue(response, List.class);
		}
	}

	@Test
	public void queryWithSystemException() {
		Response response = nettyClient.sendRequest(new Request(
				IUdtService.class, "testWithExcetion"));
		OrderResponseAssert.assertException(response, ServerException.class);
	}

	@Test
	public void queryMix() throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		Response normalQueryResponse = executorService.submit(
				new AsyncQueryCallable(nettyClient, "normal")).get();
		Response slowQueryResponse = executorService.submit(
				new AsyncQueryCallable(nettyClient, "slow")).get();
		OrderResponseAssert.assertHasReturnValue(slowQueryResponse, List.class);
		OrderResponseAssert.assertHasReturnValue(normalQueryResponse,
				List.class);
		OrderResponseAssert.assertHasReturnValue(normalQueryResponse,
				"neworder");
	}
}
