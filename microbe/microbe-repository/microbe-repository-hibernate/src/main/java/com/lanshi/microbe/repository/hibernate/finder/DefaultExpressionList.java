package com.lanshi.microbe.repository.hibernate.finder;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

public class DefaultExpressionList implements ExpressionList {
	private Session session;
	private Class<?> clazz;
	private final Criteria crit;
	private DetachedCriteria dcrit;
	private MyJunction junction;

	public DefaultExpressionList(final Criteria criteria) {
		this.crit = criteria;
	}

	@Override
	public MyJunction disJunction() {
		if (junction == null) {
			junction = new DefaultJunction(this, true);
		}
		return junction;
	}

	@Override
	public MyJunction conJunction() {
		if (junction == null) {
			junction = new DefaultJunction(this, false);
		}
		return junction;
	}

	@Override
	public ExpressionList endJunction() {
		return this;
	}

	@Override
	public ExpressionList eq(String propertyName, Object value) {
		System.out.println("DefaultExpressionList ..... ");
		// crit.add(Restrictions.eq(propertyName, value));
		return this;
	}

	@Override
	public ExpressionList between(String propertyName, Object lo, Object hi) {
		crit.add(Restrictions.between(propertyName, lo, hi));
		return this;
	}

	@Override
	public ExpressionList eqOrIsNull(String propertyName, Object value) {
		crit.add(Restrictions.eqOrIsNull(propertyName, value));
		return this;
	}

	@Override
	public ExpressionList in(String propertyName, Object[] values) {
		crit.add(Restrictions.in(propertyName, values));
		return this;
	}

	@Override
	public ExpressionList in(String propertyName, Collection values) {
		crit.add(Restrictions.in(propertyName, values));
		return this;
	}

	@Override
	public ExpressionList isNotNull(String propertyName) {
		crit.add(Restrictions.isNotNull(propertyName));
		return this;
	}

	@Override
	public ExpressionList isNull(String propertyName) {
		crit.add(Restrictions.isNull(propertyName));
		return this;
	}

	@Override
	public ExpressionList neOrIsNotNull(String propertyName, Object value) {
		crit.add(Restrictions.neOrIsNotNull(propertyName, value));
		return this;
	}

}
