package com.lanshi.microbe.repository.hibernate.finder;

import java.util.Collection;

public interface ExpressionList {

	public MyJunction disJunction();

	public MyJunction conJunction();

	public ExpressionList endJunction();

	public ExpressionList eq(String propertyName, Object value);

	public ExpressionList between(String propertyName, Object lo, Object hi);

	public ExpressionList eqOrIsNull(String propertyName, Object value);

	public ExpressionList in(String propertyName, Object[] values);

	public ExpressionList in(String propertyName,
			@SuppressWarnings("rawtypes") Collection values);

	public ExpressionList isNotNull(String propertyName);

	public ExpressionList isNull(String propertyName);

	public ExpressionList neOrIsNotNull(String propertyName, Object value);
}
