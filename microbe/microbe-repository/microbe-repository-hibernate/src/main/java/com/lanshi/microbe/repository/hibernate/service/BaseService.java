package com.lanshi.microbe.repository.hibernate.service;

import javax.annotation.PostConstruct;

import com.lanshi.microbe.Logger.MicrobeLogger;

public abstract class BaseService {

	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info(getClass() + " is inited!");
	}
}
