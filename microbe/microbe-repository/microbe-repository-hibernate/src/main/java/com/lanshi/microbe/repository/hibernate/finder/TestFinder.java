package com.lanshi.microbe.repository.hibernate.finder;

import org.hibernate.Criteria;
import org.hibernate.internal.CriteriaImpl;

public class TestFinder {

	public static void main(String[] args) {
		Criteria crit = new CriteriaImpl(null, null);
		ExpressionList exp = new DefaultExpressionList(crit);
		System.out.println(exp);
		exp.disJunction().eq("", "");
		System.out.println(exp);
		exp.endJunction();
		System.out.println(exp);
	}
}
