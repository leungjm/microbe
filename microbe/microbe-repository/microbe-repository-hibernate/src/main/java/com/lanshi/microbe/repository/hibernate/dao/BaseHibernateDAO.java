package com.lanshi.microbe.repository.hibernate.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.repository.hibernate.db.BaseModel;

/**
 * Data access object (DAO) for domain model
 * 
 * @author MyEclipse Persistence Tools
 */

public abstract class BaseHibernateDAO<T extends BaseModel<T>, I extends Serializable>
		implements ApplicationContextAware {

	protected PageCounter pageCounter = null;

	protected String classType;

	protected Class<T> clazz;

	protected String hql_property;

	protected String hql_count;

	protected String hql_select;

	private static DataSource ds = null;

	public static final String ID = "id";
	public static final String CREATE_TIME = "createTime";
	public static final String UPDATE_TIME = "updateTime";

	@Autowired
	protected SessionFactory sessionFactory;

	protected static ApplicationContext applicationContext;

	protected Criteria getCriteria() {
		Criteria crit = getSession().createCriteria(clazz);
		return crit;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		BaseHibernateDAO.applicationContext = applicationContext;
	}

	@SuppressWarnings("unchecked")
	public BaseHibernateDAO() {
		this.clazz = ((Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0]);
		this.classType = this.clazz.getSimpleName();
		this.hql_property = "from " + classType + " as model where model.";
		this.hql_count = "select count(*) from " + classType;
		this.hql_select = "from " + classType;
	}

	@SuppressWarnings("unchecked")
	public T findById(I id) {
		try {
			T instance = (T) getSession().get(this.clazz, id);
			return instance;
		} catch (RuntimeException re) {
			MicrobeLogger.error("get failed", re);
			throw re;
		}
	}

	public boolean save(T instance) {
		try {
			if (instance.getCreateTime() == null) {
				instance.setCreateTime(new Date());
			}
			instance.setUpdateTime(new Date());
			getSession().save(instance);
			return true;
		} catch (DataAccessException re) {
			MicrobeLogger.error("save failed", re);
			throw re;
		}
	}

	public boolean update(T instance) {
		try {
			instance.setUpdateTime(new Date());
			getSession().update(instance);
			return true;
		} catch (DataAccessException re) {
			MicrobeLogger.error("update failed", re);
			throw re;
		}
	}

	public boolean delete(T instance) {
		try {
			getSession().delete(instance);
			return true;
		} catch (DataAccessException re) {
			MicrobeLogger.error("delete failed", re);
			throw re;
		}
	}

	public T merge(T instance) {
		try {
			instance.setUpdateTime(new Date());
			T result = (T) getSession().merge(instance);
			return result;
		} catch (DataAccessException re) {
			MicrobeLogger.error("merge failed", re);
			throw re;
		}
	}

	public boolean attachDirty(T instance) {
		try {
			if (instance.getCreateTime() == null) {
				instance.setCreateTime(new Date());
			}
			instance.setUpdateTime(new Date());
			getSession().saveOrUpdate(instance);
			return true;
		} catch (DataAccessException re) {
			MicrobeLogger.error("attachDirty failed", re);
			throw re;
		}
	}

	public boolean attachClean(T instance) {
		try {
			getSession().lock(instance, LockMode.NONE);
			return true;
		} catch (DataAccessException re) {
			MicrobeLogger.error("attachClean failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(String propertyName, Object value) {
		try {
			String queryString = hql_property + propertyName + "= ?";
			// List<T> res = (List<T>) getCacheHibernateTemplate().find(
			// queryString, value);
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			// prepareQuery(queryObject);
			List<T> res = queryObject.list();
			if (res != null)
				Collections.sort(res);

			return res;
		} catch (DataAccessException re) {
			MicrobeLogger.error("find by property failed: " + propertyName
					+ "=" + value);
			throw re;
		}
	}

	// protected void prepareQuery(Query queryObject) {
	// if (isCacheQueries()) {
	// queryObject.setCacheable(true);
	// if (getQueryCacheRegion() != null) {
	// queryObject.setCacheRegion(getQueryCacheRegion());
	// }
	// }
	// if (getFetchSize() > 0) {
	// queryObject.setFetchSize(getFetchSize());
	// }
	// if (getMaxResults() > 0) {
	// queryObject.setMaxResults(getMaxResults());
	// }
	//
	// SessionHolder sessionHolder = (SessionHolder)
	// TransactionSynchronizationManager
	// .getResource(sessionFactory);
	// if (sessionHolder != null && sessionHolder.hasTimeout()) {
	// queryObject.setTimeout(sessionHolder.getTimeToLiveInSeconds());
	// }
	// }

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		try {
			List<T> res = (List<T>) getSession().createQuery(hql_select).list();

			if (res != null)
				Collections.sort(res);

			return res;
		} catch (DataAccessException re) {
			MicrobeLogger.error("find all failed", re);
			throw re;
		}
	}

	public Integer count() {
		Query query = getSession().createQuery(hql_count);
		return ((Long) query.uniqueResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<T> findByPage(int pageNum) {
		if (null == this.pageCounter) {
			this.pageCounter = new PageCounter(this.count());
		} else {
			this.pageCounter.refresh(this.count());
		}
		this.pageCounter.pageTo(pageNum);
		Query query = getSession().createQuery(hql_select);
		query.setFirstResult(this.pageCounter.getCurrentItem());
		query.setMaxResults(this.pageCounter.getPageSize());

		return query.list();
	}

	// @Autowired
	// public void setSessionFactoryOverride(SessionFactory sessionFactory) {
	//
	// super.setSessionFactory(sessionFactory);
	// }

	// protected HibernateTemplate getCacheHibernateTemplate() {
	// this.setCacheQueries(true);
	// return this;
	// }
	//
	// protected HibernateTemplate getNonCacheHibernateTemplate() {
	// this.setCacheQueries(false);
	// return this;
	// }

	public final Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	protected static Connection getConnection() {
		if (ds == null)
			ds = (DataSource) applicationContext.getBean("dataSource");

		return ds == null ? null : DataSourceUtils.getConnection(ds);
	}

	protected static void releaseConnection(Connection conn) {
		if (conn != null && ds != null) {
			DataSourceUtils.releaseConnection(conn, ds);
		}
	}

}