package com.lanshi.microbe.repository.hibernate.dao;

import java.util.List;

public class ListPageCounter<T> extends PageCounter {

	protected List<T> response;

	public ListPageCounter(int totalItem) {
		super(totalItem);
	}

	public ListPageCounter(int totalItem, int pageSize) {
		super(totalItem, pageSize);
	}

	public ListPageCounter(int totalItem, int pageNum, int pageSize) {
		super(totalItem, pageNum, pageSize);
	}

	public ListPageCounter(int totalItem, int pageNum, int pageSize,
			List<T> list) {
		super(totalItem, pageNum, pageSize);
		this.response = list;
	}
}
