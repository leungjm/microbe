package com.lanshi.microbe.repository.hibernate.finder;

import java.util.Collection;

import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

public class DefaultJunction implements MyJunction {
	private final ExpressionList expressionList;
	private final Conjunction conjunction = Restrictions.conjunction();
	private final Disjunction disJunction = Restrictions.disjunction();

	private final Boolean disjunction;

	public DefaultJunction(final ExpressionList expressionList,
			final Boolean disjunction) {
		System.out.println("new default junction.....");
		this.expressionList = expressionList;
		this.disjunction = disjunction;
	}

	@Override
	public MyJunction disJunction() {
		return expressionList.disJunction();
	}

	@Override
	public MyJunction conJunction() {
		return expressionList.conJunction();
	}

	@Override
	public ExpressionList endJunction() {
		return expressionList.endJunction();
	}

	@Override
	public ExpressionList eq(String propertyName, Object value) {
		// TODO Auto-generated method stub
		System.out.println("defaultJunction.............");
		return null;
	}

	@Override
	public ExpressionList between(String propertyName, Object lo, Object hi) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExpressionList eqOrIsNull(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExpressionList in(String propertyName, Object[] values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExpressionList in(String propertyName, Collection values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExpressionList isNotNull(String propertyName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExpressionList isNull(String propertyName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExpressionList neOrIsNotNull(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

}
