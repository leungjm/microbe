package com.lanshi.microbe.repository.hibernate.finder;

import org.hibernate.Criteria;
import org.hibernate.Session;

public class Finder {

	private Session session;
	private final Criteria crit;
	private final ExpressionList expressionList;
	private Class<?> clazz;

	public Finder(Criteria criteria) {
		this.crit = criteria;
		this.expressionList = new DefaultExpressionList(crit);
	}

	public Criteria getCriteria() {
		return crit;
	}

	public ExpressionList where() {

		return expressionList;
	}

}
