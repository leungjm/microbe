package com.lanshi.microbe.exection;

public final class ServerException extends SystemException {

	private static final long serialVersionUID = 5438288073708201395L;

	private long messageId;

	public ServerException(final long messageId, final Exception cause) {
		super(cause);
		this.messageId = messageId;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

}
