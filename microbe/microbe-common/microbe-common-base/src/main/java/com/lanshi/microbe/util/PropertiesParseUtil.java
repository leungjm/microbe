package com.lanshi.microbe.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.lanshi.microbe.Logger.MicrobeLogger;

public class PropertiesParseUtil {
	private static Properties properties = new Properties();
	private static PropertiesParseUtil instance = null;

	private PropertiesParseUtil() {

	}

	/**
	 * return instance
	 * 
	 * @return
	 */
	public static PropertiesParseUtil getInstance() {
		if (instance == null) {
			instance = new PropertiesParseUtil();
		}
		return instance;
	}

	/**
	 * load config file Example: loadPropertyFile("server.properties");
	 * 
	 * @param file
	 *            class path
	 */
	public void loadPropertyFile(String file) {
		if (file == null || file.trim().equals("")) {
			throw new IllegalArgumentException(
					"Parameter of file can not be blank");
		}
		if (file.contains("..")) {
			throw new IllegalArgumentException(
					"Parameter of file can not contains \"..\"");
		}
		InputStream inputStream = null;
		try {
			inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(file);
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Properties file not found: "
					+ file);
		} catch (IOException e) {
			throw new IllegalArgumentException(
					"Properties file can not be loading: " + file);
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (IOException e) {
				MicrobeLogger.error(e.getMessage(), e);
			}
		}
		if (properties == null)
			throw new RuntimeException("Properties file loading failed: "
					+ file);
	}

	public String getProperty(String key) {
		return properties.getProperty(key).trim();
	}

	public String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue.trim());
	}

	public Properties getProperties() {
		return properties;
	}

	/**
	 * get the server's HTTP port,default is -1
	 * 
	 * @return
	 */
	public int getHttpPort() {
		String port = properties.getProperty("server.http.port", "-1");
		return Integer.parseInt(port);
	}

}
