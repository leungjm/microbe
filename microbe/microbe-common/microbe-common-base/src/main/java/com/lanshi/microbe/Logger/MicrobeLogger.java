package com.lanshi.microbe.Logger;

import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

public class MicrobeLogger {
//	public static ch.qos.logback.classic.Logger log;

	public static void main(String[] args) {
		test();
	}
	
	public static void test() {
		LoggerFactory.getLogger(getCallerClassName())
				.info("Hello Logging.....");
	}

	public static boolean isInfoEnabled() {
		return LoggerFactory.getLogger(getCallerClassName()).isInfoEnabled();
	}

	public static boolean isInfoEnabled(Marker marker) {
		return LoggerFactory.getLogger(getCallerClassName()).isInfoEnabled(
				marker);
	}

	public static void info(String msg) {
		LoggerFactory.getLogger(getCallerClassName()).info(msg);
	}

	public static void info(String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).info(format, arg);
	}

	public static void info(String format, Object arg1, Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).info(format, arg1, arg2);
	}

	public static void info(String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).info(format, argArray);
	}

	public static void info(String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).info(msg, t);
	}

	public static void info(Marker marker, String msg) {
		LoggerFactory.getLogger(getCallerClassName()).info(marker, msg);
	}

	public static void info(Marker marker, String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).info(marker, format, arg);
	}

	public static void info(Marker marker, String format, Object arg1,
			Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).info(marker, format,
				arg1, arg2);
	}

	public static void info(Marker marker, String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).info(marker, format,
				argArray);
	}

	public static void info(Marker marker, String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).info(marker, msg, t);
	}

	public static boolean isTraceEnabled() {
		return LoggerFactory.getLogger(getCallerClassName()).isTraceEnabled();
	}

	public static boolean isTraceEnabled(Marker marker) {
		return LoggerFactory.getLogger(getCallerClassName()).isTraceEnabled(
				marker);
	}

	public static void trace(String msg) {
		LoggerFactory.getLogger(getCallerClassName()).trace(msg);
	}

	public static void trace(String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).trace(format, arg);
	}

	public static void trace(String format, Object arg1, Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).trace(format, arg1, arg2);
	}

	public static void trace(String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).trace(format, argArray);
	}

	public static void trace(String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).trace(msg, t);
	}

	public static void trace(Marker marker, String msg) {
		LoggerFactory.getLogger(getCallerClassName()).trace(marker, msg);
	}

	public static void trace(Marker marker, String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName())
				.trace(marker, format, arg);
	}

	public static void trace(Marker marker, String format, Object arg1,
			Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).trace(marker, format,
				arg1, arg2);
	}

	public static void trace(Marker marker, String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).trace(marker, format,
				argArray);
	}

	public static void trace(Marker marker, String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).trace(marker, msg, t);
	}

	public static boolean isDebugEnabled() {
		return LoggerFactory.getLogger(getCallerClassName()).isDebugEnabled();
	}

	public static boolean isDebugEnabled(Marker marker) {
		return LoggerFactory.getLogger(getCallerClassName()).isDebugEnabled(
				marker);
	}

	public static void debug(String msg) {
		LoggerFactory.getLogger(getCallerClassName()).debug(msg);
	}

	public static void debug(String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).debug(format, arg);
	}

	public static void debug(String format, Object arg1, Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).debug(format, arg1, arg2);
	}

	public static void debug(String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).debug(format, argArray);
	}

	public static void debug(String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).debug(msg, t);
	}

	public static void debug(Marker marker, String msg) {
		LoggerFactory.getLogger(getCallerClassName()).debug(marker, msg);
	}

	public static void debug(Marker marker, String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName())
				.debug(marker, format, arg);
	}

	public static void debug(Marker marker, String format, Object arg1,
			Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).debug(marker, format,
				arg1, arg2);
	}

	public static void debug(Marker marker, String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).debug(marker, format,
				argArray);
	}

	public static void debug(Marker marker, String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).debug(marker, msg, t);
	}

	public static boolean isErrorEnabled() {
		return LoggerFactory.getLogger(getCallerClassName()).isErrorEnabled();
	}

	public static boolean isErrorEnabled(Marker marker) {
		return LoggerFactory.getLogger(getCallerClassName()).isErrorEnabled(
				marker);
	}

	public static void error(String msg) {
		LoggerFactory.getLogger(getCallerClassName()).error(msg);
	}

	public static void error(String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).error(format, arg);
	}

	public static void error(String format, Object arg1, Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).error(format, arg1, arg2);
	}

	public static void error(String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).error(format, argArray);
	}

	public static void error(String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).error(msg, t);
	}

	public static void error(Marker marker, String msg) {
		LoggerFactory.getLogger(getCallerClassName()).error(marker, msg);
	}

	public static void error(Marker marker, String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName())
				.error(marker, format, arg);
	}

	public static void error(Marker marker, String format, Object arg1,
			Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).error(marker, format,
				arg1, arg2);
	}

	public static void error(Marker marker, String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).error(marker, format,
				argArray);
	}

	public static void error(Marker marker, String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).error(marker, msg, t);
	}

	public static boolean isWarnEnabled() {
		return LoggerFactory.getLogger(getCallerClassName()).isWarnEnabled();
	}

	public static boolean isWarnEnabled(Marker marker) {
		return LoggerFactory.getLogger(getCallerClassName()).isWarnEnabled(
				marker);
	}

	public static void warn(String msg) {
		LoggerFactory.getLogger(getCallerClassName()).warn(msg);
	}

	public static void warn(String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).warn(msg, t);
	}

	public static void warn(String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).warn(format, arg);
	}

	public static void warn(String format, Object arg1, Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).warn(format, arg1, arg2);
	}

	public static void warn(String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).warn(format, argArray);
	}

	public static void warn(Marker marker, String msg) {
		LoggerFactory.getLogger(getCallerClassName()).warn(marker, msg);
	}

	public static void warn(Marker marker, String format, Object arg) {
		LoggerFactory.getLogger(getCallerClassName()).warn(marker, format, arg);
	}

	public static void warn(Marker marker, String format, Object[] argArray) {
		LoggerFactory.getLogger(getCallerClassName()).warn(marker, format,
				argArray);
	}

	public static void warn(Marker marker, String format, Object arg1,
			Object arg2) {
		LoggerFactory.getLogger(getCallerClassName()).warn(marker, format,
				arg1, arg2);
	}

	public static void warn(Marker marker, String msg, Throwable t) {
		LoggerFactory.getLogger(getCallerClassName()).warn(marker, msg, t);
	}

	// ---------------------------------------------------------------------------------

	static String getCallerClassName() {
		final int level = 5;
		return getCallerClassName(level);
	}

	/**
	 * @return the className of the class actually logging the message
	 */
	static String getCallerClassName(final int level) {
		CallInfo ci = getCallerInformations(level);
		return ci.className;
	}

	/**
	 * Examine stack trace to get caller
	 * 
	 * @param level
	 *            method stack depth
	 * @return who called the logger
	 */
	static CallInfo getCallerInformations(int level) {
		StackTraceElement[] callStack = Thread.currentThread().getStackTrace();
		StackTraceElement caller = callStack[level];
		return new CallInfo(caller.getClassName(), caller.getMethodName());
	}

	/**
	 * Info about the logger caller
	 */
	static class CallInfo {

		public String className;
		public String methodName;

		public CallInfo() {
		}

		public CallInfo(String className, String methodName) {
			this.className = className;
			this.methodName = methodName;
		}
	}
}
