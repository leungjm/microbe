package com.lanshi.microbe.serialize.kyro;

import com.lanshi.microbe.exection.SystemException;

public final class KryoPoolException extends SystemException {

	private static final long serialVersionUID = -2992257109597526961L;

	public KryoPoolException(final Exception cause) {
		super(cause);
	}
}
