package com.lanshi.microbe.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;

import com.lanshi.microbe.Microbe;
import com.lanshi.microbe.util.PropertiesConfigUtil;

public class MicrobeServer extends BaseServer {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private ServerBootstrap httpBootstrap = null;
	private ServerBootstrap httpsBootstrap = null;
	private PropertiesConfigUtil config = PropertiesConfigUtil.getInstance();
	private int httpListenPort;
	private int httpsListenPort;
	/** 用于分配处理业务线程的线程组个数 */
	protected static final int BIZGROUPSIZE = Runtime.getRuntime()
			.availableProcessors(); // 默认
	/** 业务出现线程大小 */
	protected static final int BIZTHREADSIZE = 50;

	private static int port = 8448;

	public static EventLoopGroup bossGroup = new NioEventLoopGroup(BIZGROUPSIZE);

	public static EventLoopGroup workerGroup = new NioEventLoopGroup(
			BIZTHREADSIZE);

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	public void startBySpring() {
		startServer();
	}

	private Future<MicrobeServer> startServer() {
		FutureTask<MicrobeServer> future = new FutureTask<MicrobeServer>(
				new Callable<MicrobeServer>() {

					@Override
					public MicrobeServer call() throws Exception {
						synchronized (isRunning) {
							if (isRunning.get()) {
								IllegalStateException is = new IllegalStateException(
										"Server already started.");
								log.fatal(is.getMessage(), is);
								throw is;
							}
							synchronized (Microbe.startup) {
								Microbe.startup.wait();
							}
							init();
							serverLog();
							isRunning.set(true);
							return MicrobeServer.this;

						}
					}

				});
		final Thread thread = new Thread(future,
				"MICROBE-SERVER-ERVER-STARTUP-THREAD");
		thread.start();
		return future;
	}

	private void init() {
		initServerInfo();
		if (httpListenPort != -1) {
			initHttpBootstrap();
		}
		if (httpsListenPort != -1) {
			initHttpsBootstrap();
		}
	}

	private void initHttpBootstrap() {
		// TODO Auto-generated method stub
		log.info("init HTTP Bootstrap...........");
		// ThreadFactory serverBossTF = new NamedThreadFactory("HETTY-BOSS-");
		// ThreadFactory serverWorkerTF = new
		// NamedThreadFactory("HETTY-WORKER-");

		try {
			// int coreSize = config.getServerCorePoolSize();
			// int maxSize = config.getServerMaximumPoolSize();
			// int keepAlive = config.getServerKeepAliveTime();
			// ThreadFactory threadFactory = new NamedThreadFactory("hetty-");
			httpBootstrap = new ServerBootstrap();
			httpBootstrap.group(bossGroup, workerGroup);
			httpBootstrap.channel(NioServerSocketChannel.class);
			httpBootstrap.handler(new LoggingHandler(LogLevel.INFO));
			httpBootstrap.childHandler(new MicrobeServerInitializer());

			if (!checkPortConfig(httpListenPort)) {
				throw new IllegalStateException("port: " + httpListenPort
						+ " already in use!");
			}
			httpBootstrap.bind(new InetSocketAddress(httpListenPort)).sync()
					.channel().closeFuture().sync().addListener(cleaner);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
	}

	private final ChannelFutureListener cleaner = new ChannelFutureListener() {
		@Override
		public void operationComplete(ChannelFuture future) throws Exception {
			// shutdown();
			log.info("netty operation complete ... !");
		}

	};

	private void initHttpsBootstrap() {
		// TODO Auto-generated method stub

	}

	private boolean checkPortConfig(int listenPort) {
		if (listenPort < 0 || listenPort > 65536) {
			throw new IllegalArgumentException("Invalid start port: "
					+ listenPort);
		}
		ServerSocket ss = null;
		DatagramSocket ds = null;
		try {
			ss = new ServerSocket(listenPort);
			ss.setReuseAddress(true);
			ds = new DatagramSocket(listenPort);
			ds.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (ds != null) {
				ds.close();
			}
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException e) {
					// should not be thrown, just detect port available.
				}
			}
		}
		return false;
	}

	private void initServerInfo() {
		httpListenPort = config.getHttpPort();
		httpsListenPort = config.getHttpsPort();

	}

	protected void serverLog() {
		// TODO Auto-generated method stub

	}

}
