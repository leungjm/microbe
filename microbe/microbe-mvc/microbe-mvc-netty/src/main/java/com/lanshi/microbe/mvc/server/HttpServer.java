package com.lanshi.microbe.mvc.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.concurrent.Executors;

public class HttpServer {

	private final int port;
	private int bossLoop = 1000;
	private int workerLoop = 1000;

	public HttpServer(int port) {
		this.port = port;
	}

	public static void main(String[] args) {
		new HttpServer(8080).run();
	}

	private void run() {
		NioEventLoopGroup bossGroup = new NioEventLoopGroup(bossLoop,
				Executors.newCachedThreadPool());
		NioEventLoopGroup workGroup = new NioEventLoopGroup(workerLoop,
				Executors.newCachedThreadPool());
		try {
			ServerBootstrap bootstrap = new ServerBootstrap();
			bootstrap.group(bossGroup, workGroup);
			bootstrap.channel(NioServerSocketChannel.class);
			bootstrap.childHandler(new HttpServerInitializer());

			Channel channel = bootstrap.bind(port).sync().channel();
			System.out.println("start server ....");
			channel.closeFuture().sync();

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			workGroup.shutdownGracefully();
			workGroup.shutdownGracefully();
		}

	}
}