package com.lanshi.microbe.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

public class MicrobeServerInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		// create a default pipeline implementation
		ChannelPipeline pipeline = ch.pipeline();
		/**
		 * uncomment the following line if you want HTTPS SSLEngine engine =
		 * SecureChatSslContextFactory.getServerContext().createSSLEngine();
		 * engine.setUseClientMode(false); pipeline.addLast("ssl",new
		 * SslHandler(engine));
		 */
		pipeline.addLast("decoder", new HttpRequestDecoder());
		pipeline.addLast("aggregator", new HttpObjectAggregator(1024));
		pipeline.addLast("encoder", new HttpResponseEncoder());

		/**
		 * Remove the following line if you don't want automatic content
		 * compression. pipeline.addLast("deflater", new
		 * HttpContentCompressor());
		 */
		pipeline.addLast("handler", new MicrobeServerHandler());

	}
}