package com.lanshi.microbe.server;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;

public abstract class BaseServer {

	protected final Logger log = Logger.getLogger(getClass());

	@PostConstruct
	public void postConstruct() {
		log.info(getClass() + " is inited!");
	}

	public abstract void start();
}
