package com.lanshi.microbe;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lanshi.microbe.util.PropertiesConfigUtil;

public class Microbe {
	private static Logger log = Logger.getLogger(Microbe.class);

	private static AtomicBoolean isStarted = new AtomicBoolean(false);

	static PropertiesConfigUtil configs = PropertiesConfigUtil.getInstance();

	static ClassPathXmlApplicationContext context;

	public static final Object startup = new Object();

	public synchronized void start() {
		if (isStarted.get()) {
			stop();
		}
		readConfiguration();// ��ʼ�����ļ�
		loadSpringContent();// װ��spring

	}

	private static void loadSpringContent() {
		context = new ClassPathXmlApplicationContext(configs.getProperty(
				"spring.context.file").split("[,\\s]+"));
		// context.start();
		((AbstractApplicationContext) context).registerShutdownHook();

		notifyAll2WakeUp();

	}

	private static void notifyAll2WakeUp() {
		synchronized (startup) {
			startup.notifyAll();
		}
	}

	public final static Object getBean(String name) {
		return context.getBean(name);
	}

	public final static <T> T getBean(String name, Class<T> clazz) {
		return context.getBean(name, clazz);
	}

	private static void readConfiguration() {
		configs.loadPropertyFile("microbe.properties");
		String test = configs.getProperty("properties.extends");
		System.out.println(test);
	}

	private static void stop() {
		try {
			if (context != null) {
				context.stop();
				context.close();
				context = null;
			}
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
		}

	}

	public static void main(String[] args) {
		new Microbe().start();
	}
}
