package com.lanshi.microbe.mvc.server;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderUtil;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.multipart.MemoryAttribute;
import io.netty.util.CharsetUtil;

import java.util.List;

public class HujiaWeixinServerHandler extends ChannelHandlerAdapter {

	private final StringBuilder buf = new StringBuilder();

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {

		if (msg instanceof HttpRequest) {
			HttpRequest req = (HttpRequest) msg;

			if (HttpHeaderUtil.is100ContinueExpected(req)) {
				ctx.write(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE));
			}
			boolean keepAlive = HttpHeaderUtil.isKeepAlive(req);

			String uri = req.uri();

			if (uri.equals("/favicon.ico")) {
				return;
			}
			System.out.println(uri);

			if (req.method().equals(HttpMethod.POST)) {

				HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(
						new DefaultHttpDataFactory(false), req);
				try {
					List<InterfaceHttpData> postList = decoder
							.getBodyHttpDatas();
					// 读取从客户端传过来的参数
					for (InterfaceHttpData data : postList) {
						String name = data.getName();
						System.out.println(data.toString());
						String value = null;
						if (InterfaceHttpData.HttpDataType.Attribute == data
								.getHttpDataType()) {
							MemoryAttribute attribute = (MemoryAttribute) data;
							attribute.setCharset(CharsetUtil.UTF_8);
							value = attribute.getValue();
							System.out.println("name:" + name + ",value:"
									+ value);
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1,
					OK, Unpooled.wrappedBuffer("Hello ...".getBytes()));
			response.headers().setInt(CONTENT_LENGTH,
					response.content().readableBytes());

			if (!keepAlive) {
				ctx.write(response).addListener(ChannelFutureListener.CLOSE);
			} else {
				response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE);
				ctx.write(response);
			}
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
		System.out.println("ctx close!");
	}

}