package com.lanshi.microbe.mvc.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.LastHttpContent;

import java.net.SocketAddress;

public class HttpServerHandler extends ChannelHandlerAdapter {

	private HttpRequest request;
	private ByteBuf byteBuf = null;

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		cause.printStackTrace();
		ctx.close();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		if (msg instanceof HttpRequest) {
			HttpRequest request = this.request = (HttpRequest) msg;
			System.out.println(request.headers());
			byteBuf = Unpooled.buffer();
		}

		if (msg instanceof HttpContent) {
			HttpContent httpContent = (HttpContent) msg;
			ByteBuf content = httpContent.content();
			byteBuf = Unpooled.copiedBuffer(byteBuf, content);

			if (msg instanceof LastHttpContent) {
				System.out.println("END OF CONTENT\r\n");
				String uri = request.uri();
				

				if (uri.endsWith("/")) {
					uri = uri.substring(0, uri.length() - 1);
				}

				String serviceName = uri.substring(uri.lastIndexOf("/") + 1);
				// client ip
				SocketAddress remoteAddress = ctx.channel().remoteAddress();
				String ipAddress = remoteAddress.toString().split(":")[0];
				request.headers().add("Client-IP", ipAddress.substring(1));
				handleService(serviceName, request, ctx, byteBuf);
				// byteBuf.release();
			}
		}
	}

	private void handleService(String serviceName, HttpRequest request2,
			ChannelHandlerContext ctx, ByteBuf byteBuf2) {
		// TODO Auto-generated method stub
		
	}

}