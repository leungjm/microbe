package com.lanshi.microbe.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesConfigUtil {
	private static Logger log = Logger.getLogger(PropertiesConfigUtil.class);

	private static Properties properties = new Properties();

	private static PropertiesConfigUtil instance = null;

	private PropertiesConfigUtil() {
		log.info("Create new properties config util instance.");
	}

	public static PropertiesConfigUtil getInstance() {
		if (instance == null)
			instance = new PropertiesConfigUtil();
		return instance;
	}

	public void loadPropertyFile(String file) {
		if (StringUtil.isEmptyStr(file)) {
			throw new IllegalArgumentException(
					"Parameter of file can not be blank");
		}
		if (file.contains("..")) {
			throw new IllegalArgumentException(
					"Parameter of file can not contains \"..\"");
		}
		InputStream inputStream = null;
		try {
			inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(file);
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Properties file not found: "
					+ file);
		} catch (IOException e) {
			throw new IllegalArgumentException(
					"Properties file can not be loading: " + file);
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
		if (properties == null)
			throw new RuntimeException("Properties file loading failed: "
					+ file);
	}

	public String getProperty(String key) {
		return properties.getProperty(key).trim();
	}

	public String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue.trim());
	}

	public Properties getProperties() {
		return properties;
	}

	public int getHttpPort() {
		String port = properties.getProperty("server.http.port", "-1");
		return Integer.parseInt(port);
	}

	public int getHttpsPort() {
		String port = properties.getProperty("server.https.port", "-1");
		return Integer.parseInt(port);
	}

	/**
	 * get the core number of threads
	 * 
	 * @return
	 */
	public int getServerCorePoolSize() {
		String coreSize = properties.getProperty("server.thread.corePoolSize",
				"4");
		return Integer.parseInt(coreSize);
	}

	/**
	 * get the maximum allowed number of threads
	 * 
	 * @return
	 */
	public int getServerMaximumPoolSize() {
		String maxSize = properties.getProperty("server.thread.maxPoolSize",
				"16");
		return Integer.parseInt(maxSize);
	}

	/**
	 * get the thread keep-alive time, which is the amount of time that threads
	 * in excess of the core pool size may remain idle before being terminated
	 * 
	 * @return
	 */
	public int getServerKeepAliveTime() {
		String aleveTime = properties.getProperty(
				"server.thread.keepAliveTime", "3000");
		return Integer.parseInt(aleveTime);
	}

}
