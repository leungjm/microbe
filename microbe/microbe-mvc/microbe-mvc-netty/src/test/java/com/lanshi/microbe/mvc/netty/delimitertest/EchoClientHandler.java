package com.lanshi.microbe.mvc.netty.delimitertest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

public class EchoClientHandler extends ChannelHandlerAdapter {

	private final String smsg = "send msg to server ^.^ $_";

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		ByteBuf b = null;
		for (int i = 0; i < 10; i++) {
			System.out.println("....");
			b = Unpooled.copiedBuffer(smsg.getBytes());
			ctx.writeAndFlush(b);
		}

	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		String body = (String) msg;
		System.out.println(body);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		ctx.flush();
	}

}
