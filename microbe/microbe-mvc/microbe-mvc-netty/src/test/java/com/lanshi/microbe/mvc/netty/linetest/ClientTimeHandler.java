package com.lanshi.microbe.mvc.netty.linetest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

public class ClientTimeHandler extends ChannelHandlerAdapter {

	private byte[] req;
	private Integer counter = 1;

	public ClientTimeHandler() {
		req = ("QUERY TIME ORDER" + System.getProperty("line.separator"))
				.getBytes();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		System.out.println("cause some exception:" + cause.getMessage());
		ctx.close();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {

		ByteBuf firstMsg = null;
		for (int i = 0; i < 100; i++) {
			System.out.println("********************");
			firstMsg = Unpooled.buffer(req.length);
			firstMsg.writeBytes(req);
			ctx.writeAndFlush(firstMsg);
		}
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {

		String body = (String) msg;
		System.out.println("rec server msg:" + body + ", counter:" + counter++);
	}

}
