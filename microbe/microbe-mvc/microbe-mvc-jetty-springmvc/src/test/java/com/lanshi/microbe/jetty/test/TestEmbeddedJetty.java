package com.lanshi.microbe.jetty.test;
import com.lanshi.microbe.jetty.EmbeddedJetty;


public class TestEmbeddedJetty {

	public static void main(String[] args) throws Exception {
		new EmbeddedJetty().startJetty(args);
	}

}
