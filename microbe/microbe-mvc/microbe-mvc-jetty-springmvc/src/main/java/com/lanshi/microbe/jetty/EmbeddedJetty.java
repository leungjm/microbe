package com.lanshi.microbe.jetty;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.lanshi.microbe.util.PropertiesParseUtil;

public class EmbeddedJetty {

	private static final Logger logger = LoggerFactory
			.getLogger(EmbeddedJetty.class);
	private static final int DEFAULT_PORT = 8080;
	private static final String CONTEXT_PATH = "/";
	private static final String CONFIG_LOCATION = "com.lanshi.jetty.config";
	private static final String XML_CONFIG_LOCATION = "classpath:spring/root/applicationContext.xml";
	private static final String MAPPING_URL = "/*";
	private static final String DEFAULT_PROFILE = "dev";
	private static final String DEFAULT_JETTY_CONFIG_PROFILE = "prop/jetty.properties";

	private static String xmlConfigLocation;
	private static String webappPath;
	private static String contextPath;
	private static String resourceBasePath;
	private static String mappingUrl;
	private static int port;
	private static int maxThreads;
	private static int minThreads;
	private static int idleTimeout;
	private static int acceptors;
	private static int selectors;
	private static int outputBufferSize;
	private static int headerCacheSize;
	private static int responseHeaderSize;

	private BlockingQueue<Runnable> queue;
	private ConnectionFactory connectionFactory;

//	public static void main(String[] args) throws Exception {
//		new EmbeddedJetty().startJetty(args);
//	}

	private static void initProperties(String[] args) {
		String jettyProp = null;
		if (args.length > 0) {
			jettyProp = args[0];
			logger.info("load jetty http properties with {}", args[0]);
		} else {
			jettyProp = DEFAULT_JETTY_CONFIG_PROFILE;
			logger.info("load jetty http properties with {}", DEFAULT_JETTY_CONFIG_PROFILE);
		}
		PropertiesParseUtil prop = PropertiesParseUtil.getInstance();
		prop.loadPropertyFile(jettyProp);
		port = Integer.valueOf(prop.getProperty("jetty.port","8080"));
		maxThreads = Integer.valueOf(prop
				.getProperty("jetty.queued.max.threads", "100"));
		minThreads = Integer.valueOf(prop
				.getProperty("jetty.queued.min.threads", "10"));
		idleTimeout = Integer.valueOf(prop
				.getProperty("jetty.queued.idle.timeout", "3000"));
		acceptors = Integer.valueOf(prop
				.getProperty("jetty.server.connector.acceptors.num", "4"));
		selectors = Integer.valueOf(prop
				.getProperty("jetty.server.connector.selectors.num", "4"));
		outputBufferSize = Integer.valueOf(prop
				.getProperty("jetty.connection.outputBufferSize", "32768"));
		headerCacheSize = Integer.valueOf(prop
				.getProperty("jetty.connection.headerCacheSize", "8192"));
		responseHeaderSize = Integer.valueOf(prop
				.getProperty("jetty.connection.responseHeaderSize", "8192"));
		resourceBasePath = prop.getProperty("jetty.resource.base.path", "webapp");
		contextPath = prop.getProperty("jetty.context.path", CONTEXT_PATH);
		webappPath = prop.getProperty("jetty.webapp.path", "webapp");
		mappingUrl = prop.getProperty("jetty.mapping.url", MAPPING_URL);
		xmlConfigLocation = prop.getProperty("jetty.spring.xml.config.location", XML_CONFIG_LOCATION);
		logger.info(
				"port:{}, maxThreads:{}, minThreads:{}, idleTimeout:{}, acceptors:{}, selectors:{}, outputBufferSize:{}, headerCacheSize:{}, responseHeaderSize:{}, resourceBasePath:{}, contextPath:{}, webappPath:{}, mappingUrl:{}",
				port, maxThreads, idleTimeout, acceptors, selectors,
				outputBufferSize, headerCacheSize, responseHeaderSize,
				resourceBasePath, contextPath, webappPath, mappingUrl);
	}

	private static int getPortFromArgs(String[] args) {
		if (args.length > 0) {
			try {
				return Integer.valueOf(args[0]);
			} catch (NumberFormatException ignore) {
			}
		}
		logger.debug("No server port configured, falling back to {}",
				DEFAULT_PORT);
		return DEFAULT_PORT;
	}
	
	public void startJetty2(String[] args) throws Exception{
		initProperties(args);
		Server server = new Server(port);
        server.setHandler(getServletContextHandler(getXmlContext()));
        server.start();
        logger.info("Server started at port {}", port);
        server.join();
	}

	public void startJetty(String[] args) throws Exception {
		initProperties(args);
		logger.debug("Starting server at port {}", port);
		ThreadPool pool = new QueuedThreadPool(maxThreads, minThreads,
				idleTimeout, queue);
		Server server = new Server(pool);
		// 设置在JVM退出时关闭Jetty的钩子。
		server.setStopAtShutdown(true);
		// Http配置
		HttpConfiguration config = new HttpConfiguration();
		config.setOutputBufferSize(outputBufferSize);
		config.setHeaderCacheSize(headerCacheSize);
		config.setResponseHeaderSize(responseHeaderSize);
		connectionFactory = new HttpConnectionFactory(config);
		// http的连接器
		ServerConnector connector = new ServerConnector(server, acceptors,
				selectors, connectionFactory);
		// 设置端口
		connector.setPort(port);
		// 解决Windows下重复启动Jetty居然不报告端口冲突的问题.
		connector.setReuseAddress(false);
		server.setConnectors(new Connector[] { connector });
		server.setHandler(getServletContextHandler(getXmlContext()));
		server.start();
		logger.info("Server started at port {}", port);
		server.join();
	}

	private static ServletContextHandler getServletContextHandler(
			WebApplicationContext context) throws IOException {
		ServletContextHandler contextHandler = new ServletContextHandler();
		contextHandler.setErrorHandler(null);
		contextHandler.setContextPath(contextPath);
		contextHandler.addServlet(new ServletHolder(new DispatcherServlet(
				context)), mappingUrl);
		contextHandler.addEventListener(new ContextLoaderListener(context));
		contextHandler.setResourceBase(new ClassPathResource(resourceBasePath).getURI()
				.toString());
		return contextHandler;
	}

	/**
	 * XML配置
	 * 
	 * @return
	 */
	private static WebApplicationContext getXmlContext() {
		XmlWebApplicationContext context = new XmlWebApplicationContext();
		context.setConfigLocations(xmlConfigLocation);
//		context.registerShutdownHook();
		return context;
	}

	/**
	 * 注解方式配置
	 * 
	 * @return
	 */
	private static WebApplicationContext getContext() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setConfigLocation(CONFIG_LOCATION);
		context.getEnvironment().setDefaultProfiles(DEFAULT_PROFILE);
		return context;
	}

}
