package com.lanshi.microbe.web;

import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.jetty.JettyBootstrap;
import com.lanshi.microbe.services.IHelloWorldService;

@Controller
public class MainController {
	

	@PostConstruct
	public void postConstract(){
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
		System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBb");
	}
	
	@Autowired
	private IHelloWorldService hello;

	@RequestMapping("/index.do")
	@ResponseBody
	public String index() {
//		ModelAndView view = new ModelAndView("index");
		System.out.println("******************");
		System.out.println("******************");
		System.out.println("******************");
		return hello.helloWorld("lao wang");
	}

	@RequestMapping("/stopJettyServer.do")
	@ResponseBody
	public Object stopJettyServer() {
		MicrobeLogger.info("stoping jetty server now ...");
		Executors.newFixedThreadPool(1).execute(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(5000);
					System.out.println("*******************************");
					JettyBootstrap.stopServer();
					if (JettyBootstrap.isJettyStoped())
						System.exit(1);
				} catch (InterruptedException e) {
					MicrobeLogger.error("", e);
				}

			}
		});
		MicrobeLogger.info("stoped!");
		return "server is stoped!";
	}
}