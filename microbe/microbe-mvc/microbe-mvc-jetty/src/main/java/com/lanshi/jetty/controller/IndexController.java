package com.lanshi.jetty.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lanshi.microbe.Logger.MicrobeLogger;

@Controller
@SuppressWarnings("UnusedDeclaration")
public class IndexController {
	
	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info(getClass() + " is inited!");
	}


    @Value("${example.message}")
    private String message;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public String showIndex() {
        return message;
    }

}
