package com.lanshi.jetty.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.lanshi.jetty.controller")
public class AppConfig {
}