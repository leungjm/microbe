package com.lanshi.microbe.jetty;

import java.io.IOException;

import org.springframework.beans.BeansException;
import org.springframework.context.support.AbstractApplicationContext;

import com.lanshi.microbe.container.spring.SpringContainerImpl;

public class JettyBootstrap {
	private static AbstractApplicationContext ctx;

	public static void main(String[] args) {
		bootStart();
	}
	
	public static void bootStart(){
		SpringContainerImpl container = startContainer();
		ctx = container.getContext().get();
//		startServer();
	}

	private static SpringContainerImpl startContainer() {
		SpringContainerImpl container = new SpringContainerImpl();
		container.start();
		return container;
	}

	private static void startServer() {
		try {
			ctx.getBean(JettyContainer.class).start();
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void stopServer() {
		JettyContainer jetty = ctx.getBean(JettyContainer.class);
		jetty.stop();
		// ctx.stop();
	}

	public static boolean isJettyStoped() {
		return ctx.getBean(JettyContainer.class).getContext().get().isStopped();
	}
}
