package com.lanshi.microbe.jetty;

import java.util.concurrent.BlockingQueue;

import javax.annotation.PostConstruct;

import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.lanshi.microbe.Logger.MicrobeLogger;

//@Component
public class JettyContainer2 implements ApplicationContextAware{

	private static final String DEFAULT_WEBAPP_PATH = "/";
	@Value("${jetty.webapp.path}")
	private String webappPath;
	@Value("${jetty.context.path}")
	private String contextPath;
	@Value("${jetty.webapp.context.path}")
	private String webappContextPath;
	@Value("${jetty.resource.base.path}")
	private String resourceBasePath;
	@Value("${jetty.port}")
	private int port;
	@Value("${jetty.queued.max.threads}")
	private int maxThreads;
	@Value("${jetty.queued.min.threads}")
	private int minThreads;
	@Value("${jetty.queued.idle.timeout}")
	private int idleTimeout;
	@Value("${jetty.server.connector.acceptors.num}")
	private int acceptors;
	@Value("${jetty.server.connector.selectors.num}")
	private int selectors;
	@Value("${jetty.connection.outputBufferSize}")
	private int outputBufferSize;
	@Value("${jetty.connection.headerCacheSize}")
	private int headerCacheSize;
	@Value("${jetty.connection.responseHeaderSize}")
	private int responseHeaderSize;

	private BlockingQueue<Runnable> queue;
	private JettyContext jettyContext;
	private ConnectionFactory connectionFactory;
	private ApplicationContext applicationContext;

	@PostConstruct
	public void start() {
		initConfig();
		ThreadPool pool = new QueuedThreadPool(maxThreads, minThreads,
				idleTimeout, queue);
		Server server = new Server(pool);
		HandlerList handlerList = new HandlerList();
		// 设置在JVM退出时关闭Jetty的钩子。
		server.setStopAtShutdown(true);
		jettyContext = new JettyContext(server);
		// Http配置
		HttpConfiguration config = new HttpConfiguration();
		config.setOutputBufferSize(outputBufferSize);
		config.setHeaderCacheSize(headerCacheSize);
		config.setResponseHeaderSize(responseHeaderSize);
		connectionFactory = new HttpConnectionFactory(config);
		// http的连接器
		ServerConnector connector = new ServerConnector(server, acceptors,
				selectors, connectionFactory);
		connector.setPort(port);
		// 解决Windows下重复启动Jetty居然不报告端口冲突的问题.
		connector.setReuseAddress(false);
		server.setConnectors(new Connector[] { connector });
		String realWebappPath = webappPath == null
				| webappPath.trim().equals("") ? DEFAULT_WEBAPP_PATH
				: webappPath;
		WebAppContext webContext = new WebAppContext();
		webContext.setContextPath(contextPath);
//		webContext.setBaseResource(Resource.newClassPathResource("META-INF/webapp"));
//				new WebAppContext(realWebappPath, contextPath);
		//- webContext.setContextPath("/");
		//- webContext.setDescriptor("src/main/webapp/WEB-INF/web.xml");
		webContext.setClassLoader(applicationContext.getClassLoader());  
//		webContext.setDescriptor(webappContextPath);
		// 设置webapp的位置
		webContext.setResourceBase(resourceBasePath);
//		webContext.setClassLoader(Thread.currentThread()
//				.getContextClassLoader());
//        webContext.setClassLoader(applicationContext.getClassLoader());  
		XmlWebApplicationContext context = new XmlWebApplicationContext();
        context.setParent(applicationContext);  
        context.setConfigLocations(new String[]{"classpath:spring/mvc/applicationContext.xml"});
        context.setServletContext(webContext.getServletContext());
        context.refresh();
//        webContext.setAttribute(  
//                WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE,  
//                context);
//		server.setHandler(webContext);
		// 以下代码是关键  
		/*webContext.setClassLoader(applicationContext.getClassLoader());  
          
        XmlWebApplicationContext xmlWebAppContext = new XmlWebApplicationContext();  
        xmlWebAppContext.setParent(applicationContext);  
        xmlWebAppContext.setConfigLocation("");  
        xmlWebAppContext.setServletContext(webContext.getServletContext());  
        xmlWebAppContext.refresh();  
          
        webContext.setAttribute(  
                WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE,  
                xmlWebAppContext);*/
        //-----------------------------
		
        ServletContextHandler spingMvcHandler = new ServletContextHandler();
        spingMvcHandler.setContextPath("/");
        spingMvcHandler.addEventListener(new ContextLoaderListener(context));
        spingMvcHandler.addServlet(new ServletHolder(new DispatcherServlet(context)), "*.do");
        handlerList.addHandler(spingMvcHandler);
//        server.setHandler(spingMvcHandler);
        server.setHandler(handlerList);
		handlerList.addHandler(webContext);
		try {
			server.stop();
			server.start();
			server.join();
		} catch (Exception e) {
			MicrobeLogger.error("Jetty Container Start Error.", e);
			System.exit(-1);
		}
	}

	private void initConfig() {
		// TODO Auto-generated method stub

	}

	public void stop() {
		try {
			Server server = jettyContext.get();
			if (server != null) {
				// jettyContext.get().getStopAtShutdown();
				jettyContext.get().stop();
			} else {
				MicrobeLogger
						.error("Jetty Container Stop Error Without Started.");
			}
		} catch (Exception e) {
			MicrobeLogger.error("Jetty Container Stop Error.", e);
			System.exit(-1);
		}

	}

	public JettyContext getContext() {
		return jettyContext;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
		
	}

}
