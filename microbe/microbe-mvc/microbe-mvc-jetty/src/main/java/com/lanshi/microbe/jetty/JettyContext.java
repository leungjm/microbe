package com.lanshi.microbe.jetty;

import org.eclipse.jetty.server.Server;

public class JettyContext {

	private final Server server;

	public JettyContext(final Server server) {
		this.server = server;
	}

	public Server get() {
		return this.server;
	}

}
