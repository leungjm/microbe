package com.lanshi.microbe.container.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.ExecutorServiceFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.container.netty.cpu.NameExecutorServiceFactory;
import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.remoting.server.IServer;

@Component
public class ServletServer implements IServer, ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Value("${server.boss.group.threads}")
	private int bossGroupThreads;

	@Value("${server.worker.group.threads}")
	private int workerGroupThreads;

	@Value("${server.backlog.size}")
	private int backlogSize;

	// acceptor deal thread factory
	private ExecutorServiceFactory bossFactory;
	// connecter deal thread factory
	private ExecutorServiceFactory workerFactory;

	private NioEventLoopGroup bossGroup;
	private NioEventLoopGroup workerGroup;

	@Override
	public void start(String host, int port) {
		ServerBootstrap b = new ServerBootstrap();
		bossFactory = new NameExecutorServiceFactory("boss");
		workerFactory = new NameExecutorServiceFactory("worker");
		bossGroup = new NioEventLoopGroup(bossGroupThreads, bossFactory);
		workerGroup = new NioEventLoopGroup(workerGroupThreads, workerFactory);

		try {
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.option(ChannelOption.SO_BACKLOG, backlogSize)
					.childOption(ChannelOption.SO_KEEPALIVE, true)
					.childOption(ChannelOption.TCP_NODELAY, true)
					.childHandler(new DispatcherServletChannelInitializer());

			InetAddress address = null;
			if (host == null || host.equals("")) {
				address = InetAddress.getLocalHost();
			} else {
				address = InetAddress.getByName(host);
			}
			Channel channel = b.bind(address, port).sync().channel();
			MicrobeLogger.info(
					"Netty Servlet Server started at {} with protocol {}",
					channel.localAddress(), "Http");
			channel.closeFuture().sync();
		} catch (InterruptedException | UnknownHostException e) {
			throw new ServerException(IServer.SYSTEM_MESSAGE_ID, e);
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

	@Override
	public boolean stop() {
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		bossGroup = null;
		workerGroup = null;
		MicrobeLogger.info("Netty Servlet server stoped!");
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
