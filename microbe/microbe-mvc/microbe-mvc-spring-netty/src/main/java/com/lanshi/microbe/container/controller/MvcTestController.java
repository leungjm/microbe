package com.lanshi.microbe.container.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MvcTestController {
	// @Autowired

	@RequestMapping("/foo")
	@ResponseBody
	public String handleFoo() {
		return "Hello world";
	}

	@RequestMapping("/duw")
	@ResponseBody
	public HelloWorld handleDuanwu() {
		HelloWorld hw = new HelloWorld();
		hw.setName("端午节快乐");
		return hw;
	}

	@RequestMapping(value = "/foo2", method = RequestMethod.DELETE)
	@ResponseBody
	public String handleFoo2() {
		return "Hello world";
	}

	class HelloWorld {
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

}
