package com.lanshi.microbe.container.spring;

import javax.servlet.ServletException;

import org.springframework.beans.BeansException;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.container.IContainer;

public class SpringContainer implements IContainer {
	public static final String SPRING_XML_PATH = "classpath:spring/root/applicationContext.xml";
	private SpringContext springContext;
	private static DispatcherServlet dispatcherServlet;

	@Override
	public void start() {
		try {
			MockServletContext servletContext = new MockServletContext();
			MockServletConfig servletConfig = new MockServletConfig(
					servletContext);
			servletConfig.addInitParameter("contextConfigLocation",
					"classpath:/META-INF/spring/root-context.xml");
			servletContext.addInitParameter("contextConfigLocation",
					"classpath:/META-INF/spring/root-context.xml");

			// AnnotationConfigWebApplicationContext applicationContext = new
			// AnnotationConfigWebApplicationContext();
			XmlWebApplicationContext applicationContext = new XmlWebApplicationContext();
			
			// ClassPathXmlApplicationContext applicationContext = new
			// ClassPathXmlApplicationContext();
			applicationContext.setServletContext(servletContext);
			applicationContext.setServletConfig(servletConfig);
			applicationContext.setConfigLocation(SPRING_XML_PATH);
			// applicationContext.register(WebConfig.class);
			applicationContext.refresh();

			dispatcherServlet = new DispatcherServlet(applicationContext);
			dispatcherServlet.init(servletConfig);
			springContext = new SpringContext(applicationContext);

			MicrobeLogger.warn(
					"now starting spring content with sprign path:{}",
					SPRING_XML_PATH);
			applicationContext.start();
		} catch (BeansException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void stop() {
		MicrobeLogger.warn("now going to stop spring application...");
		if (springContext != null && springContext.get() != null) {
			springContext.get().close();
			springContext = null;
		}

	}

	@Override
	public SpringContext getContext() {
		return springContext;
	}

	public static DispatcherServlet getDispatcherServlet() {
		return dispatcherServlet;
	}

}
