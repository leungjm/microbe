package com.lanshi.microbe.container.netty.cpu;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NameThreadFactory implements ThreadFactory {
	private static final AtomicInteger counter = new AtomicInteger();

	private final String name;

	public NameThreadFactory(final String name) {
		this.name = name;
	}

	@Override
	public Thread newThread(Runnable r) {
		return new Thread(r, name + '-' + counter.getAndIncrement());
	}

}
