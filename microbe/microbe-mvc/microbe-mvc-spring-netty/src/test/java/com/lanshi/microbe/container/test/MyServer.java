package com.lanshi.microbe.container.test;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.ExecutorServiceFactory;

import com.lanshi.microbe.container.netty.cpu.NameExecutorServiceFactory;

public class MyServer {

	private final int port;

	public MyServer(int port) {
		this.port = port;
	}

	// acceptor deal thread factory
	private ExecutorServiceFactory bossFactory;
	// connecter deal thread factory
	private ExecutorServiceFactory workerFactory;

	private NioEventLoopGroup bossGroup;
	private NioEventLoopGroup workerGroup;

	public void run() throws Exception {
		ServerBootstrap server = new ServerBootstrap();
		bossFactory = new NameExecutorServiceFactory("boss");
		workerFactory = new NameExecutorServiceFactory("worker");
		bossGroup = new NioEventLoopGroup(2, bossFactory);
		workerGroup = new NioEventLoopGroup(10, workerFactory);
		try {
			server.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class).localAddress(port)
					.childHandler(new DispatcherServletChannelInitializer());

			server.bind().sync().channel().closeFuture().sync();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

	public static void main(String[] args) throws Exception {
		int port;
		if (args.length > 0) {
			port = Integer.parseInt(args[0]);
		} else {
			port = 7099;
		}
		new MyServer(port).run();
	}
}
