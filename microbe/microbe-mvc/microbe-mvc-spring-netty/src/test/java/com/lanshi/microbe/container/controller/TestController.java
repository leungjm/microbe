package com.lanshi.microbe.container.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
	// @Autowired

	@RequestMapping("/foo")
	@ResponseBody
	public String handleFoo() {
		return "Hello world";
	}

	@RequestMapping(value = "/foo2", method = RequestMethod.DELETE)
	@ResponseBody
	public String handleFoo2() {
		return "Hello world";
	}

}
