package com.lanshi.microbe.register.server;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.BeansException;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.register.IRegisterManager;
import com.lanshi.microbe.remoting.netty.server.NettyServer;

public class RegisterServerStartup {
	private static AbstractApplicationContext ctx;

	public static void main(String[] args) throws BeansException,
			NumberFormatException, UnknownHostException {
		SpringContainerImpl container = startContainer();
		ctx = container.getContext().get();
		startServer();
	}

	private static SpringContainerImpl startContainer() {
		SpringContainerImpl container = new SpringContainerImpl();
		container.start();
		return container;
	}

	private static void startServer() throws BeansException,
			NumberFormatException, UnknownHostException {
		Object port = ctx.getBean(PropertySourcesPlaceholderConfigurer.class)
				.getAppliedPropertySources().get("localProperties")
				.getProperty("server.port");
		ctx.getBean(NettyServer.class).start(
				InetAddress.getLocalHost().getHostAddress(),
				Integer.parseInt(port.toString()));
		ctx.getBean(IRegisterManager.class).register();
		// ctx.getBean(TestIRegister.class).unregister("123");
	}

	public static void stopServer() {
		ctx.getBean(NettyServer.class).stop();
	}
}
