package com.lanshi.microbe.register.api.test;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.register.annotation.RmiProvider;
import com.lanshi.microbe.register.api.TestIRegister;

@Component
@RmiProvider(desc = "测试", interfaceClass = TestIRegister.class, name = "test", version = "1.0")
public class TestIRegisterImpl implements TestIRegister {

	@PostConstruct
	public void init() {
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	}

	@Override
	public boolean unregister(String request) {
		System.out.println("#######################" + request);
		return false;
	}

}
