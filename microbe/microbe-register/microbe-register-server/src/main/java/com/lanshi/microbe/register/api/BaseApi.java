package com.lanshi.microbe.register.api;

import javax.annotation.PostConstruct;

import com.lanshi.microbe.Logger.MicrobeLogger;

public abstract class BaseApi {
	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info("{} is inited!", getClass());
	}
}
