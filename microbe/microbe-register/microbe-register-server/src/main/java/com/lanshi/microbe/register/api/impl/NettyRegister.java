package com.lanshi.microbe.register.api.impl;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.register.api.BaseApi;
import com.lanshi.microbe.register.api.IRegister;
import com.lanshi.microbe.register.model.CopyOnWriteMap;
import com.lanshi.microbe.register.model.RegisterRequest;

@Component
public class NettyRegister extends BaseApi implements IRegister {
	CopyOnWriteMap<String, List<InetSocketAddress>> serviceHost = new CopyOnWriteMap<String, List<InetSocketAddress>>();

	@Override
	public synchronized CopyOnWriteMap<String, List<InetSocketAddress>> doRegister(
			RegisterRequest request) {
		Map<String, Class<?>> services = request.getClazzs();
		InetSocketAddress socketAddress = request.getSocketAddress();
		if (services != null) {
			for (String key : services.keySet()) {
				if (serviceHost.containsKey(key)) {
					List<InetSocketAddress> existAddress = serviceHost.get(key);
					existAddress.add(socketAddress);
					serviceHost.put(key, existAddress);
				} else {
					List<InetSocketAddress> newSocketAddress = (List<InetSocketAddress>) Collections
							.synchronizedList(new ArrayList<InetSocketAddress>());
					newSocketAddress.add(socketAddress);
					serviceHost.put(key, newSocketAddress);
				}
			}
		}
		return serviceHost;
	}

	@Override
	public synchronized boolean unregister(RegisterRequest request) {
		Map<String, Class<?>> services = request.getClazzs();
		InetSocketAddress socketAddress = request.getSocketAddress();
		if (services != null) {
			for (String key : services.keySet()) {
				if (serviceHost.containsKey(key)) {
					serviceHost.get(key).remove(socketAddress);
				}
			}
		}
		return true;
	}

	@Override
	public CopyOnWriteMap<String, List<InetSocketAddress>> getRegisterClient() {
		return serviceHost;
	}
}
