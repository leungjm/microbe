package com.microbe.register;

import javax.annotation.PostConstruct;

import org.slf4j.LoggerFactory;

public abstract class BaseManager {

	@PostConstruct
	public void init() {
		// MicrobeLogger.info(getClass() + " is init!");
		LoggerFactory.getLogger(getClass()).info(getClass() + " is init!");
	}
}
