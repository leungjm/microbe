package com.microbe.register.normal.manager;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.register.IRegisterManager;
import com.lanshi.microbe.register.IRouteManager;
import com.lanshi.microbe.register.model.CopyOnWriteMap;
import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.InterfaceRequireCenter;
import com.lanshi.microbe.register.model.MicrobeSocketAddress;
import com.lanshi.microbe.register.model.Route;
import com.lanshi.microbe.register.model.ServiceProvider;
import com.lanshi.microbe.register.model.ServiceProviderCenter;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;
import com.microbe.register.BaseManager;

@Component
public class NormalRegisterManager extends BaseManager implements
		IRegisterManager<IClient>, ApplicationContextAware {

	@Value("${register.server.host:null}")
	private String registerServerHost;
	@Value("${register.server.port:null}")
	private Integer registerServerPort;
	@Value("${server.port:null}")
	private Integer servicePort;

	/**
	 * 本地路由实现
	 */
	private static CopyOnWriteMap<MicrobeSocketAddress, IClient> localClients = new CopyOnWriteMap<MicrobeSocketAddress, IClient>();

	private static IClient registerClient = null;

	private static LinkedBlockingQueue<Boolean> registerFlag = new LinkedBlockingQueue<Boolean>(
			1);

	private ApplicationContext applicationContext;

	@Autowired
	private IRouteManager routeManager;

	class updataLocalRoute implements Runnable {
		@Override
		public void run() {
			MicrobeLogger.info("updataLocalRoute running....");
			if (registerServerHost != null
					&& !registerServerHost.trim().isEmpty()
					&& registerServerPort != null && registerServerPort > 0) {
				try {
					if (registerClient == null) {
						InetSocketAddress registerServerAddr = new InetSocketAddress(
								registerServerHost, registerServerPort);
						registerClient = getNewClientInstance();
						registerClient.connect(registerServerAddr);
					}
					List<InterfaceRequire> require = routeManager
							.getLocalInterfaceRequire();
					List<ServiceProvider> provider = routeManager
							.getLocalService();
					MicrobeSocketAddress localAddr = null;
					try {
						localAddr = new MicrobeSocketAddress(InetAddress
								.getLocalHost().getHostAddress(), servicePort);
					} catch (UnknownHostException e) {
						MicrobeLogger
								.warn("local host service is un config service port, make sure you are not the service provider!",
										e);
					}
					MicrobeLogger
							.info("require interface size : {} , provider service size : {}",
									require.size(), provider.size());
					for (InterfaceRequire req : require) {
						System.out.println(req.getInterfaceClass());
					}
					for (ServiceProvider pro : provider) {
						System.out.println(pro.getServiceInterface());
					}

					System.out.println();
					Response response = registerClient.sendRequest(new Request(
							IRegisterManager.class, "register2Server",
							localAddr, require, provider));
					if (response.getException() == null) {
						Object result = response.getReturnValue();
						if (result != null) {
							routeManager.refreshRoute((Route) result);
						}
					} else {
						throw response.getException();
					}

					// 更新本地clients
					List<ServiceProviderCenter> providerAct = routeManager
							.getRequireInterfaceProvider(require);

					MicrobeLogger.info("get remote provider size : {}",
							providerAct.size());
					boolean refresh = routeManager.addRoute(providerAct);
					if (refresh) {
						MicrobeLogger.info("refresh local route finish!!");
					}
					if (registerFlag.isEmpty()) {
						registerFlag.add(true);
					}
				} catch (Exception e) {
					MicrobeLogger.warn("someting wrong in register to server!",
							e);
				} catch (Throwable e) {
					MicrobeLogger.warn("someting wrong in register to server!",
							e);
				}
			} else {
				MicrobeLogger
						.error("uncorrect register host {} and port {} for InetSocketAddress!",
								registerServerHost, registerServerPort);
				throw new RuntimeException("uncorrect register host and port");
			}
		}

	}

	@Override
	public void register() {
		MicrobeLogger
				.info("going to register to server {}", registerServerHost);
		ScheduledExecutorService scheduled = Executors
				.newScheduledThreadPool(1);
		scheduled.scheduleAtFixedRate(new updataLocalRoute(), 0, 60,
				TimeUnit.SECONDS);

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public Route register2Server(MicrobeSocketAddress socketAddress,
			CopyOnWriteArrayList<InterfaceRequire> require,
			CopyOnWriteArrayList<ServiceProvider> provider) {
		Route route = new Route();
		if (require != null) {
			for (InterfaceRequire req : require) {
				routeManager.addRemoteInterface(new InterfaceRequireCenter(req,
						socketAddress));
			}
		}

		// List<ServiceProviderCenter> result = routeManager
		// .getRequireInterfaceProvider(require);
		if (provider != null) {
			for (ServiceProvider prov : provider) {
				routeManager.addRemoteService(new ServiceProviderCenter(prov,
						socketAddress));
			}
		}
		route.setInterfaceServicesCenter(routeManager.getRequires());
		route.setServiceProvidersCenter(routeManager.getProviders());
		return route;
	}

	@Override
	public boolean unregister(MicrobeSocketAddress socketAddress) {
		localClients.remove(socketAddress);
		return routeManager.removeRoute(socketAddress);
	}

	@Override
	public IClient getNewClientInstance() {
		return applicationContext.getBean(IClient.class);
	}

	@Override
	public IClient getClient(MicrobeSocketAddress socketAddress) {
		IClient client = localClients.get(socketAddress);
		if (client == null) {
			client = getNewClientInstance();
			InetSocketAddress real = socketAddress.getInetSocketAddress();
			MicrobeLogger.info("client connect to {}", real.toString());
			client.connect(real);
			localClients.put(socketAddress, client);
		}
		return client;
	}

	@Override
	public boolean isregister() {
		boolean isreg = false;
		try {
			isreg = registerFlag.take();
			registerFlag.put(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return isreg;
	}

}
