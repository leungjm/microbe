package com.microbe.register;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.register.IRegisterManager;
import com.lanshi.microbe.register.IRouteManager;
import com.lanshi.microbe.register.RouteTable;
import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.InterfaceRequireCenter;
import com.lanshi.microbe.register.model.MicrobeSocketAddress;
import com.lanshi.microbe.register.model.Route;
import com.lanshi.microbe.register.model.ServiceProvider;
import com.lanshi.microbe.register.model.ServiceProviderCenter;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

/**
 * 注册中心管理
 * 
 * @author liang.jm
 * 
 */
@Component
public class RouteManager extends BaseManager implements IRouteManager {

	private Random random = new Random();
	/**
	 * 本地所需要的接口服务
	 */
	// private static CopyOnWriteArrayList<InterfaceRequire> interfaceServices =
	// new CopyOnWriteArrayList<InterfaceRequire>();
	/**
	 * 本地所需要尚未获得提供的服务接口
	 */
	// private static CopyOnWriteArrayList<InterfaceRequire>
	// interfaceRequireServices = new CopyOnWriteArrayList<InterfaceRequire>();
	/**
	 * 本地所提供的远程服务
	 */
	// private static CopyOnWriteArrayList<ServiceProvider> serviceProviders =
	// new CopyOnWriteArrayList<ServiceProvider>();
	/**
	 * 本地路由表
	 */
	// private static CopyOnWriteMap<Class<?>,
	// CopyOnWriteArrayList<InetSocketAddress>> localRoute = new
	// CopyOnWriteMap<Class<?>, CopyOnWriteArrayList<InetSocketAddress>>();
	/**
	 * 远程注册所需的接口服务
	 */
	// private static CopyOnWriteArrayList<InterfaceRequireCenter>
	// interfaceServicesCenter = new
	// CopyOnWriteArrayList<InterfaceRequireCenter>();
	/**
	 * 远程注册所提供的服务
	 */
	// private static CopyOnWriteArrayList<ServiceProviderCenter>
	// serviceProvidersCenter = new
	// CopyOnWriteArrayList<ServiceProviderCenter>();

	@Autowired
	private IRegisterManager<IClient> registerManager;

	public void addLocalInterface(InterfaceRequire require) {
		RouteTable.interfaceServices.addIfAbsent(require);
	}

	public void removeLocalInterface(InterfaceRequire require) {
		RouteTable.interfaceServices.remove(require);
	}

	public List<InterfaceRequire> getLocalInterfaceRequire() {
		return RouteTable.interfaceServices;
	}

	public void addLocalService(ServiceProvider provider) {
		RouteTable.serviceProviders.addIfAbsent(provider);
	}

	public void removeLocalService(ServiceProvider provider) {
		RouteTable.serviceProviders.remove(provider);
	}

	public List<ServiceProvider> getLocalService() {
		return RouteTable.serviceProviders;
	}

	@Override
	public void addRemoteInterface(InterfaceRequireCenter require) {
		RouteTable.interfaceServicesCenter.addIfAbsent(require);

	}

	@Override
	public void removeRemoteInterface(InterfaceRequireCenter require) {
		RouteTable.interfaceServicesCenter.remove(require);
	}

	@Override
	public void addRemoteService(ServiceProviderCenter provider) {
		RouteTable.serviceProvidersCenter.addIfAbsent(provider);

	}

	@Override
	public void removeRemoteService(ServiceProviderCenter provider) {
		RouteTable.serviceProvidersCenter.remove(provider);
	}

	@Override
	public void removeRemoteInterfaces(MicrobeSocketAddress socketAddress) {
		Iterator<InterfaceRequireCenter> iter = RouteTable.interfaceServicesCenter
				.iterator();
		while (iter.hasNext()) {
			InterfaceRequireCenter o = iter.next();
			if (socketAddress.equals(o.getRequireAddress())) {
				RouteTable.interfaceServicesCenter.remove(o);
			}
		}
	}

	@Override
	public void removeRemoteServices(MicrobeSocketAddress socketAddress) {
		Iterator<ServiceProviderCenter> iter = RouteTable.serviceProvidersCenter
				.iterator();
		while (iter.hasNext()) {
			ServiceProviderCenter o = iter.next();
			if (socketAddress.equals(o.getProviderSocketAddress())) {
				RouteTable.serviceProvidersCenter.remove(o);
			}
		}
	}

	@Override
	public List<ServiceProviderCenter> getRequireInterfaceProvider(
			List<InterfaceRequire> requires) {
		List<ServiceProviderCenter> list = new ArrayList<ServiceProviderCenter>();
		if (requires != null && requires.size() > 0) {
			Iterator<ServiceProviderCenter> iter = RouteTable.serviceProvidersCenter
					.iterator();
			while (iter.hasNext()) {
				ServiceProviderCenter o = iter.next();
				for (InterfaceRequire require : requires) {
					if (require.compareTo(o) == 0) {
						list.add(o);
					}
				}
			}
		}
		return list;
	}

	@Override
	public void addStillRequireService(InterfaceRequire require) {
		RouteTable.interfaceRequireServices.add(require);
	}

	@Override
	public void removeStillRequireService(InterfaceRequire require) {
		RouteTable.interfaceRequireServices.remove(require);
	}

	@Override
	public boolean removeRoute(MicrobeSocketAddress clientSocketAddr) {
		Iterator<CopyOnWriteArrayList<MicrobeSocketAddress>> iter = RouteTable.localRoute
				.values().iterator();
		while (iter.hasNext()) {
			CopyOnWriteArrayList<MicrobeSocketAddress> o = iter.next();
			Iterator<MicrobeSocketAddress> clientIter = o.iterator();
			while (clientIter.hasNext()) {
				MicrobeSocketAddress client = clientIter.next();
				if (client.equals(clientSocketAddr)) {
					o.remove(client);
				}
			}
		}
		return true;
	}

	@Override
	public boolean addRoute(List<ServiceProviderCenter> providers) {
		for (ServiceProviderCenter provider : providers) {
			CopyOnWriteArrayList<MicrobeSocketAddress> clients = RouteTable.localRoute
					.get(provider.getServiceInterface());
			boolean addFlag = true;
			if (clients != null) {
				Iterator<MicrobeSocketAddress> clientIter = clients.iterator();
				while (clientIter.hasNext()) {
					MicrobeSocketAddress client = clientIter.next();
					if (client.equals(provider.getServiceInterface())) {
						addFlag = false;
					}
				}
			} else {
				clients = new CopyOnWriteArrayList<MicrobeSocketAddress>();
			}
			if (addFlag) {
				try {
					clients.add(provider.getProviderSocketAddress());
					RouteTable.localRoute.put(provider.getServiceInterface(),
							clients);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public CopyOnWriteArrayList<InterfaceRequireCenter> getRequires() {
		return RouteTable.interfaceServicesCenter;
	}

	@Override
	public CopyOnWriteArrayList<ServiceProviderCenter> getProviders() {
		return RouteTable.serviceProvidersCenter;
	}

	@Override
	public boolean refreshRoute(Route route) {
		RouteTable.interfaceRequireServices.clear();
		RouteTable.interfaceRequireServices.addAll(route
				.getInterfaceServicesCenter());
		RouteTable.serviceProvidersCenter.clear();
		RouteTable.serviceProvidersCenter.addAll(route
				.getServiceProvidersCenter());
		return true;
	}

	@Override
	public Response sendRequest(Request request) {
		CopyOnWriteArrayList<MicrobeSocketAddress> providers = RouteTable.localRoute
				.get(request.getApiClass());
		if (providers != null) {
			int selected = random.nextInt(providers.size());
			IClient client = registerManager.getClient(providers.get(selected));
			if (client == null) {
				return new Response(request.getMessageId(),
						new ServerException(request.getMessageId(),
								new RuntimeException(
										"can not get the service provider with apiClass:"
												+ request.getApiClass())));
			}
			return client.sendRequest(request);
		} else {
			return new Response(request.getMessageId(), new ServerException(
					request.getMessageId(), new RuntimeException(
							"can not get the service provider with apiClass:"
									+ request.getApiClass()
									+ ", other thing local route is empty!")));
		}

	}

	@Override
	public IClient getNewUnconnectClient() {
		// TODO Auto-generated method stub
		return null;
	}
}
