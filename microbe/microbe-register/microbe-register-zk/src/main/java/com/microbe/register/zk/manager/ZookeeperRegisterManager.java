package com.microbe.register.zk.manager;

import java.net.InetSocketAddress;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.register.IRegisterManager;
import com.lanshi.microbe.register.model.CopyOnWriteMap;
import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.MicrobeSocketAddress;
import com.lanshi.microbe.register.model.Route;
import com.lanshi.microbe.register.model.ServiceProvider;
import com.lanshi.microbe.remoting.client.IClient;

@Component
public class ZookeeperRegisterManager implements IRegisterManager<IClient>,
		ApplicationContextAware {
	@Value("${zookeeper.register.servers:null}")
	private String zookeeperRegisterServers;
	@Value("${register.type.name}")
	private boolean registerTypeName;
	/**
	 * 本地路由实现
	 */
	private static CopyOnWriteMap<MicrobeSocketAddress, IClient> localClients = new CopyOnWriteMap<MicrobeSocketAddress, IClient>();

	private static IClient registerClient = null;

	private static LinkedBlockingQueue<Boolean> registerFlag = new LinkedBlockingQueue<Boolean>(
			1);

	private ApplicationContext applicationContext;

	@Override
	public void register() {
		// TODO Auto-generated method stub

	}

	@Override
	public Route register2Server(MicrobeSocketAddress socketAddress,
			CopyOnWriteArrayList<InterfaceRequire> require,
			CopyOnWriteArrayList<ServiceProvider> provider) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean unregister(MicrobeSocketAddress socketAddress) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IClient getNewClientInstance() {
		return applicationContext.getBean(IClient.class);
	}

	@Override
	public IClient getClient(MicrobeSocketAddress socketAddress) {
		IClient client = localClients.get(socketAddress);
		if (client == null) {
			client = getNewClientInstance();
			InetSocketAddress real = socketAddress.getInetSocketAddress();
			MicrobeLogger.info("client connect to {}", real.toString());
			client.connect(real);
			localClients.put(socketAddress, client);
		}
		return client;
	}

	@Override
	public boolean isregister() {
		boolean isreg = false;
		try {
			isreg = registerFlag.take();
			registerFlag.put(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return isreg;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
