package com.lanshi.microbe.register.zk;

import java.io.IOException;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;

public class ZookeeperSimpleTest {

	public static void main(String[] args) {
		try {
			ZooKeeper zk = new ZooKeeper(
					"127.0.0.1:2181,127.0.0.1:2182,127.0.0.1:2183", 5000,
					new Watcher() {

						@Override
						public void process(WatchedEvent event) {
							System.out.println("已经触发了" + event.getType()
									+ "事件！");

						}
					});
			System.out.println(zk.exists("/zkTest", true));
			if (zk.exists("/zkTest", true) == null) {
				zk.create("/zkTest", "zkTestData".getBytes(),
						Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			}
			if (zk.exists("/zkTest/zkTestChild", false) == null) {
				zk.create("/zkTest/zkTestChild", "zkTestChildData".getBytes(),
						Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			}
			System.out.println(new String(zk.getData("/zkTest", true, null)));
			System.out.println(zk.getChildren("/zkTest", false));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeeperException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
