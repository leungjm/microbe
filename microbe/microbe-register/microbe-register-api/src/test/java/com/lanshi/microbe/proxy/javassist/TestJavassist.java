package com.lanshi.microbe.proxy.javassist;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;
import javassist.NotFoundException;

public class TestJavassist {
	public static void main(String[] args) {
		ClassPool pool = new ClassPool(true);
		String className = "com.lanshi.microbe.register.api.impl.Register";
		CtClass cc = pool.makeClass(className);

		try {
			cc.addInterface(pool
					.getCtClass("com.lanshi.microbe.register.api.IRegister"));
			cc.addMethod(CtNewMethod
					.make("public boolean doResgister(java.util.Map clazzs){System.out.println(\"do Something\");return true;}",
							cc));
			// Class<?> clazz = cc.getClass();
			// cc.writeFile("./target/classes");
			// 通过反射创建无参的实例，并调用getName方法
			// Object o = Class.forName(
			// "com.lanshi.microbe.register.api.impl.Register")
			// .newInstance();
			Object o = cc.toClass().newInstance();
			Method getter = o.getClass().getMethod("doResgister", Map.class);
			System.out.println(getter
					.invoke(o, new HashMap<String, Class<?>>()));
		} catch (CannotCompileException e) {
			throw new RuntimeException(e);
		} catch (NotFoundException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
