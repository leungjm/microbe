package com.lanshi.microbe.register;

/**
 * 服务注册接口
 * 
 * @author liangjm
 * 
 */
public interface Register {

	void register();
}
