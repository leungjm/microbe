package com.lanshi.microbe.register.model;

import java.io.Serializable;

import com.lanshi.microbe.register.api.IRegister;

public class InterfaceRequire implements Serializable,
		Comparable<ServiceProvider> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String interfaceName;
	protected Class<?> interfaceClass;
	protected String describe;
	protected String version;

	public InterfaceRequire() {
	}

	public InterfaceRequire(String interfaceName, Class<?> interfaceClass,
			String describe, String version) {
		super();
		this.interfaceName = interfaceName;
		this.interfaceClass = interfaceClass;
		this.describe = describe;
		this.version = version;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public Class<?> getInterfaceClass() {
		return interfaceClass;
	}

	public void setInterfaceClass(Class<?> interfaceClass) {
		this.interfaceClass = interfaceClass;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public int compareTo(ServiceProvider o) {
		if (this.interfaceClass.getName().equals(
				o.getServiceInterface().getName())
				&& this.version.equals(o.getVersion())) {
			return 0;
		}
		return -1;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof InterfaceRequire)
			return false;
		InterfaceRequire oth = (InterfaceRequire) obj;
		return this.interfaceClass.equals(oth.getInterfaceClass())
				&& this.interfaceName.equals(oth.getInterfaceName())
				&& this.version.equals(oth.getVersion());
	}

	public static void main(String[] args) {
		ServiceProvider sp = new ServiceProvider("ABC", IRegister.class, "",
				"1.1");
		InterfaceRequire ir = new InterfaceRequire("BCD", IRegister.class, "",
				"1.1");
		if (sp.compareTo(ir) == 0) {
			System.out.println("哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈");
		} else {
			System.out.println("哐哐哐哐哐哐");
		}
	}
}
