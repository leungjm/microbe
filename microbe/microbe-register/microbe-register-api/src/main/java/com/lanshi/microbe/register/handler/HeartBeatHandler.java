package com.lanshi.microbe.register.handler;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.register.service.IClientRegister;
import com.lanshi.microbe.register.service.IHeartBeat;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

@Component
public class HeartBeatHandler {

	@Autowired
	private IClientRegister<IClient> clientRegister;

	public HeartBeatHandler() {
	}

	public boolean sendHeartBeat() {
		List<IClient> clients = clientRegister.getClients();
		if (clients != null && clients.size() > 0) {
			ScheduledExecutorService executor = Executors
					.newSingleThreadScheduledExecutor();
			executor.scheduleAtFixedRate(new HeartBeatTask(clientRegister), 1,
					5, TimeUnit.SECONDS);
		}
		return false;
	}

	private class HeartBeatTask implements Runnable {
		private final IClientRegister<IClient> regcli;

		public HeartBeatTask(IClientRegister<IClient> regcli) {
			this.regcli = regcli;
		}

		@Override
		public void run() {
			MicrobeLogger.info("going to send heart beat msg...");
			List<IClient> clients = regcli.getClients();
			for (IClient client : clients) {
				try {
					Response response = client.sendRequest(new Request(
							IHeartBeat.class, "beat", "beat-client"));
					if (response.getException() != null) {
						throw response.getException();
					}
					MicrobeLogger.info("{} get heart beat back with result {}",
							client.getRemoteAddress(),
							response.getReturnValue());
				} catch (Throwable e) {
					MicrobeLogger
							.error("client with "
									+ client.getRemoteAddress()
									+ " is close, now unregister services and going to reconnect! ",
									e);
					// IClient newClient = regcli.getNewClientInstance();
					// newClient.connect(client.getRemoteAddress());
					regcli.reconnectServer(client.getRemoteAddress());
					break;
				}
			}
		}

	}
}
