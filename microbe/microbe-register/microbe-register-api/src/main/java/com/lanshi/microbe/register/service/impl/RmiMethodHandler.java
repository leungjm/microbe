package com.lanshi.microbe.register.service.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.exection.ClientException;
import com.lanshi.microbe.exection.ServerException;
import com.lanshi.microbe.register.IRegisterManager;
import com.lanshi.microbe.register.service.IRmiClient;
import com.lanshi.microbe.register.service.IRmiMethodHandler;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

@Component
public class RmiMethodHandler implements IRmiMethodHandler {

	@PostConstruct
	public void init() {
		System.out.println("init rmiMethodhandler.....");
	}

	// @Value("${register.server.host}")
	// private String registeServerHost;
	// @Value("${register.server.port}")
	// private int registeServerPort;
	// @Autowired
	// private IClient nettyClient;
	@Autowired
	private IRmiClient<IClient> rmiClient;
	@Autowired
	private IRegisterManager<IClient> registerManager;

	@Override
	public Object invoke(Class<?> proxyInterface, String methodName,
			Object[] args) {
		// nettyClient.connect(new InetSocketAddress(registeServerHost,
		// registeServerPort));
		Request request = new Request(proxyInterface, methodName, args);
		Response response = null;
		if (registerManager.isregister()) {
			// Response response = nettyClient.sendRequest(request);
			response = rmiClient.sendRequest(request);
			if (request.getMessageId() == response.getMessageId()) {
				if (response.getException() != null) {
					MicrobeLogger.error("Rmi request found Exception:",
							response.getException());
					return null;
				} else {
					return response.getReturnValue();
				}
			}
		}
		throw new ServerException(request.getMessageId(), new ClientException(
				new RuntimeException("request messageId is "
						+ request.getMessageId() + ", but return messageId is "
						+ (response != null ? response.getMessageId() : "null")
						+ ", undesired! ")));
	}
}
