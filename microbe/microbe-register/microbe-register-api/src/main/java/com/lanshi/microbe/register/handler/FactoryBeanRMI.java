package com.lanshi.microbe.register.handler;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cglib.core.SpringNamingPolicy;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.context.ApplicationContext;

public class FactoryBeanRMI<T> implements InitializingBean, FactoryBean<T> {
	private ApplicationContext applicationContext;
	private String innerClassName;

	public FactoryBeanRMI(String innerClassName,
			ApplicationContext applicationContext) {
		this.innerClassName = innerClassName;
		this.applicationContext = applicationContext;
	}

	public void setInnerClassName(String innerClassName) {
		this.innerClassName = innerClassName;
	}

	@SuppressWarnings("unchecked")
	public T getObject() throws Exception {
		@SuppressWarnings("rawtypes")
		Class innerClass = Class.forName(innerClassName);
		if (innerClass.isInterface()) {
			return (T) InterfaceProxy.newInstance(innerClass,
					applicationContext);
		} else {
			Enhancer enhancer = new Enhancer();
			enhancer.setSuperclass(innerClass);
			enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
			enhancer.setCallback(new MethodInterceptorImpl());
			return (T) enhancer.create();
		}
	}

	public Class<?> getObjectType() {
		try {
			return Class.forName(innerClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isSingleton() {
		return true;
	}

	public void afterPropertiesSet() throws Exception {
	}
}
