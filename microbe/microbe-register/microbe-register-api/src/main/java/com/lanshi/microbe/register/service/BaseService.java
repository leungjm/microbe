package com.lanshi.microbe.register.service;

import javax.annotation.PostConstruct;

import com.lanshi.microbe.Logger.MicrobeLogger;

public abstract class BaseService {
	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info("{} is inited!", getClass());
	}
}
