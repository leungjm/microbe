package com.lanshi.microbe.register.service;

import java.net.InetSocketAddress;
import java.util.List;

import com.lanshi.microbe.remoting.client.IClient;

public interface IClientRegister<T extends IClient> {
	public boolean register2Server();

	public boolean unregister();

	public T getLoadBalanceClient(String serviceName);

	public T getNewClientInstance();

	public List<T> getClients();

	public void reconnectServer(InetSocketAddress socketAddress);
}
