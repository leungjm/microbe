package com.lanshi.microbe.register.model;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

public class Route implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 远程注册所需的接口服务
	 */
	private CopyOnWriteArrayList<InterfaceRequireCenter> interfaceServicesCenter;
	/**
	 * 远程注册所提供的服务
	 */
	private CopyOnWriteArrayList<ServiceProviderCenter> serviceProvidersCenter;

	public CopyOnWriteArrayList<InterfaceRequireCenter> getInterfaceServicesCenter() {
		return interfaceServicesCenter;
	}

	public void setInterfaceServicesCenter(
			CopyOnWriteArrayList<InterfaceRequireCenter> interfaceServicesCenter) {
		this.interfaceServicesCenter = interfaceServicesCenter;
	}

	public CopyOnWriteArrayList<ServiceProviderCenter> getServiceProvidersCenter() {
		return serviceProvidersCenter;
	}

	public void setServiceProvidersCenter(
			CopyOnWriteArrayList<ServiceProviderCenter> serviceProvidersCenter) {
		this.serviceProvidersCenter = serviceProvidersCenter;
	}

}
