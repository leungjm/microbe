package com.lanshi.microbe.register.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.springframework.context.ApplicationContext;

import com.lanshi.microbe.register.service.IRmiMethodHandler;

public class InterfaceProxy implements InvocationHandler {

	private ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		System.out.println("ObjectProxy execute:" + method.getName());
		IRmiMethodHandler rmiMethodHandler = applicationContext
				.getBean(IRmiMethodHandler.class);
		if (rmiMethodHandler != null) {
			Class<?>[] inter = proxy.getClass().getInterfaces();
			return rmiMethodHandler.invoke(inter[0], method.getName(), args);
		}
		return method.invoke(proxy, args);
	}

	@SuppressWarnings("unchecked")
	public static <T> T newInstance(Class<T> innerInterface,
			ApplicationContext applicationContext) {
		ClassLoader classLoader = innerInterface.getClassLoader();
		Class<?>[] interfaces = new Class[] { innerInterface };
		InterfaceProxy proxy = new InterfaceProxy();
		proxy.setApplicationContext(applicationContext);
		return (T) Proxy.newProxyInstance(classLoader, interfaces, proxy);
	}
}