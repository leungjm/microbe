package com.lanshi.microbe.register.service;

//@RmiRegister(desc = "心跳接口", name = "heartBeatServer", version = "1.0")
public interface IHeartBeat {
	public Byte beat(String hostAddr);
}
