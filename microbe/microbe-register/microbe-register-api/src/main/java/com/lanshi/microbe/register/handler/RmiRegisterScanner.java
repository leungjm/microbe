package com.lanshi.microbe.register.handler;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import com.lanshi.microbe.register.RouteTable;
import com.lanshi.microbe.register.annotation.RmiProvider;
import com.lanshi.microbe.register.annotation.RmiRegister;
import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.ServiceProvider;

public class RmiRegisterScanner extends ClassPathBeanDefinitionScanner {
	private ApplicationContext applicationContext;

	public RmiRegisterScanner(BeanDefinitionRegistry beanFactory) {
		super(beanFactory);
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;

	}

	@Override
	protected void registerDefaultFilters() {
		this.addIncludeFilter(new AnnotationTypeFilter(RmiRegister.class));
		this.addIncludeFilter(new AnnotationTypeFilter(RmiProvider.class));
	}

	@Override
	protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
		Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);
		for (BeanDefinitionHolder holder : beanDefinitions) {// 查询到需要RMI注册的接口
			GenericBeanDefinition definition = (GenericBeanDefinition) holder
					.getBeanDefinition();
			// 生成RMI接口STUB,无需生成，使用InvocationHandler代理实现
			// System.out.println(holder.getBeanDefinition().getBeanClassName());
			// System.out.println(holder.getShortDescription());
			// System.out.println(holder.getLongDescription());
			// String proxyBeanName =
			// ProxyRmiStubUtil.generatorProxyClass(holder
			// .getBeanDefinition().getBeanClassName());
			// definition.getPropertyValues().add("proxyClassName",
			// proxyBeanName);
			// definition.getPropertyValues().add("innerClassName",
			// definition.getBeanClassName());
			definition.getConstructorArgumentValues().addIndexedArgumentValue(
					0, definition.getBeanClassName());
			definition.getConstructorArgumentValues().addIndexedArgumentValue(
					1, applicationContext);
			definition.setBeanClass(FactoryBeanRMI.class);
		}
		return beanDefinitions;
	}

	@Override
	protected boolean isCandidateComponent(
			AnnotatedBeanDefinition beanDefinition) {// 扫描RmiRegister注解接口
		// IRouteManager route =
		// applicationContext.getBean(IRouteManager.class);
		// return super.isCandidateComponent(beanDefinition)
		// && beanDefinition.getMetadata().hasAnnotation(
		// RmiRegister.class.getName());
		if (super.isCandidateComponent(beanDefinition)
				&& beanDefinition.getMetadata().hasAnnotation(
						RmiProvider.class.getName())) {
			System.out.println(beanDefinition.getBeanClassName());
			Map<String, Object> attrs = beanDefinition.getMetadata()
					.getAnnotationAttributes(RmiProvider.class.getName());
			ServiceProvider provider = getServiceProviderByAttrs(attrs);
			if (provider != null) {
				// IRouteManager route = applicationContext
				// .getBean(IRouteManager.class);
				// route.addLocalService(provider);
				RouteTable.addLocalService(provider);
			}
		}
		boolean flag = beanDefinition.getMetadata().isInterface()
				&& beanDefinition.getMetadata().hasAnnotation(
						RmiRegister.class.getName());
		if (flag) {
			Map<String, Object> attrs = beanDefinition.getMetadata()
					.getAnnotationAttributes(RmiRegister.class.getName());
			InterfaceRequire require = getInterfaceRequireByAttrs(attrs,
					beanDefinition.getBeanClassName());
			if (require != null) {
				// IRouteManager route = applicationContext
				// .getBean(IRouteManager.class);
				// route.addLocalInterface(require);
				RouteTable.addLocalInterface(require);
			}
		}
		return flag;
	}

	private InterfaceRequire getInterfaceRequireByAttrs(
			Map<String, Object> attrs, String beanClassName) {
		Class<?> interfaceClass;
		String describe;
		String interfaceName;
		String version;
		if (attrs != null) {
			interfaceName = (String) attrs.get("name");
			describe = (String) attrs.get("desc");
			version = (String) attrs.get("version");
			try {
				interfaceClass = Class.forName(beanClassName);
				return new InterfaceRequire(interfaceName, interfaceClass,
						describe, version);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private ServiceProvider getServiceProviderByAttrs(Map<String, Object> attrs) {
		String serviceName;

		Class<?> serviceInterface;
		String describe;
		String version;
		if (attrs != null) {
			serviceName = (String) attrs.get("name");
			serviceInterface = (Class<?>) attrs.get("interfaceClass");
			describe = (String) attrs.get("desc");
			version = (String) attrs.get("version");
			return new ServiceProvider(serviceName, serviceInterface, describe,
					version);
		}
		return null;
	}

	// public static class FactoryBeanRMI<T> implements InitializingBean,
	// FactoryBean<T> {
	// private String innerClassName;
	//
	// public FactoryBeanRMI(String innerClassName) {
	// this.innerClassName = innerClassName;
	// }
	//
	// public void setInnerClassName(String innerClassName) {
	// this.innerClassName = innerClassName;
	// }
	//
	// public T getObject() throws Exception {
	// @SuppressWarnings("rawtypes")
	// // Class innerClass = Class.forName(innerClassName);
	// Class innerClass = Class.forName(innerClassName);
	// if (innerClass.isInterface()) {
	// return (T) InterfaceProxy.newInstance(innerClass);
	// } else {
	// Enhancer enhancer = new Enhancer();
	// enhancer.setSuperclass(innerClass);
	// enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
	// enhancer.setCallback(new MethodInterceptorImpl());
	// return (T) enhancer.create();
	// }
	// }
	//
	// public Class<?> getObjectType() {
	// try {
	// return Class.forName(innerClassName);
	// // return Class.forName(innerClassName);
	// } catch (ClassNotFoundException e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	//
	// public boolean isSingleton() {
	// return true;
	// }
	//
	// public void afterPropertiesSet() throws Exception {
	// }
	// }

	// public static class InterfaceProxy implements InvocationHandler {
	// public Object invoke(Object proxy, Method method, Object[] args)
	// throws Throwable {
	// System.out.println("ObjectProxy execute:" + method.getName());
	// return method.invoke(proxy, args);
	// }
	//
	// public static <T> T newInstance(Class<T> innerInterface) {
	// ClassLoader classLoader = innerInterface.getClassLoader();
	// Class[] interfaces = new Class[] { innerInterface };
	// InterfaceProxy proxy = new InterfaceProxy();
	// return (T) Proxy.newProxyInstance(classLoader, interfaces, proxy);
	// }
	// }

	// public static class MethodInterceptorImpl implements MethodInterceptor {
	// public Object intercept(Object o, Method method, Object[] objects,
	// MethodProxy methodProxy) throws Throwable {
	// System.out.println("MethodInterceptorImpl:" + method.getName());
	// return methodProxy.invokeSuper(o, objects);
	// }
	// }
}
