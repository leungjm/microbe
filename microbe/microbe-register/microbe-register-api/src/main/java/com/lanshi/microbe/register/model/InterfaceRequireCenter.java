package com.lanshi.microbe.register.model;

public class InterfaceRequireCenter extends InterfaceRequire {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MicrobeSocketAddress requireAddress;

	public InterfaceRequireCenter(MicrobeSocketAddress requireAddress) {
		this.requireAddress = requireAddress;
	}

	public InterfaceRequireCenter(InterfaceRequire require,
			MicrobeSocketAddress requireAddress) {
		this.interfaceName = require.getInterfaceName();
		this.interfaceClass = require.getInterfaceClass();
		this.describe = require.getDescribe();
		this.version = require.getVersion();
		this.requireAddress = requireAddress;
	}

	public InterfaceRequireCenter(String interfaceName,
			Class<?> interfaceClass, String describe, String version,
			MicrobeSocketAddress requireAddress) {
		this.interfaceName = interfaceName;
		this.interfaceClass = interfaceClass;
		this.describe = describe;
		this.version = version;
		this.requireAddress = requireAddress;
	}

	public MicrobeSocketAddress getRequireAddress() {
		return requireAddress;
	}

	public void setRequireAddress(MicrobeSocketAddress requireAddress) {
		this.requireAddress = requireAddress;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null && !(obj instanceof InterfaceRequireCenter))
			return false;
		InterfaceRequireCenter center = (InterfaceRequireCenter) obj;
		return this.interfaceClass.equals(center.getInterfaceClass())
				&& this.interfaceName.equals(center.getInterfaceName())
				&& this.version.equals(center.getVersion())
				&& this.requireAddress.equals(center.getRequireAddress());
	}

}
