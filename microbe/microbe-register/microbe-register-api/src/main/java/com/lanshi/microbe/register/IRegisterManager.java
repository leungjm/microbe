package com.lanshi.microbe.register;

import java.util.concurrent.CopyOnWriteArrayList;

import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.MicrobeSocketAddress;
import com.lanshi.microbe.register.model.Route;
import com.lanshi.microbe.register.model.ServiceProvider;

public interface IRegisterManager<T> {

	void register();

	/**
	 * 向服务器注册
	 * 
	 * @param require
	 *            申请所需服务列表
	 * @param provider
	 *            本机提供的服务列表
	 * @return
	 */
	Route register2Server(MicrobeSocketAddress socketAddress,
			CopyOnWriteArrayList<InterfaceRequire> require,
			CopyOnWriteArrayList<ServiceProvider> provider);

	boolean unregister(MicrobeSocketAddress socketAddress);

	T getNewClientInstance();

	/**
	 * 获取一个服务
	 * 
	 * @param apiClass
	 * @return
	 */
	T getClient(MicrobeSocketAddress socketAddress);

	/**
	 * 确认是否注册
	 * 
	 * @return
	 */
	boolean isregister();
}
