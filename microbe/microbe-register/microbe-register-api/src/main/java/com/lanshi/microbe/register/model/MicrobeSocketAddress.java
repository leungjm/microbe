package com.lanshi.microbe.register.model;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class MicrobeSocketAddress extends SocketAddress {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6374801494862894178L;
	// The hostname of the Socket Address
	private String hostname;
	// The IP address of the Socket Address
	private String addr;
	// The port number of the Socket Address
	private int port;

	public MicrobeSocketAddress(int port) throws UnknownHostException {
		this(null, InetAddress.getLocalHost().getHostAddress(), port);
	}

	public MicrobeSocketAddress(String addr, int port) {
		this(null, addr, port);
	}

	public MicrobeSocketAddress(String hostname, String addr, int port) {
		this.hostname = hostname;
		this.addr = addr;
		this.port = port;
	}

	public InetSocketAddress getInetSocketAddress() {
		InetSocketAddress sock = new InetSocketAddress(addr, port);
		return sock;
	}

	public String getIp() {
		return this.addr;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && !(obj instanceof MicrobeSocketAddress))
			return false;
		MicrobeSocketAddress oth = (MicrobeSocketAddress) obj;
		return this.getIp().equals(oth.getIp()) && this.port == oth.getPort();
	}
}
