package com.lanshi.microbe.register.api;

import com.lanshi.microbe.register.annotation.RmiRegister;

@RmiRegister(desc = "服务注册接口", name = "registerServer", version = "1.0")
public interface TestIRegister {

	public boolean unregister(String request);

}
