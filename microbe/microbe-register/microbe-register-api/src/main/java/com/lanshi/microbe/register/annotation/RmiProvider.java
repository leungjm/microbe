package com.lanshi.microbe.register.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface RmiProvider {
	/**
	 * 远程接口实现名称
	 * 
	 * @return
	 */
	String name();

	/**
	 * 提供调用的接口类
	 * 
	 * @return
	 */
	Class<?> interfaceClass();

	/**
	 * 远程接口实现描述
	 * 
	 * @return
	 */
	String desc();

	/**
	 * 版本号
	 * 
	 * @return
	 */
	String version();
}
