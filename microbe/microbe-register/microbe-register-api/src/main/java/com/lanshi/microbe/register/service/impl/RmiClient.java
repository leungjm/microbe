package com.lanshi.microbe.register.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.register.IRouteManager;
import com.lanshi.microbe.register.service.IClientRegister;
import com.lanshi.microbe.register.service.IRmiClient;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

@Component
public class RmiClient implements IRmiClient<IClient> {

	@Autowired
	private IClientRegister<IClient> register;
	@Autowired
	private IRouteManager routeManager;

	@Override
	public Response sendRequest(Request request) {
		return routeManager.sendRequest(request);
		// IClient client = register.getLoadBalanceClient(request.getApiClass()
		// .getName());
		// return client.sendRequest(request);
	}

	@Override
	public IClient getNewUnconnectClient() {
		return register.getNewClientInstance();
	}

}
