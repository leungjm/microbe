package com.lanshi.microbe.register.service;

public interface IRmiMethodHandler {
	public Object invoke(Class<?> proxyInterface, String methodName,
			Object[] args);
}
