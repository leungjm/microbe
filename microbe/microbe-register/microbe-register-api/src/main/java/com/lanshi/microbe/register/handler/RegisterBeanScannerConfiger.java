package com.lanshi.microbe.register.handler;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@Component
// @PropertySource("classpath:prop/register.properties")
public class RegisterBeanScannerConfiger implements BeanFactoryPostProcessor,
		ApplicationContextAware {

	/**
	 * @value无效，BeanFactoryPostProcessor 
	 *                                   postProcessBeanFactory优先于InitializingBean执行？
	 */
	@Value("${rmi.interface.packages}")
	private String rmiPackages;

	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public void postProcessBeanFactory(
			ConfigurableListableBeanFactory beanFactory) throws BeansException {
		RmiRegisterScanner scanner = new RmiRegisterScanner(
				(BeanDefinitionRegistry) beanFactory);
		scanner.setResourceLoader(this.applicationContext);
		scanner.setApplicationContext(applicationContext);
		if (rmiPackages == null) {
			rmiPackages = applicationContext
					.getBean(PropertySourcesPlaceholderConfigurer.class)
					.getAppliedPropertySources().get("localProperties")
					.getProperty("rmi.interface.packages").toString();
		}
		if (rmiPackages != null) {
			String[] packages = rmiPackages.split(";");
			scanner.scan(packages);
		}
		// scanner.scan("com.lanshi.microbe.register.api");
	}
}
