package com.lanshi.microbe.register.service;

import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

public interface IRmiClient<T extends IClient> {
	public Response sendRequest(Request request);

	public T getNewUnconnectClient();
}
