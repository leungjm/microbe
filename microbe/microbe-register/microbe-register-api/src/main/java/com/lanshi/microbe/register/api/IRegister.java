package com.lanshi.microbe.register.api;

import java.net.InetSocketAddress;
import java.util.List;

import com.lanshi.microbe.register.annotation.RmiRegister;
import com.lanshi.microbe.register.model.CopyOnWriteMap;
import com.lanshi.microbe.register.model.RegisterRequest;

@RmiRegister(desc = "服务注册接口", name = "registerServer", version = "1.0")
public interface IRegister {
	/**
	 * 注册接口
	 * 
	 * @param clazzs
	 * @return
	 */
	public CopyOnWriteMap<String, List<InetSocketAddress>> doRegister(
			RegisterRequest request);

	public boolean unregister(RegisterRequest request);

	public CopyOnWriteMap<String, List<InetSocketAddress>> getRegisterClient();
}
