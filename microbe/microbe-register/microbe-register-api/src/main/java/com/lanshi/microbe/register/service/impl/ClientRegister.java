package com.lanshi.microbe.register.service.impl;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.register.api.IRegister;
import com.lanshi.microbe.register.handler.HeartBeatHandler;
import com.lanshi.microbe.register.model.CopyOnWriteMap;
import com.lanshi.microbe.register.model.RegisterRequest;
import com.lanshi.microbe.register.service.IClientRegister;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

@Component
public class ClientRegister implements IClientRegister<IClient>,
		ApplicationContextAware {
	private ApplicationContext applicationContext;

	// private static CopyOnWriteMap<String, List<InetSocketAddress>>
	// serviceHostMap = new CopyOnWriteMap<String, List<InetSocketAddress>>();
	private static CopyOnWriteMap<String, List<IClient>> serviceClientMap = new CopyOnWriteMap<String, List<IClient>>();
	// private static CopyOnWriteMap<String, IClient> clientsMap = new
	// CopyOnWriteMap<String, IClient>();

	@Value("${register.server.host}")
	private String registeServerHost;
	@Value("${register.server.port}")
	private int registeServerPort;

	@Value("${server.port}")
	private int servicePort;

	private String clientName;

	@Autowired
	private IClient nettyClient;

	@Autowired
	private IRegister register;

	@Autowired
	private HeartBeatHandler hbHandler;

	@PostConstruct
	public void postConstruct() {
		MicrobeLogger.info("{} is inited!", getClass());
		MicrobeLogger.info(
				"now going to register to server with server host:{} port:{}",
				registeServerHost, registeServerPort);
		// initRegister();
	}

	private void initRegister() {
		try {
			nettyClient.connect(new InetSocketAddress(registeServerHost,
					registeServerPort));
			RegisterRequest params = new RegisterRequest();
			params.setHostName("Hello");
			params.setSocketAddress(new InetSocketAddress(InetAddress
					.getLocalHost(), servicePort));
			Response response = nettyClient.sendRequest(new Request(
					IRegister.class, "doRegister", params));
			System.out
					.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println(response.toString());
		} catch (UnknownHostException e) {
			MicrobeLogger.error("注册服务异常", e);
		}

	}

	@Override
	public boolean register2Server() {
		try {
			// serviceHostMap.clear();
			serviceClientMap.clear();
			// clientsMap.clear();
			// 初始化注册地址及客户端连接
			InetSocketAddress registerServerAddr = new InetSocketAddress(
					registeServerHost, registeServerPort);
			// List<InetSocketAddress> registerAddrs = new
			// ArrayList<InetSocketAddress>();
			// registerAddrs.add(registerServerAddr);
			// serviceHostMap.put(IRegister.class.getName(), registerAddrs);
			IClient registerclient = getNewClientInstance();
			registerclient.connect(registerServerAddr);
			List<IClient> regList = new ArrayList<IClient>();
			regList.add(registerclient);
			serviceClientMap.put(IRegister.class.getName(), regList);
			// clientsMap.put(registerServerAddr.toString(), registerclient);
			// ----------注册----------------
			RegisterRequest params = new RegisterRequest();
			params.setHostName(clientName);
			InetSocketAddress localAddr = new InetSocketAddress(
					InetAddress.getLocalHost(), servicePort);
			params.setSocketAddress(localAddr);
			CopyOnWriteMap<String, List<InetSocketAddress>> serviceMap = register
					.doRegister(params);
			undateLocalServices(serviceMap, false);
			// serviceHostMap.putAll(serviceMap);
			hbHandler.sendHeartBeat();
		} catch (UnknownHostException e) {
			MicrobeLogger.error("注册服务出错", e);
			return false;
		}
		return true;
	}

	private void undateLocalServices(
			CopyOnWriteMap<String, List<InetSocketAddress>> serviceMap,
			boolean reload) {
		CopyOnWriteMap<String, List<IClient>> newClients = new CopyOnWriteMap<String, List<IClient>>();

		for (String key : serviceMap.keySet()) {
			List<IClient> clients = newClients.get(key);
			if (clients == null) {
				clients = new ArrayList<IClient>();
			}
			List<InetSocketAddress> hosts = serviceMap.get(key);
			if (hosts != null) {
				for (InetSocketAddress host : hosts) {
					// String hostKey = host.toString();
					boolean newadd = true;
					for (IClient ct : clients) {
						if (ct.getRemoteAddress().equals(host)) {
							newadd = false;
						}
					}
					if (newadd) {
						IClient nclient = null;
						try {
							nclient = getNewClientInstance();
							nclient.connect(host);
							clients.add(nclient);
						} catch (Exception e) {
							MicrobeLogger.warn("connetc to " + host
									+ " false, now drop it!", e);
						}
					}
					// if (!clientsMap.containsKey(hostKey)) {
					// IClient nclient = getNewClientInstance();
					// nclient.connect(host);
					// clients.add(nclient);
					// clientsMap.put(hostKey, nclient);
					// }
				}
			}
			newClients.put(key, clients);
			if (reload) {
				List<IClient> regList = serviceClientMap.get(IRegister.class
						.getName());
				serviceClientMap.clear();
				serviceClientMap.put(IRegister.class.getName(), regList);
				serviceClientMap.putAll(newClients);
			}
		}
	}

	@Override
	public boolean unregister() {
		try {
			RegisterRequest params = new RegisterRequest();
			params.setHostName(clientName);
			params.setSocketAddress(new InetSocketAddress(InetAddress
					.getLocalHost(), servicePort));
			return register.unregister(params);
		} catch (UnknownHostException e) {
			MicrobeLogger.error("注销服务出错", e);
		}
		return false;
	}

	@Override
	public IClient getLoadBalanceClient(String serviceName) {
		// List<InetSocketAddress> hosts = serviceHostMap.get(serviceName);
		List<IClient> clients = serviceClientMap.get(serviceName);
		int selected = new Random().nextInt(clients.size());
		return clients.get(selected);
		// InetSocketAddress selectedAddr = hosts.get(selected);
		// return clientsMap.get(selectedAddr.toString());
	}

	@Override
	public IClient getNewClientInstance() {
		return applicationContext.getBean(IClient.class);
	}

	@Override
	public List<IClient> getClients() {
		List<IClient> clients = new ArrayList<IClient>();
		LinkedHashSet<InetSocketAddress> lhs = new LinkedHashSet<InetSocketAddress>();
		for (String key : serviceClientMap.keySet()) {
			List<IClient> cls = serviceClientMap.get(key);
			for (IClient client : cls) {
				if (lhs.add(client.getRemoteAddress())) {
					clients.add(client);
				}
			}
		}
		// for (IClient client : clientsMap.values()) {
		// clients.add(client);
		// }
		return clients;
	}

	@Override
	public void reconnectServer(InetSocketAddress socketAddress) {
		CopyOnWriteMap<String, List<IClient>> scMap = new CopyOnWriteMap<String, List<IClient>>();
		for (String key : serviceClientMap.keySet()) {
			List<IClient> cls = serviceClientMap.get(key);
			IClient removeClient = null;
			for (IClient client : cls) {
				if (client.getRemoteAddress().equals(socketAddress)) {
					removeClient = client;
					break;
				}
			}
			cls.remove(removeClient);
			scMap.put(key, cls);
		}
		for (String key : scMap.keySet()) {
			serviceClientMap.put(key, scMap.get(key));
		}

		CopyOnWriteMap<String, List<InetSocketAddress>> nreg = register
				.getRegisterClient();
		undateLocalServices(nreg, true);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
