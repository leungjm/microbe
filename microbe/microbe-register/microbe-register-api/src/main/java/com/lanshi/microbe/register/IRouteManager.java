package com.lanshi.microbe.register;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.InterfaceRequireCenter;
import com.lanshi.microbe.register.model.MicrobeSocketAddress;
import com.lanshi.microbe.register.model.Route;
import com.lanshi.microbe.register.model.ServiceProvider;
import com.lanshi.microbe.register.model.ServiceProviderCenter;
import com.lanshi.microbe.remoting.client.IClient;
import com.lanshi.microbe.remoting.exchange.sim.Request;
import com.lanshi.microbe.remoting.exchange.sim.Response;

/**
 * 注册中心管理
 * 
 * @author liang.jm
 * 
 */
public interface IRouteManager {
	/**
	 * 添加本地所需接口
	 * 
	 * @param require
	 */
	public void addLocalInterface(InterfaceRequire require);

	/**
	 * 删除本地所需接口
	 * 
	 * @param require
	 */
	public void removeLocalInterface(InterfaceRequire require);

	/**
	 * 获取本地所需接口
	 * 
	 * @return
	 */
	public List<InterfaceRequire> getLocalInterfaceRequire();

	/**
	 * 添加本地提供接口服务
	 * 
	 * @param provider
	 */
	public void addLocalService(ServiceProvider provider);

	/**
	 * 删除本地提供接口服务
	 * 
	 * @param provider
	 */
	public void removeLocalService(ServiceProvider provider);

	/**
	 * 获取本地提供的服务接口
	 * 
	 * @return
	 */
	public List<ServiceProvider> getLocalService();

	/**
	 * 收集远程所需方法及地址
	 * 
	 * @param require
	 */
	public void addRemoteInterface(InterfaceRequireCenter require);

	/**
	 * 获取需求提供表
	 * 
	 * @return
	 */
	public CopyOnWriteArrayList<InterfaceRequireCenter> getRequires();

	/**
	 * 删除远程所需方法及地址
	 * 
	 * @param require
	 */
	public void removeRemoteInterface(InterfaceRequireCenter require);

	/**
	 * 收集远程方法提供者信息
	 * 
	 * @param provider
	 */
	public void addRemoteService(ServiceProviderCenter provider);

	/**
	 * 删除远程方法提供者信息
	 * 
	 * @param provider
	 */
	public void removeRemoteService(ServiceProviderCenter provider);

	/**
	 * 获取服务提供者路由信息
	 * 
	 * @return
	 */
	public CopyOnWriteArrayList<ServiceProviderCenter> getProviders();

	/**
	 * 删除远程机器所需的接口信息
	 * 
	 * @param socketAddress
	 */
	public void removeRemoteInterfaces(MicrobeSocketAddress socketAddress);

	/**
	 * 删除远程机器所提供的服务信息
	 * 
	 * @param socketAddress
	 */
	public void removeRemoteServices(MicrobeSocketAddress socketAddress);

	/**
	 * 获取所需的服务提供者
	 * 
	 * @param requires
	 */
	public List<ServiceProviderCenter> getRequireInterfaceProvider(
			List<InterfaceRequire> requires);

	/**
	 * 添加尚未找到服务提供者的接口信息
	 * 
	 * @param require
	 */
	public void addStillRequireService(InterfaceRequire require);

	/**
	 * 删除尚未找到服务提供者的接口信息
	 * 
	 * @param require
	 */
	public void removeStillRequireService(InterfaceRequire require);

	/**
	 * 删除一个服务地址
	 * 
	 * @param clientSocketAddr
	 * @return
	 */
	public boolean removeRoute(MicrobeSocketAddress clientSocketAddr);

	/**
	 * 添加路由表
	 * 
	 * @param providers
	 * @return
	 */
	public boolean addRoute(List<ServiceProviderCenter> providers);

	/**
	 * 刷新路由表
	 * 
	 * @param route
	 * @return
	 */
	public boolean refreshRoute(Route route);

	/**
	 * 发送请求
	 * 
	 * @param request
	 * @return
	 */
	public Response sendRequest(Request request);

	/**
	 * 获取新的连接
	 * 
	 * @return
	 */
	public IClient getNewUnconnectClient();

}
