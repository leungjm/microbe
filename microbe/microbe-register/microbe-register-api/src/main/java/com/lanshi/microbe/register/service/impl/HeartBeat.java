package com.lanshi.microbe.register.service.impl;

import org.springframework.stereotype.Component;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.register.service.BaseService;
import com.lanshi.microbe.register.service.IHeartBeat;

@Component
public class HeartBeat extends BaseService implements IHeartBeat {

	@Override
	public Byte beat(String hostAddr) {
		MicrobeLogger.info("recv {} beat!", hostAddr);
		return (byte) 1;
	}

}
