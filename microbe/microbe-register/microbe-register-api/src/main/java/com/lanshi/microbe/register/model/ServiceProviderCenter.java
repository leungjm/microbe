package com.lanshi.microbe.register.model;

public class ServiceProviderCenter extends ServiceProvider {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MicrobeSocketAddress providerSocketAddress;

	public ServiceProviderCenter(MicrobeSocketAddress providerSocketAddress) {
		this.providerSocketAddress = providerSocketAddress;
	}

	public ServiceProviderCenter(ServiceProvider provider,
			MicrobeSocketAddress providerSocketAddress) {
		this.serviceName = provider.getServiceName();
		this.serviceInterface = provider.getServiceInterface();
		this.describe = provider.getDescribe();
		this.version = provider.getVersion();
		this.providerSocketAddress = providerSocketAddress;
	}

	public MicrobeSocketAddress getProviderSocketAddress() {
		return providerSocketAddress;
	}

	public void setProviderSocketAddress(
			MicrobeSocketAddress providerSocketAddress) {
		this.providerSocketAddress = providerSocketAddress;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null && !(obj instanceof ServiceProviderCenter))
			return false;
		ServiceProviderCenter center = (ServiceProviderCenter) obj;
		return this.serviceInterface.equals(center.getServiceInterface())
				&& this.serviceName.equals(center.getServiceName())
				&& this.version.equals(center.getVersion())
				&& this.providerSocketAddress.equals(center
						.getProviderSocketAddress());
	}

}
