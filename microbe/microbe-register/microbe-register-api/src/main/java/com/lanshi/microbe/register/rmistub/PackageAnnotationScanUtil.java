package com.lanshi.microbe.register.rmistub;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javassist.CtClass;

import com.lanshi.microbe.Logger.MicrobeLogger;

public class PackageAnnotationScanUtil {

	public static List<CtClass> scanPackageExistAnnotation(
			Class<?> annotationClass, String... packageStr) {
		List<CtClass> clazzs = new ArrayList<CtClass>();
		if (packageStr != null && packageStr.length > 0) {
			for (String pack : packageStr) {
				MicrobeLogger.info("scan package {} with annotation class {}",
						pack, annotationClass.getName());
				Set<CtClass> clzz = JavassistScan.getClasses(pack);
				try {
					for (CtClass clz : clzz) {
						Object apiclz = clz.getAnnotation(annotationClass);
						if (apiclz != null) {// 解析类
							// RmiRegister annot = (RmiRegister) apiclz;
							clazzs.add(clz);
						}
					}
				} catch (ClassNotFoundException e) {
					MicrobeLogger.error("解析Rmi接口注册时找不到相关RmiRegister类", e);
				}
			}
		}
		return clazzs;
	}
}
