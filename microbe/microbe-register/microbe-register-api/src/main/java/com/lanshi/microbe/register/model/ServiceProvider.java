package com.lanshi.microbe.register.model;

import java.io.Serializable;

import com.lanshi.microbe.register.api.IRegister;

public class ServiceProvider implements Serializable,
		Comparable<InterfaceRequire> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String serviceName;
	protected Class<?> serviceInterface;
	protected String describe;
	protected String version;

	public ServiceProvider() {
	}

	public ServiceProvider(String serviceName, Class<?> serviceInterface,
			String describe, String version) {
		this.serviceName = serviceName;
		this.serviceInterface = serviceInterface;
		this.describe = describe;
		this.version = version;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Class<?> getServiceInterface() {
		return serviceInterface;
	}

	public void setServiceInterface(Class<?> serviceInterface) {
		this.serviceInterface = serviceInterface;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * 服务提供与所需接口比较
	 */
	@Override
	public int compareTo(InterfaceRequire o) {
		if (this.serviceInterface.getName().equals(
				o.getInterfaceClass().getName())
				&& this.version.equals(o.getVersion())) {
			return 0;
		}
		return -1;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof ServiceProvider)
			return false;
		ServiceProvider oth = (ServiceProvider) obj;
		return this.serviceInterface.equals(oth.getServiceInterface())
				&& this.serviceName.equals(oth.getServiceName())
				&& this.version.equals(oth.getVersion());
	}

	public static void main(String[] args) {
		ServiceProvider sp = new ServiceProvider("ABC", IRegister.class, "",
				"1.1");
		InterfaceRequire ir = new InterfaceRequire("BCD", IRegister.class, "",
				"1.2");
		if (sp.compareTo(ir) == 0) {
			System.out.println("哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈");
		} else {
			System.out.println("哐哐哐哐哐哐");
		}
	}
}
