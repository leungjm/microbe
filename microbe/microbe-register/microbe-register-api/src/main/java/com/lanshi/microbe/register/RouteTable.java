package com.lanshi.microbe.register;

import java.util.concurrent.CopyOnWriteArrayList;

import com.lanshi.microbe.register.model.CopyOnWriteMap;
import com.lanshi.microbe.register.model.InterfaceRequire;
import com.lanshi.microbe.register.model.InterfaceRequireCenter;
import com.lanshi.microbe.register.model.MicrobeSocketAddress;
import com.lanshi.microbe.register.model.ServiceProvider;
import com.lanshi.microbe.register.model.ServiceProviderCenter;

public class RouteTable {
	/**
	 * 本地所需要的接口服务
	 */
	public static CopyOnWriteArrayList<InterfaceRequire> interfaceServices = new CopyOnWriteArrayList<InterfaceRequire>();
	/**
	 * 本地所需要尚未获得提供的服务接口
	 */
	public static CopyOnWriteArrayList<InterfaceRequire> interfaceRequireServices = new CopyOnWriteArrayList<InterfaceRequire>();
	/**
	 * 本地所提供的远程服务
	 */
	public static CopyOnWriteArrayList<ServiceProvider> serviceProviders = new CopyOnWriteArrayList<ServiceProvider>();
	/**
	 * 本地路由表
	 */
	public static CopyOnWriteMap<Class<?>, CopyOnWriteArrayList<MicrobeSocketAddress>> localRoute = new CopyOnWriteMap<Class<?>, CopyOnWriteArrayList<MicrobeSocketAddress>>();
	/**
	 * 远程注册所需的接口服务
	 */
	public static CopyOnWriteArrayList<InterfaceRequireCenter> interfaceServicesCenter = new CopyOnWriteArrayList<InterfaceRequireCenter>();
	/**
	 * 远程注册所提供的服务
	 */
	public static CopyOnWriteArrayList<ServiceProviderCenter> serviceProvidersCenter = new CopyOnWriteArrayList<ServiceProviderCenter>();

	/**
	 * 添加本地所需接口
	 * 
	 * @param require
	 */
	public static void addLocalInterface(InterfaceRequire require) {
		interfaceServices.addIfAbsent(require);
	}

	/**
	 * 添加本地提供接口服务
	 * 
	 * @param provider
	 */
	public static void addLocalService(ServiceProvider provider) {
		serviceProviders.addIfAbsent(provider);
	}

}
