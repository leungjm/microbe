package com.lanshi.microbe.register.rmistub;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.NotFoundException;

import com.lanshi.microbe.register.annotation.RmiRegister;

public class ProxyRmiStubUtil {
	public static ClassPool pool = new ClassPool(true);
	private static String proxyStubPackageDefault = "com.lanshi.microbe.rmi.proxy.stub";

	public static void main(String[] args) {
		scanRmiRegisterAnnotation("com.lanshi.microbe.register.api");
	}

	public static String generatorProxyClass(String beanName) {
		try {
			CtClass clz = pool.get(beanName);

			String packageName = clz.getPackageName();
			if (packageName == null || clz.getPackageName().length() < 1) {
				packageName = proxyStubPackageDefault;
			} else {
				packageName = packageName + ".impl";
			}
			CtClass stubClass = pool.makeClass(packageName + "."
					+ clz.getName() + "Impl");
			stubClass.addInterface(clz);// 添加接口
			CtMethod[] intMethods = clz.getDeclaredMethods();
			for (CtMethod met : intMethods) {

				try {
					CtMethod newMet = CtNewMethod
							.make(met.getReturnType(),
									met.getName(),
									met.getParameterTypes(),
									null,
									"{System.out.println(\"ABCD  Hi................................\");return false;}",
									stubClass);
					// CtMethod newMet = CtNewMethod.make(methodStr, clz);
					// newMet.setBody("{System.out.println(\"Hi................................\");return false;}");
					stubClass.addMethod(newMet);
					// Class<?> clazz = stubClass.getClass();
					stubClass.writeFile("./target/classes");
					// Object o = stubClass.toClass().newInstance();
					// Method getter = o.getClass().getMethod(met.getName(),
					// getParamsTypes(met));
					// System.out.println(getter.invoke(o,
					// new HashMap<String, Class<?>>()));
					System.out
							.println("-------------------------------------------");
					return stubClass.getName();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void scanRmiRegisterAnnotation(String... packages) {
		List<CtClass> clazzs = PackageAnnotationScanUtil
				.scanPackageExistAnnotation(RmiRegister.class, packages);
		for (CtClass clz : clazzs) {
			String packageName = clz.getPackageName();
			if (packageName == null || clz.getPackageName().length() < 1) {
				packageName = proxyStubPackageDefault;
			} else {
				packageName = packageName + ".impl";
			}
			CtClass stubClass = pool.makeClass(packageName + "."
					+ clz.getName() + "Impl");
			stubClass.addInterface(clz);// 添加接口
			CtMethod[] intMethods = clz.getDeclaredMethods();
			for (CtMethod met : intMethods) {

				try {
					CtMethod newMet = CtNewMethod
							.make(met.getReturnType(),
									met.getName(),
									met.getParameterTypes(),
									null,
									"{System.out.println(\"Hi................................\");return false;}",
									stubClass);
					// CtMethod newMet = CtNewMethod.make(methodStr, clz);
					// newMet.setBody("{System.out.println(\"Hi................................\");return false;}");
					stubClass.addMethod(newMet);
					// Class<?> clazz = stubClass.getClass();
					stubClass.writeFile("./target/classes");
					Object o = stubClass.toClass().newInstance();
					Method getter = o.getClass().getMethod(met.getName(),
							getParamsTypes(met));
					System.out.println(getter.invoke(o,
							new HashMap<String, Class<?>>()));
					System.out
							.println("-------------------------------------------");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	private static String getParams(CtMethod met) throws NotFoundException {
		StringBuffer sb = new StringBuffer();
		// 使用javaassist的反射方法获取方法的参数名---接口类无法获取参数名称
		// MethodInfo methodInfo = met.getMethodInfo();
		// CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
		// LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute
		// .getAttribute(LocalVariableAttribute.tag);
		// if (attr == null) {
		// // exception
		// throw new RuntimeException("LocalVariableTable is Null!");
		// }
		// Map<String, String> paramsMap = new HashMap<String, String>();
		// int pos = Modifier.isStatic(met.getModifiers()) ? 0 : 1;
		int vnIndex = 0;
		for (CtClass ctc : met.getParameterTypes()) {// 解析方法参数名
			// String pname = attr.variableName(vnIndex++ + pos);
			// System.out.println(pname + ":" +
			// ctc.getName());
			// sb.append(pname + " " + ctc.getName() + ",");
			sb.append(ctc.getName() + " arg" + vnIndex++ + ",");
			// paramsMap.put(pname, ctc.getName());

		}
		if (sb.length() > 1) {
			return sb.substring(0, sb.length() - 1);
		}
		return sb.toString();
	}

	private static Class<?>[] getParamsTypes(CtMethod met)
			throws NotFoundException, ClassNotFoundException {
		Class<?>[] cls = new Class<?>[met.getParameterTypes().length];
		int index = 0;
		for (CtClass ctc : met.getParameterTypes()) {
			System.out.println(ctc.getName());
			cls[index++] = Class.forName(ctc.getName());
		}
		return cls;
	}

}
