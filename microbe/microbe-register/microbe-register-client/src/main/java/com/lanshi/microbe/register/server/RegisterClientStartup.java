package com.lanshi.microbe.register.server;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.lanshi.microbe.Logger.MicrobeLogger;
import com.lanshi.microbe.container.spring.SpringContainerImpl;
import com.lanshi.microbe.register.IRegisterManager;
import com.lanshi.microbe.register.api.TestRmi;
import com.lanshi.microbe.register.service.IClientRegister;
import com.lanshi.microbe.remoting.netty.server.NettyServer;

public class RegisterClientStartup {
	private static AbstractApplicationContext ctx;

	public static void main(String[] args) {
		SpringContainerImpl container = startContainer();
		ctx = container.getContext().get();
		startServer();
	}

	private static SpringContainerImpl startContainer() {
		SpringContainerImpl container = new SpringContainerImpl();
		container.start();
		return container;
	}

	private static void startServer() {
		Object port = ctx.getBean(PropertySourcesPlaceholderConfigurer.class)
				.getAppliedPropertySources().get("localProperties")
				.getProperty("server.port");
		ctx.getBean(NettyServer.class).start("localhost",
				Integer.parseInt(port.toString()));
		// ctx.getBean(IClientRegister.class).register2Server();
		ctx.getBean(IRegisterManager.class).register();
		ctx.getBean(TestRmi.class).test();
	}

	public static void stopServer() {
		boolean flag = ctx.getBean(IClientRegister.class).unregister();
		MicrobeLogger.warn("server unregister status->" + flag);
		ctx.getBean(NettyServer.class).stop();
	}
}
