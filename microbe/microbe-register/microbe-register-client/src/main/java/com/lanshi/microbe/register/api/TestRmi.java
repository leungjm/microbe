package com.lanshi.microbe.register.api;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestRmi {
	@Autowired
	private TestIRegister testIRegister;

	@PostConstruct
	public void init() {
	}

	public void test() {
		testIRegister.unregister("");
	}
}
